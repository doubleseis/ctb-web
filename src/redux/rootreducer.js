import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import {proReducer} from './reducers/proUser.reducer';


const persistConfig = {
  key: 'root',
  storage,

};

const rootReducer = combineReducers({
  prouser: proReducer,
 
});

export default rootReducer
