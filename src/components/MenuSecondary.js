import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link, NavLink } from "react-router-dom";
import LogIn from './LogIn';

class MenuSecondary extends Component {
     constructor(props) {
          super(props)
          this.state = {
               condition: false,
               showModal: false
          }
          this.handleClick = this.handleClick.bind(this)
          this.handleOpenModal = this.handleOpenModal.bind(this)
          this.handleCloseModal = this.handleCloseModal.bind(this);
     }

     handleClick(){
          this.setState({
               condition: !this.state.condition
          })
     }

     handleOpenModal () {
          this.setState({ showModal: true });
     }
     
     handleCloseModal () {
          this.setState({ showModal: false });
     }

     render() {
          return (
               <div className={'menu menu--primary'}>
                    <HamburgerButton onClick={this.handleClick} className={`menu__hamburger-wrapper ${this.state.condition ? 'active' : ''}`} toggleClassName={ this.handleClick }>
                         <div className={'menu__hamburger'}></div>
                    </HamburgerButton>
                    <nav id="navigation" className={'menu__nav'}>
                         <ul className={'menu__list'} role={'menu'}>
                              <li className={'menu__list-item menu__list-item--bold'}>
                                   <NavLink to="/">Home</NavLink>
                              </li>
                              <li className={'menu__list-item menu__list-item--bold'}>
                                   <NavLink to="/find-a-pro">Find a Pro</NavLink>
                              </li>
                              <li className={'menu__list-item menu__list-item--icon menu__list-item--icon-user'}>
                                   <NavLink to="/dashboard">Dashboard</NavLink>
                              </li>
                              <li className={'menu__list-item'}>
                                   <Link to="/logout" aria-label="Log Out">Sign Out</Link>
                              </li>
                         </ul>
                    </nav>
                    <Modal 
                         isOpen={this.state.showModal}
                         contentLabel="Minimal Modal Example"
                         // style={modalStyles}
                         shouldCloseOnOverlayClick={true}
                         // className="modal"
                         // overlayClassName="overlay"
                    >
                         <LogIn />
                         <button onClick={this.handleCloseModal}>Close Modal</button>
                    </Modal>
               </div>
          );
     }
}

class HamburgerButton extends Component {
     render() {
          return (
               <button
                    type={'button'}
                    className={ this.props.className }
                    onClick={ this.props.toggleClassName }
               >
                    { this.props.children }
               </button>
          )    
     }
}
   
export default MenuSecondary;