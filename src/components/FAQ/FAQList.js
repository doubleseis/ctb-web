import React, { Component } from 'react';

class FAQList extends Component {
     render() {
          return (
               <section className={'accordion__list'}>
                    {this.props.children}
               </section>
          );
     }
}
   
export default FAQList;