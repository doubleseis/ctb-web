import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { Accordion } from 'semantic-ui-react';

class FAQItem extends Component {
     constructor(props) {
          super(props);
          this.props = {
               isMessage: this.props.isMessage,
               question: this.props.question,
               answer: this.props.answer,
               index: this.props.index,
               read: this.props.read,
               subject: this.props.subject,
               body: this.props.body
          }
          this.state = {
               activeIndex: 0
          }
          this.renderMessageButtons = this.renderMessageButtons.bind(this);
     }

     handleClick = (e, titleProps) => {
          const { index } = titleProps
          const { activeIndex } = this.state
          const newIndex = activeIndex === index ? -1 : index
     
          this.setState({ activeIndex: newIndex })
     }

     handleAccessibility = () => {
          
     }

     deleteMessage = () => {
          
     }

     renderMessagePreview = () => {

     }

     renderMessageButtons = () => {
          return (
               <div className='buttons'>
                    test boom
               </div>
          )
     }
     
     componentDidMount() {
          
     }

     render() {
          const { activeIndex } = this.state

          return (
               <Accordion className={'accordion__item'} tabIndex='1' onClick={ this.handleAccessibility }>
                    <Accordion.Title className={'accordion__item-title'} active={activeIndex === -1} index={0} onClick={this.handleClick} >
                         { this.props.isMessage !== true ? ReactHtmlParser(this.props.question) : ReactHtmlParser(this.props.subject) }
                         <span>
                              <FontAwesomeIcon icon={faCaretDown} />
                         </span>
                    </Accordion.Title>
                    <Accordion.Content className={'accordion__item-content'} active={activeIndex === -1}>
                         {/* { this.isMessage !== true ? this.renderMessageButtons() : '' } */}
                         { this.props.isMessage !== true ? ReactHtmlParser(this.props.answer) : ReactHtmlParser(this.props.body) }
                    </Accordion.Content>
               </Accordion>
          );
     }
}
   
export default FAQItem;