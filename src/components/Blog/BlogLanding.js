import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import noImageFound from '../../assets/images/match_with_pro2-1.jpg';
import headImage from '../../assets/images/walk-up-steps.jpg';
import BlogCard from '../Cards/BlogCard';
import { db } from '../Firebase/Firebase';
import PageHeader from '../PageHeader/PageHeader';

class BlogLanding extends Component {
     constructor(props) {
          super(props);
          this.state = {
               posts: []
          }
     }

     componentDidMount() {
          // let posts = [];
          let currentComponent = this;
          db.collection('blog').get().then(function(querySnapshot) {
               querySnapshot.forEach(function(doc) {
                    currentComponent.setState((prevState) => ({
                         posts: [...prevState.posts, {
                              published: doc.data().published,
                              id: doc.id,
                              slug: doc.data().slug ? doc.data().slug : doc.id, 
                              title: doc.data().title,
                              featuredImage: doc.data().featured_image ? doc.data().featured_image : noImageFound,
                              content: doc.data().content,
                              tag: doc.data().tag,
                              updated_at: doc.data().updated_at,
                              date: doc.data().date,
                         }]
                    }))
               });
          });
     }

     render() {
          const pageTitle = {
               pageTitle: 'Blog',
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };

          return (
               <section className="">
                    <PageHeader {...pageTitle} />
                    <div className="blog__list" >
                         <div className="blog__list-container container">
                              { this.state.posts.map((post) => ( post.published === true ?
                                   <BlogCard {...post} />
                              : '')) }
                         </div>
                    </div>
               </section>
          );
     }
}

export default withRouter(BlogLanding);