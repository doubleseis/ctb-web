import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { withRouter } from 'react-router-dom';
import noImageFound from '../../assets/images/match_with_pro2-1.jpg';
import { db } from "../Firebase/Firebase";
import { socialShare } from "../Helpers";

class BlogPost extends Component {
     constructor(props) {
          super(props);
          this.state = {
               content: "loading",
               id: "loading",
               date: "loading",
               title: "loading",
               featuredImage: "",
               number: 1,
               loading: true
          }
     }

     convertDate = (date) => {
          var date;
          var dateArray = date.split('-').reverse().join('.');
          // console.log(dateArray);
          return dateArray;
     }

     componentWillReceiveProps(nextProps) {
          this.setState({ loading: true })
          let currentComponent = this;

          db.collection('blog').get().then(function (querySnapshot) {
               querySnapshot.forEach(function (doc) {
                    if (doc.data().slug == currentComponent.props.match.params.blogid) {
                         // console.log(currentComponent.props.match.params.blogid)
                         // console.log(doc.data().number)
                         currentComponent.setState({
                              id: doc.id,
                              // username: doc.data().user.username,
                              content: doc.data().content,
                              featuredImage: doc.data().featured_image ? doc.data().featured_image : noImageFound,
                              date: doc.data().date,
                              title: doc.data().title,
                              slug: doc.data().slug
                              // number: doc.data().number

                         })
                         currentComponent.setState({ loading: false })
                    }
               })
          });
     }

     componentDidMount() {
          let currentComponent = this;
          db.collection('blog').get().then(function (querySnapshot) {
               querySnapshot.forEach(function (doc) {
                    if (doc.data().slug == currentComponent.props.match.params.blogid) {

                         currentComponent.setState({
                              id: doc.id,
                              // username: doc.data().user.username,
                              content: doc.data().content,
                              featuredImage: doc.data().featured_image ? doc.data().featured_image : noImageFound,
                              date: doc.data().date,
                              title: doc.data().title,
                              // number: doc.data().number
                              slug: doc.data().slug

                         })
                         currentComponent.setState({ loading: false })
                    }
               })
          });
     }

     render() {
          return (
               <section className={'blog-post'}>
                    { this.state.loading ?
                         (
                              <div>
                                   <h1 style={{ fontSize: 60 }}>LOADİNG</h1>
                              </div>
                         ) : (
                              <div>
                                   <div className="blog-post__header" style={{ backgroundImage: `url(${this.state.featuredImage})`, backgroundRepeat: "no-repeat", backgroundSize: "cover" }}>
                                        <div className="container">
                                             <div className="blog-post__header-content">
                                                  <h1 className='heading heading--xl'>{this.state.title}</h1>
                                                  <time>{this.convertDate(this.state.date)}</time>
                                             </div>
                                        </div>
                                   </div>
                                   <div className={'blog-post__container container'}>
                                        <div className={'blog-post__share'}>
                                             { socialShare('Blog') }
                                        </div>
                                        <div className={'blog-post__content'}>
                                             { ReactHtmlParser(this.state.content) }
                                        </div>
                                   </div>
                                   
                                   {/* <Row className="flexcontainer">
                                        <Col>
                                             <Button variant="contained" color="primary" onClick={ () => {
                                                  console.log(this.state.number)
                                                  if (this.state.number == 1) {
                                                       this.props.history.push("/blog")
                                                  } else {
                                                       let number = this.state.number - 1
                                                       this.props.history.push("/blog/" + number)
                                                  }
                                             }}>Previous</Button>
                                        
                                             <Button variant="contained" color="secondary" onClick={() => {
                                                  let number = this.state.number + 1
                                                  this.props.history.push("/blog/" + number)
                                             }}>Next</Button>
                                        </Col>
                                   </Row> */}
                              </div>
                         )
                    }
               </section>
          );
     }
}

export default withRouter(BlogPost);