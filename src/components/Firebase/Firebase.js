import * as firebase from 'firebase';

const config = {
     apiKey: "AIzaSyCKrYOJtO13QZpDQxU1TRKA01N966mDKnU",
     authDomain: "choose-to-be-fit.firebaseapp.com",
     databaseURL: "https://choose-to-be-fit.firebaseio.com",
     projectId: "choose-to-be-fit",
     storageBucket: "choose-to-be-fit.appspot.com",
     messagingSenderId: "840888064186",
     appId: "1:840888064186:web:24f708ed9b37dc3f23ca83",
     measurementId: "G-G3ZPHNCRZH" 
};

const fire =  firebase.initializeApp(config);
const admin = fire.firestore();
const db = firebase.firestore();
const dbRef = firebase.database();
const auth = firebase.auth();
const storage = firebase.storage();
const google = new firebase.auth.GoogleAuthProvider();
const fb = new firebase.auth.FacebookAuthProvider();

export { admin, fire, db, auth, google, fb, storage, dbRef };

