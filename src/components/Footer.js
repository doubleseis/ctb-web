import { faFacebookF, faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { db } from './Firebase/Firebase';

 
class Footer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               data: []
          }
     }

     componentDidMount() {
          db.collection("components").doc("footer").get()
               .then((doc) => {
                    if (doc.exists) {
                         const data = doc.data();
                         this.setState({ data: data });
                         // console.log("Document data:", data);
                    } else {
                         // doc.data() will be undefined in this case
                         this.setState({ data: null });
                         // console.log("No such document!");
                    }
               }).catch(function (error) {
                    this.setState({ data: null });
                    console.log("Error getting document:", error);
               });
     }

     render() {
          
          return (
               <footer className={'footer'}>
                    <div className={'footer__newsletter'}>
                         <div className={'footer__newsletter-container'}>
                              <form method={'post'} action={'https://choosetobefit.us20.list-manage.com/subscribe/post?u=0c2f5384fdef29a5a9b9956dd&amp;id=441bbab02d'} className={'splash__form form validate'} name="mc-embedded-subscribe-form" target="_blank" noValidate onSubmit={ this.handleSubmit }>
                                   <fieldset className={'form__container'}>
                                        <legend>Join Our Community</legend>
                                        <p className={'text-center text-white'}>Be the first to know when we expand to new cities and release life-changing health tips. <br />Also, don’t miss out on our exclusive discounts and latest updates.</p>
                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label className={'screen-reader-text'} htmlFor='mce-FNAME'>First Name*</label>
                                                  <input placeholder={'First Name'} id='mce-FNAME' className={'required'} type='text' name='FNAME' onChange={ this.handleChange } required />
                                             </div>
                                             <div className={'form__col'}>
                                                  <label className={'screen-reader-text'} htmlFor='mce-LNAME'>Last Name*</label>
                                                  <input placeholder={'Last Name'} id='mce-LNAME' type='text' name='LNAME' onChange={ this.handleChange } />
                                             </div>
                                             <div className={'form__col'}>
                                                  <label className={'screen-reader-text'} htmlFor='mce-EMAIL'>Email</label>
                                                  <input placeholder={'Email'} id='mce-EMAIL' type='email' name='EMAIL' onChange={ this.handleChange } required />
                                             </div>
                                             <div className={'form__submit'}>
                                                  <input className={'button button--primary-inverse'} type='submit' value='Subscribe' name='subscribe' id="mc-embedded-subscribe" />
                                             </div>
                                        </div>
                                   </fieldset>

                                   <div id="mce-responses" className="clear">
                                        <div className="response" id="mce-error-response hide"></div>
                                        <div className="response" id="mce-success-response hide" ></div>
                                   </div>

                                   <div className={'screen-reader-text'} aria-hidden="true">
                                        <input type="text" name="b_0c2f5384fdef29a5a9b9956dd_441bbab02d" tabIndex="-1" defaultValue="" />
                                   </div>

                              </form>
                         </div>
                    </div>
                    <div className={'footer__container-top'}>
                         <div className={'footer__row'}>
                              <div className={'col col__3'}>
                                   <h2>About C2B</h2>
                                   <ul>
                                        <li><Link to='/about'>About the Company</Link></li>
                                        <li><Link to='/contact'>Contact Us</Link></li>
                                        <li><Link to='/terms-of-use'>Terms of Use</Link></li>
                                        <li><Link to='/privacy-policy'>Privacy Policy</Link></li>
                                   </ul>
                              </div>
                              <div className={'col col__3'}>
                                   <h2>Helpful Links</h2>
                                   <ul>
                                        <li><Link to='/how-choose-to-be-fit-works'>How It Works</Link></li>
                                        <li><Link to='/find-a-pro'>Look Up Your Pro</Link></li>
                                        <li><Link to='/'>Join as a Pro</Link></li>
                                        <li><Link to='/faq-pros'>Pro FAQs</Link></li>
                                        <li><Link to='/faq-clients'>Client FAQs</Link></li>
                                        <li><Link to='/blog'>Blog</Link></li>
                                   </ul>
                              </div>
                              <div className={'col col__3'}>
                                   <h2>Contact</h2>
                                   <ul>
                                        <li>We’re here to support you. Email us if you have any questions or concerns.</li>
                                        <li>Email: <a href={'mailto:support@choosetobefit.net'} title={'Email Support'}>Contact Support</a></li>
                                        <li>Phone: <a href={'tel:+14076050816'} title={'Call Support'}>407-605-0816</a></li>
                                   </ul>
                                   <ul className={'social-list'}>
                                        <li>
                                             <a href={'https://www.facebook.com/Choose-To-Be-Fit-126918551503455/?ref=br_rs'} target={'_blank'}><FontAwesomeIcon icon={faFacebookF} /></a>
                                        </li>
                                        <li>
                                             <a href={'https://www.instagram.com/c2bfit_'} target={'_blank'}><FontAwesomeIcon icon={faInstagram} /></a>
                                        </li>
                                        <li>
                                             <a href={'https://www.youtube.com/channel/UCgy3W5Jq5mSp2Tn9_4tUd2Q'} target={'_blank'}><FontAwesomeIcon icon={faYoutube} /></a>
                                        </li>
                                   </ul>
                              </div>
                         </div>
                    </div>
                    <div className={'footer__container-btm'}>
                         <div className={'footer__row'}>
                              <div className={'col col__12'}>
                                   <p>{this.state.data.copyright}</p>
                              </div>
                         </div>
                    </div>
               </footer>
          );
     }
}
   
export default Footer;