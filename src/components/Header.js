import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logoSecondary from '../assets/images/c2by-logo.png';
import MenuPrimary from './Menu';
import MenuSecondary from './MenuSecondary';

class Header extends Component {
     render() {
          return (
               <header className={this.props.authenticated ? 'header header--auth' : 'header'}>
                    <div className={'header__container'}>
                         <div className={'header__logo'}>
                              <Link to='/'>
                                   <img alt={'logo'} src={ logoSecondary } />
                              </Link>
                         </div>
                         <div className={'header__menu'}>
                              {this.props.authenticated ? <MenuSecondary /> : <MenuPrimary />}
                         </div>
                    </div>
               </header>
          );
     }
}
   
export default Header;