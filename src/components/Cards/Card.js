import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import Deletion from "../../assets/images/deleteimage.png";
import Modal from "../ModalSecondary/Modal";

const useStyles = makeStyles({
     root: {
          maxWidth: 600,
     },
     media: {
          height: 250,
     },
});

export default function MediaCard() {
     const classes = useStyles();
     const [open,setopen] = React.useState(false)
     // console.log(open)

     const close=()=>{
          setopen(false)
     }

     return (
          <div>
               <Card className={classes.root} >
                    <CardActionArea>
                         <CardMedia
                              className={classes.media}
                              image={Deletion}
                              title="Contemplative Reptile"
                         />
                         <CardContent style={{margin:"0 20%"}}>
                              <Typography gutterBottom variant="h5" component="h2" style={{margin:"0 5%"}}>DELETE ACCOUNT</Typography>
                              <Typography variant="body2" color="textSecondary" component="p">ARE YOU SURE DELETE ACCOUNT ?</Typography>
                         </CardContent>
                    </CardActionArea>
                    <CardActions>
                         <Button variant="contained" color="secondary" style={{margin:"0 37%"}} onClick={()=>{setopen(!open)}}>DELETE</Button>
                    </CardActions>
               </Card>
               <Modal open={open} close={close}></Modal>
          </div>
     );
}