import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class BlogCard extends Component {
  constructor(props) {
    super(props);
    // this.number = props.number;
    // this.username = props.username;
    this.id = props.id;
    this.slug = props.slug;
    this.title = props.title;
    this.featuredImage = props.featuredImage;
    this.content = props.content;
    this.tag = props.tag;
    this.updated_at = props.updated_at;
    this.date = props.date;
  }

  letroute = () => {
    this.props.history.push("blog/" + this.slug);
  };

  render() {
    console.log(this.title);
    return (
      <section className={'blog-card'}>
        <div className={'blog-card__container'}>
          <div className={'blog-card__image'} onClick={this.letroute}>
            <img alt={this.props.title} src={this.featuredImage} />
          </div>
          <div className={'blog-card__content'}>
            <div className={'blog-card__content-container'}>
              {/* <div className={'blog-card__topic'}><a href={'/' + this.slug}>{ReactHtmlParser(this.tag)}</a></div> */}
              <h2 className={'heading--sm'} onClick={this.letroute}>{this.title}</h2>
              <div className={'blog-card__meta'}>
                {/* <span className={'author'}>{this.title}</span>
                <time className={'time'}>{this.updated_at}</time> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(BlogCard);
