
import { Button } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Alert from "../Alert/index";
import { auth, db } from "../Firebase/Firebase";
import { removeUnderscore } from '../Helpers';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

export default function CheckboxesGroup() {
  const classes = useStyles();
  const [primaryarray, setarray] = React.useState({})
  const [state, setState] = React.useState({

  });

  const [data, setdata] = React.useState()

  const [current, setcurrent] = React.useState({}

  )
  const [isfitness, setisfitness] = React.useState(false)
  const [ischef, setischef] = React.useState(false)
  const [isnutri, setisnutri] = React.useState(false)
  const [ismassage, setismassage] = React.useState(false)
  const [success, setsuccess] = React.useState(false)
  
  const [Fitness] = React.useState([
    "competition_prep",
    "power_lifting",
    "weight_loss",
    "body_fat_loss",
    "size_gaining",
    "endurance_training",
    "forming_and_toning",
    "flexibility",
    "aerobic_fitness",
    "pregnancy",
    "rehabilitation",
    "pilates",
    "yoga",
    "athletic"
  ])

  const [nutri] = React.useState([
    "normal_nutrition",
    "vegan_or_vegetarian",
    "paleo",
    "public_health",
    "sports",
    "pediatric",
    "diabetes",
    "heart_health",
    "autoimmune_disease",
    "food_allergies",
  ])

  const [massage] = React.useState([
    "deep_tissue",
    "swedish",
    "stone",
    "thai",
    "pregnancy",
    "sports",
    "reflexology",
  ])

  const [chef] = React.useState([
    "seafood",
    "american",
    "breakfast_or_brunch",
    "international",
    "southern",
    "healthy",
    "desserts",
    "juices_and_smoothies"
  ])

  React.useEffect(() => {
    let checkItems = []
    let user = auth.currentUser.uid
    let getuser = db.collection('users').doc(user);
    let getDoc = getuser.get()
      .then(doc => {
        setdata(doc.data())
        if (!doc.exists) {
          console.log('No such document!');
        } else {
          setarray({ ...doc.data().professions })
          setState({ ...doc.data().Ability })

          if (doc.data().professions.FitnessTrainer) {
            setisfitness(true)
          }

          if (doc.data().professions.MassageThreapist) {
            setismassage(true)
          }
          if (doc.data().professions.Nutritionist) {
            // console.log("girdi")
            setisnutri(true)
          }
          if (doc.data().professions.Chef) {
            setischef(true)
          }
          console.log(checkItems)
        }
      }).then(

      ).catch(e => console.log(e))

  }, [])
  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const handleChange2 = (event) => {
    setarray({ ...primaryarray, [event.target.name]: event.target.checked });
  };



  const submit = () => {
    let user = auth.currentUser.uid
    let setDoc = db.collection('users').doc(user).update({
      'Ability': state
    }).then(() => {

      setsuccess(true)
      setTimeout(() => {
        setsuccess(false)
      }, 3000);
    })




  }
  const submit2 = () => {
    let user = auth.currentUser.uid
    let setDoc = db.collection('users').doc(user).update({
      'professions': primaryarray
    }).then(() => {

      setsuccess(true)
      setTimeout(() => {
        setsuccess(false)
      }, 3000);

    })




  }

  console.log(success)

  return (
    <div className={'pro-services'}>
      { success && (<Alert />) }

      { data && data.isPro && (
        <div className={'pro-services__container'}>
          <div className={'pro-services__primary blocked'}>
            <h3>Select Primary Service</h3>
            <div className={'pro-services__selections'}>
              <FormControlLabel
                control={<Checkbox color={'primary'} checked={primaryarray.FitnessTrainer ? primaryarray.FitnessTrainer : false} onChange={handleChange2} name="FitnessTrainer" />}
                label={"Fitness Trainer"}
              />

              <FormControlLabel
                control={<Checkbox color={'primary'} checked={primaryarray.Chef ? primaryarray.Chef : false} onChange={handleChange2} name="Chef" />}
                label="Chef"
              />

              <FormControlLabel
                control={<Checkbox color={'primary'} checked={primaryarray.Nutritionist ? primaryarray.Nutritionist : false} onChange={handleChange2} name="Nutritionist" />}
                label="Nutritionist"
              />

              <FormControlLabel
                control={<Checkbox color={'primary'} checked={primaryarray.MassageThreapist ? primaryarray.MassageThreapist : false} onChange={handleChange2} name="MassageThreapist" />}
                label="Massage Threapist"
              />
            </div>

            <Button variant="contained" color="primary" onClick={submit2}>Update</Button>
          </div>

          <div className={'pro-services__secondary'}>
            
            <h2>Update Your Abilities</h2>

            { isfitness && (
              <div className={'pro-services__secondary-sub blocked'}>
                <h3>Fitness Trainer Abilities</h3>
                <div className={'pro-services__selections'}>
                  { Fitness.map(item => {
                    return (
                      <FormControlLabel
                        key={item}
                        control={<Checkbox checked={state[item]} color={'primary'} onChange={handleChange} name={item} />}
                        label={ removeUnderscore(item) }
                      />
                    )
                  })}
                </div>
              </div>
            )}
            
            { ischef && (
              <div className={'pro-services__secondary-sub blocked'}>
                <h3>Chef Abilities</h3>
                <div className={'pro-services__selections'}>
                  { chef.map(item =>
                    <FormControlLabel
                      key={item}
                      control={<Checkbox checked={state[item]} color={'primary'} onChange={handleChange} name={item} />}
                      label={removeUnderscore(item)}
                    />
                  )}
                </div>
              </div>
            )}
            
            { isnutri && (
              <div className={'pro-services__secondary-sub blocked'}>
                <h3>Nutritionist Abilities</h3>
                <div className={'pro-services__selections'}>
                  { nutri.map(item =>
                    <FormControlLabel
                      key={item}
                      control={<Checkbox checked={state[item]} color={'primary'} onChange={handleChange} name={item} />}
                      label={removeUnderscore(item)}
                    />
                  )}
                </div>
              </div>
            )}
            
            { ismassage && (
              <div className={'pro-services__secondary-sub blocked'}>
                <h3>Massage Abilities</h3>
                <div className={'pro-services__selections'}>
                  { massage.map( item =>
                    <FormControlLabel
                      key={item}
                      control={<Checkbox checked={state[item]} color={'primary'} onChange={handleChange} name={item} />}
                      label={removeUnderscore(item)}
                    />
                  )}
                </div>
              </div>
            )}
            
            <Button variant="contained" color="primary" onClick={submit}> Update</Button>

          </div>
        </div>
      )}

    </div>
  )
}