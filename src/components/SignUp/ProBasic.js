import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import defaultProfileImage from '../../assets/images/default-user.jpg';
import { auth, db } from '../Firebase/Firebase';

class ProBasic extends Component {
     constructor(props) {
          super(props);
          this.state = {
               name__first: '',
               name__last: '',
               email: '',
               email__repeat: '',
               password: '',
               password__repeat: '',
               gender__male: null,
               gender__female: null,
               address__street: '',
               address__street2: '',
               dob: '',
               termsPrivacy: false,
               isAdmin: false,
               error: null,
          }
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log(this.state.email, this.state.password);
          const { name__first, name__last, email, password, isAdmin } = this.state;
          auth.createUserWithEmailAndPassword(email, password)
               .then((user) => {
                    user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         isAdmin: isAdmin,
                         photoURL: defaultProfileImage
                    })
                         .then(function() {
                              console.log("Document successfully written!");
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                         
                    // See the UserRecord reference doc for the contents of userRecord.
                    console.log('Successfully created new user:', user.uid);
               })
               .catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log('Error code for creating new user:', errorCode);
                    console.log('Error msg for creating new user:', errorMessage);
               });
     }

     render(){
          return (
               <section>
                    <form className={'form'} onSubmit={ this.handleSubmit }>
                         <fieldset className={'form__container'}>
                              <legend>Creat Your Account</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__first'>First Name*</label>
                                        <input id='name__first' type='text' name='name__first' onChange={ this.handleChange } placeholder='First Name' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__last'>Last Name*</label>
                                        <input id='name__last' type='text' name='name__last' onChange={ this.handleChange } placeholder='Last Name' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } placeholder='Email' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='password'>Password</label>
                                        <input id='password' type='password' name='password' onChange={ this.handleChange } placeholder='Password' />
                                   </div>
                                   <p>By clicking Create Account, you agree to the <a href={'/terms-of-use'} target={'blank'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                              </div>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='dob'>Date of Birth*</label>
                                        <input id='dob' type='text' name='dob' onChange={ this.handleChange } placeholder='Date of Birth' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='gender'>Gender*</label>
                                        <div id="gender" className={'form__radio'}>
                                             <span>
                                                  <input id='gender__male' type='radio' name='gender__male' onChange={ this.handleChange } />
                                                  <label htmlFor='gender__male'>Male</label>
                                             </span>
                                             <span>
                                                  <input id='gender__female' type='radio' name='gender__female' onChange={ this.handleChange } />
                                                  <label htmlFor='gender__female'>Female</label>
                                             </span>
                                        </div>
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='address__street'>Street Address</label>
                                        <input id='address__street' type='text' name='address__street' onChange={ this.handleChange } placeholder='Street Address' />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='address__street2'>Address Line 2</label>
                                        <input id='address__street2' type='text' name='address__street2' onChange={ this.handleChange } placeholder='Address Line 2' />
                                   </div>
                              </div>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='city'>City</label>
                                        <input id='city' type='text' name='city' onChange={ this.handleChange } placeholder='City' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='state'>State / Province / Region</label>
                                        <input id='state' type='text' name='state' onChange={ this.handleChange } placeholder='State / Province / Region' />
                                   </div>
                              </div>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='zip'>Zip / Postal Code</label>
                                        <input id='zip' type='text' name='zip' onChange={ this.handleChange } placeholder='Zip / Postal Code' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='country'>Country</label>
                                        <select id='country'>
                                             <option value={'United States'} defaultValue>United States</option>
                                             <option value={'Bahamas'}>Bahamas</option>
                                        </select>
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='phone__mobile'>Mobile Phone Number</label>
                                        <input id='phone__mobile' type='text' name='phone__mobile' onChange={ this.handleChange } placeholder='Mobile Phone Number' />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='username'>Username</label>
                                        <input id='username' type='text' name='username' onChange={ this.handleChange } placeholder='Username' />
                                   </div>
                              </div>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='password__repeat'>Repeat Password</label>
                                        <input id='password__repeat' type='password' name='password__repeat' onChange={ this.handleChange } placeholder='Repeat Password' />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='instagram'>Your Instagram Profile URL</label>
                                        <input id='instagram' type='text' name='instagram' onChange={ this.handleChange } placeholder='Instagram Profile URL' />
                                   </div>
                              </div>
                         </fieldset>

                         <fieldset className={'form__container'}>
                              <legend>Terms of Use &amp; Privacy Policy</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email__repeat'>Repeat Email</label>
                                        <input id='email__repeat' type='email' name='email__repeat' onChange={ this.handleChange } placeholder='Repeat Email' />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <input id='termsPrivacy' type='checkbox' onChange={ this.handleChange } />
                                        <label htmlFor='termsPrivacy'>I have read and understand the <Link to='#'>Terms of Use</Link> &amp; <Link to='#'>Privacy Policy</Link></label>
                                   </div>
                              </div>
                         </fieldset>

                         <fieldset className={'form__container'}>
                              <legend>Main Training or Working Location</legend>
                         </fieldset>

                         <fieldset className={'form__container'}>
                              <legend>Professional Information</legend>
                         </fieldset>

                         <fieldset className={'form__container'}>
                              <legend>Tell us about you</legend>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='about-1'>Tell us about who you are, where you come from</label>
                                        <textarea id='about-1' name='about-1' onChange={ this.handleChange } ></textarea>
                                   </div>
                              </div>
                         </fieldset>
                         <div className={'form__submit'}>
                              <input type='submit' value='Sign Up' />
                         </div>
                    </form>
               </section>
          )
     }
}

export default ProBasic;