import React, { Component } from 'react';

class ProPremium extends Component {
     constructor(props) {
          super(props);
          this.state = {
               email: '',
               password: '',
               firstName: '',
               lastName: '',
               isAdmin: false,
               error: null,
          }
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log(this.state);
          const { email, password, firstName, lastName, isAdmin, error } = this.state;
     }

     render(){
          return (
               <section>
                    <form className={'form'} onSubmit={ this.handleSubmit }>
                         <div className={'form__row'}>
                              <label htmlFor='firstName'>First Name*</label>
                              <input id='firstName' type='text' name='firstName' onChange={ this.handleChange } placeholder='First Name' />
                         </div>
                         <div className={'form__row'}>
                              <label htmlFor='lastName'>Last Name*</label>
                              <input id='lastName' type='text' name='lastName' onChange={ this.handleChange } placeholder='Last Name' />
                         </div>
                         <div className={'form__row'}>
                              <label htmlFor='email'>Email*</label>
                              <input id='email' type='email' name='email' onChange={ this.handleChange } placeholder='Email' />
                         </div>
                         <div className={'form__row'}>
                              <label htmlFor='password'>Password*</label>
                              <input id='password' type='password' name='password' onChange={ this.handleChange } placeholder='Password' />
                         </div>
                         <div className={'form__submit'}>
                              <input type='submit' value='Sign Up' />
                         </div>
                    </form>
               </section>
          )
     }
}

export default ProPremium;