import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import CardError from "../Cards/CardError";


function rand() {
     return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
     const top = 50 + rand();
     const left = 50 + rand();

     return {
          top: `${top}%`,
          left: `${left}%`,
          transform: `translate(-${top}%, -${left}%)`,
     };
}

const useStyles = makeStyles((theme) => ({
     paper: {
          position: 'absolute',
          width:500,
     },
}));

function SimpleModal(props) {
     let o=props.open
     const classes = useStyles();
     // getModalStyle is not a pure function, we roll the style only on the first render
     const [modalStyle] = React.useState(getModalStyle);
     const handleClose = () => {
          props.close()
     };
     
     return (
          <div>
               <Modal
                    open={o}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
               >


                    <div style={modalStyle} className={classes.paper}>
                         <CardError handleClose={handleClose} errorbody={props.errorbody} style={{width:"100%"}} />
                    </div>       
               </Modal>
          </div>
     );
}

export default SimpleModal