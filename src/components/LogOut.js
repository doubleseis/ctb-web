import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { fire } from './Firebase/Firebase';

class LogOut extends Component {
     constructor(){
          super();
          this.state = {
               redirect: false 
          }
     }

     componentWillMount() {
          fire.auth().signOut().then((user) => {
               this.setState({ redirect: true })
          })
     }

     render() {
          if(this.state.redirect === true){
               return <Redirect to='/' />
          }
          return (
               <div>
                    <p>Logging Out</p>
               </div>
          )
     }

}

export default LogOut;