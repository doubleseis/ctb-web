import React, { Component } from 'react';
import Search from '../../components/Search/Search.js';

class PageHeader extends Component {
     constructor(props) {
          super(props);
          this.pageTitle = props.pageTitle;
          this.includeSearch = Boolean;
          this.background = {
               image: props.background.image,
               size: props.background.size,
               repeat: props.background.repeat,
               position: props.background.position,
               overlay: Boolean
          }
     }

     renderSearchHeading = () => {
          return(
               <div className={'search text-center'}>
                    <h2 className={'heading--lg'}>{ this.props.pageTitle }</h2>
                    <Search />
                    {/* <Link to="find-a-pro" className={'button button--contrast'}>Find Pros</Link> */}
               </div>
          );
     }

     renderTitleHeading = () => {
          return(
               <h1 className={'heading--xl'}>{ this.props.pageTitle }</h1>
          );
     }

     render() {
          const pageHeaderStyle = {
               backgroundImage: 'url(' + this.props.background.image + ')',
               backgroundSize: this.props.background.size,
               backgroundRepeat: this.props.background.repeat
          }
          return (
               <section style={ pageHeaderStyle } className={this.props.includeSearch === true ? 'page-header page-header--search' : 'page-header'}>
                    <div className={'container page-header__container'}>
                         {this.props.includeSearch ? this.renderSearchHeading() : this.renderTitleHeading() }
                    </div>

                    <div className={ this.props.background.overlay === true ? 'page-header__overlay' : '' }></div>
               </section>
          );
     }
}
   
export default PageHeader;