import { faFacebookSquare, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import defaultProfileImage from '../../assets/images/default-user.jpg';
import { auth, db, fb, google } from '../Firebase/Firebase';


class SignUpForm extends Component {
     constructor(props) {
          super(props);
          this.state = {
               name__first: '',
               name__last: '',
               email: '',
               username:"",
               password: '',
               isAdmin: false,
               user: null
          }
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log(this.state.email, this.state.password);
          const { name__first, name__last, email, password, isAdmin } = this.state;
          auth.createUserWithEmailAndPassword(email, password)
               .then((user) => {
                    user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         username:"user-"+Math.random().toString(36).substr(2, 9),
                         isAdmin: isAdmin,
                         photoURL: defaultProfileImage
                    })
                         .then(function() {
                              console.log("Document successfully written!");
                              window.location.href = "/dashboard";
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                         
                    // See the UserRecord reference doc for the contents of userRecord.
                    // console.log('Successfully created new user:', user.uid);
               })
               .catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log('Error code for creating new user:', errorCode);
                    console.log('Error msg for creating new user:', errorMessage);
                    alert(errorMessage);
               });
     }

     handleFacebookLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(fb)
               .then(({ user }) => {
                    this.setState({ user })
                    // user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         username:"user-"+Math.random().toString(36).substr(2, 9),
                         isAdmin: isAdmin,
                         photoURL: defaultProfileImage,
                         finishedOnboarding: false
                    })
                    db.collection('users').doc(user.uid).collection('social').doc('following').set({
                         accounts: []
                    }).then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/dashboard";
                    })
               })
     }

     handleGoogleLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(google)
               .then(function(result) {
                    var token = result.credential.accessToken;
                    var user_id = result.user.uid;
                    var user_first_name = result.additionalUserInfo.profile.given_name;
                    var user_last_name = result.additionalUserInfo.profile.family_name;
                    var user_image_url = result.additionalUserInfo.profile.picture;
                    var user_creation_time = result.user.creationTime;

                    console.log(result);
               
                    db.collection('users').doc(user_id).set({
                         uid: user_id,
                         firstName: user_first_name,
                         lastName: user_last_name,
                         username:"user-"+Math.random().toString(36).substr(2, 9),
                         isAdmin: isAdmin,
                         finishedOnboarding: false,
                         photoURL: user_image_url,
                    })
                    db.collection('users').doc(user_id).collection('social').doc('following').set({
                         accounts: []
                    }).then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/dashboard";
                    })
               })
               .catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...
                    console.error("Error: ", error);
               });
     }

     render(){
          return (
               <section className={'form__outer-container'}>
                    <form className={'form'} onSubmit={ this.handleSubmit }>
                         <fieldset className={'form__container'}>
                              <legend>Create Your Account</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__first'>First Name*</label>
                                        <input id='name__first' type='text' name='name__first' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__last'>Last Name*</label>
                                        <input id='name__last' type='text' name='name__last' onChange={ this.handleChange } />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='password'>Password</label>
                                        <input id='password' type='password' name='password' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'disclaimer'}>
                                        <p>By clicking Create Account, you agree to the <a href={'/terms-of-use'} target={'blank'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                                   </div>
                              </div>
                         </fieldset>
                         
                         <div className={'form__submit'}>
                              <input className={'button button--secondary'} type='submit' value='Create Account' />
                         </div>
                    </form>

                    <div className={'divider'}>
                         
                    </div>
                    
                    <div className={'social-logins'}>
                         <div className={'disclaimer social-logins__disclaimer'}>
                              <p>By clicking Sign Up with Facebook or Sign Up with Google, you agree to the <a href={'/terms-of-use'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                         </div>
                         <div className={'social-logins__buttons'}>
                              <button className={'button button--secondary button--facebook'} onClick={this.handleFacebookLogin}><FontAwesomeIcon icon={faFacebookSquare} /> Sign Up with Facebook</button>
                              <button className={'button button--secondary button--google'} onClick={this.handleGoogleLogin}><FontAwesomeIcon icon={faGoogle} /> Sign Up with Google</button>
                         </div>
                    </div>

               </section>
          )
     }
}
   
export default SignUpForm;