import React, { Component } from 'react';
import { auth } from '../Firebase/Firebase';

class ResetPassword extends Component {
     constructor(props) {
          super(props);
          this.state = {
               email: '',
               modal: false,
          }
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          const { email } = this.state;
          if( email === '' ) {
               alert('Add an email');
               return false;
          }

          auth.sendPasswordResetEmail(email)
               .then(() => {
                    console.log('email sent');
                    var form = document.getElementById('resetForm');
                    form.classList.add('email-sent');
                    setTimeout(function(){
                         form.classList.remove('email-sent');
                    }, 5000);
               })
               .catch(function(error) {
                    console.log('Error', error);
                    if(error.code == 'auth/user-not-found') {
                         var form = document.getElementById('resetForm');
                         form.classList.add('error');
                         setTimeout(function(){
                              form.classList.remove('error');
                         }, 5000);
                    }
               });
     }

     render(){
          return (
               <section className={'form__outer-container'}>
                    <form id={'resetForm'} className={'form'} onSubmit={ this.handleSubmit }>
                         <fieldset className={'form__container'}>
                              <legend>Forgot Password</legend>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } />
                                   </div>
                              </div>
                         </fieldset>
                         
                         <div className={'form__submit'}>
                              <input className={'button button--secondary'} type='submit' value='Send Reset Password Email' />
                         </div>

                         <div class="email-message">
                              <div class="email-message__container">
                                   <p>Email has been sent. Please check your inbox.</p>
                              </div>
                              <div class="email-message__container-error">
                                   <p>This email does not exist in our database. Contact us if you feel this is an error.</p>
                              </div>
                         </div>
                    </form>

               </section>
          )
     }
}
   
export default ResetPassword;