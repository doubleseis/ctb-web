import React, { Component } from 'react';

class UserProfileUpdate extends Component {
     // Current Working
     // This component
     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          // console.log(this.state.email, this.state.password);
          // const { name__first, name__last, email, password, isAdmin } = this.state;
          // auth.currentUser(email, password)
          //      .then((user) => {
          //           user = auth.currentUser
          //           db.collection('users').doc(user.uid).set({
          //                firstName: name__first,
          //                lastName: name__last,
          //                isAdmin: isAdmin
          //           })
          //                .then(function() {
          //                     console.log("Document successfully written!");
          //                     window.location.href = "/dashboard";
          //                })
          //                .catch(function(error) {
          //                     console.error("Error writing document: ", error);
          //                });
                         
          //           // See the UserRecord reference doc for the contents of userRecord.
          //           // console.log('Successfully created new user:', user.uid);
          //      })
          //      .catch(function(error) {
          //           var errorCode = error.code;
          //           var errorMessage = error.message;
          //           console.log('Error code for creating new user:', errorCode);
          //           console.log('Error msg for creating new user:', errorMessage);
          //           alert(errorMessage);
          //      });
     }

     render() {
          return (
               <section className={'user-profile'}>
                    <form className={'user-profile__form'}>
                         <fieldset className={'form__container'}>
                              <legend>Update Profile</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__first'>First Name*</label>
                                        <input id='name__first' type='text' name='name__first' onChange={ this.handleChange } value={ this.props.data.firstName } placeholder='First Name' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__last'>Last Name*</label>
                                        <input id='name__last' type='text' name='name__last' onChange={ this.handleChange } placeholder='Last Name' />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } placeholder='Email' />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='password'>Password</label>
                                        <input id='password' type='password' name='password' onChange={ this.handleChange } placeholder='Password' />
                                   </div>
                                   <p>By clicking Create Account, you agree to the <a href={'/terms-of-use'} target={'blank'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                              </div>
                         </fieldset>
                         
                         <div className={'form__submit'}>
                              <input type='submit' value='Sign Up' />
                         </div>
                    </form>
               </section>

          );
     }
}
   
export default UserProfileUpdate;