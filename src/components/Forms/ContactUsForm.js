import React, { Component } from 'react';
import { auth, db, fb } from '../Firebase/Firebase';

class ContactUsForm extends Component {
     constructor(props) {
          super(props);
          this.state = {
               name__first: '',
               name__last: '',
               email: '',
               password: '',
               isAdmin: false,
               user: null
          }
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log(this.state.email, this.state.password);
          const { name__first, name__last, email, password, isAdmin } = this.state;
          auth.createUserWithEmailAndPassword(email, password)
               .then((user) => {
                    user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         isAdmin: isAdmin
                    })
                         .then(function() {
                              console.log("Document successfully written!");
                              window.location.href = "/dashboard";
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                         
                    // See the UserRecord reference doc for the contents of userRecord.
                    // console.log('Successfully created new user:', user.uid);
               })
               .catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log('Error code for creating new user:', errorCode);
                    console.log('Error msg for creating new user:', errorMessage);
                    alert(errorMessage);
               });
     }

     handleFacebookLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(fb)
               .then(({ user }) => {
                    this.setState({ user })
                    // user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         isAdmin: isAdmin
                    })
                    .then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/dashboard";
                    })
               })
               // .catch(function(error) {
               //      var errorCode = error.code;
               //      var errorMessage = error.message;
               //      console.log('Error code for creating new user:', errorCode);
               //      console.log('Error msg for creating new user:', errorMessage);
               //      alert(errorMessage);
               // });
     }

     render(){
          return (
               <section className={'form__outer-container'}>
                    <form className={'form'} onSubmit={ this.handleSubmit }>
                         <fieldset className={'form__container'}>
                              <legend>Contact Us</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__first'>First Name*</label>
                                        <input id='name__first' type='text' name='name__first' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__last'>Last Name*</label>
                                        <input id='name__last' type='text' name='name__last' onChange={ this.handleChange } />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='password'>Message</label>
                                        <textarea onChange={ this.handleChange }></textarea>
                                   </div>
                              </div>
                         </fieldset>
                         
                         <div className={'form__submit'}>
                              <input className={'button button--secondary'} type='submit' value='Send Message' />
                         </div>
                    </form>

               </section>
          )
     }
}
   
export default ContactUsForm;