import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import SimpleModal from "../ModalSecondary/modal";

const useStyles = makeStyles({
  root: {
    maxWidth: 600,
  },
  media: {
    height: 250,
  },
});

export default function MediaCard() {
  const classes = useStyles();
  const [open, setopen] = React.useState(false);
  console.log(open);

  const close = () => {
    setopen(false);
  };

  return (
    <div>
      <Card className={classes.root}>
        <CardActionArea>
          <CardContent style={{ margin: "auto" }}>
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              style={{ marginLeft: 10 }}
            >
              DELETE ACCOUNT
            </Typography>

            <Typography variant="body2" color="textSecondary" component="p">
              ARE YOU SURE DELETE ACCOUNT ?
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button
            variant="contained"
            color="secondary"
            style={{ margin: "0 37%" }}
            onClick={() => {
              setopen(!open);
            }}
          >
            DELETE
          </Button>
        </CardActions>
      </Card>
      <SimpleModal open={open} close={close}></SimpleModal>
    </div>
  );
}
