import React from "react";
import Image from '../Profile/Image';

const userCard =({profile})=>{

     const professions = [];
     if(profile.professions.FitnessTrainer) { professions.push('Fitness Trainer'); }
     if(profile.professions.Chef) { professions.push('Chef'); }
     if(profile.professions.MassageThreapist) { professions.push('Massage Therapist'); }
     if(profile.professions.Nutritionist) { professions.push('Nutritionist'); }
     const renderProfessions = professions.join(', ');

     return(
          <div className={ `p-card` } key={profile.uid}>
               <div className={ 'p-card__outer' }>
                    <div className={ 'p-card__inner p-card__inner--image' }>
                         <a href={`/pro/${profile.username}`}>
                              <Image image={ profile.photoURL } user={ profile.uid } />
                         </a>
                    </div>
                    <div className={ 'p-card__inner' }>
                         <div className={ 'p-card__title' }>
                              <h2>{ profile.firstName } { profile.lastName }</h2>
                         </div>
                         <div className={ 'p-card__body' }>
                              <strong>Professions: </strong> { renderProfessions }
                         </div>
                         <div className={ 'p-card__button' }>
                              <a href={`/pro/${profile.username}`}>View Profile</a>
                         </div>
                    </div>
               </div>
          </div>
     )
}

export default userCard