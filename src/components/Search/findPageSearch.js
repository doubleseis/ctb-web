import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import React from 'react';
import { connect } from "react-redux";
import { removeUnderscore } from '../Helpers';

function BasicTextFields({ set, list, checkedlist }) {


  const [ischef, setischef] = React.useState(false)
  const [isnutri, setisnutri] = React.useState(false)
  const [isfitness, setisfitness] = React.useState(false)
  const [ismassage, setismassage] = React.useState(false)
  const [property, setproperty] = React.useState({})
  const [value, setvalue] = React.useState("")

  const [Fitness] = React.useState(
    [
      "competition_prep",
      "power_lifting",
      "weight_loss",
      "body_fat_loss",
      "size_gaining",
      "endurance_training",
      "forming_and_toning",
      "flexibility",
      "aerobic_fitness",
      "pregnancy",
      "rehabilitation",
      "pilates",
      "yoga",
      "athletic"
    ]
  )

  const [nutri] = React.useState([
    "normal_nutrition",
    "vegan_or_vegetarian",
    "paleo",
    "public_health",
    "sports",
    "pediatric",
    "diabetes",
    "heart_health",
    "autoimmune_disease",
    "food_allergies",
  ])

  const [massage] = React.useState([
    "deep_tissue",
    "swedish",
    "stone",
    "thai",
    "pregnancy",
    "sports",
    "reflexology",
  ])

  const [chef] = React.useState([
    "seafood",
    "american",
    "breakfast_or_brunch",
    "international",
    "southern",
    "healthy",
    "desserts",
    "juices_and_smoothies"
  ])

  React.useEffect(() => {
    setischef(checkedlist.ischef)
    setisnutri(checkedlist.isnutri)
    setisfitness(checkedlist.isfitness)
    setismassage(checkedlist.ismassage)
    setproperty(checkedlist.property)
  }, [])

  const handleChangechef = (event) => {
    setischef(!ischef)
  };

  const letfilter = (val) => {
    // Variable to hold the original version of the list
    let currentList = [];
    // Variable to hold the filtered list before putting into state
    let newList = [];
    // If the search bar isn't empty
    if (val !== "") {
      // Assign the original list to currentList
      currentList = list;
      // Use .filter() to determine which items should be displayed
      // based on the search terms
      newList = currentList.filter(item => {
        // change current item to lowercase
        const prousername = item.firstName.toLowerCase();
        const prouserlastname = item.lastName.toLowerCase();
        // change search term to lowercase
        const fullname = `${prousername} ${prouserlastname}`
        // change search term to lowercase
        const filter = val.toLowerCase();
        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content
        return fullname.includes(filter);
      });
    } else {
      // If the search bar is empty, set newList to original task list
      newList = list;
    }

    // console.log(newList)

    if (Object.keys(property).length > 0) {

      const propArray = Object.keys(property)
      // console.log(propArray);
      
      for (let index = 0; index < propArray.length; index++) {
        newList = newList.filter(item2 => {
          if (!property[propArray[index]]) {
            // console.log('item2:',item2)
            return true
          } else {
            if( typeof(item2.Ability) == 'object' ) {
              // console.log('we are closer!');
              return item2.Ability[propArray[index]];
            }
          }
        })
      }
      // console.log(newList)

      if (isnutri) {
        newList = newList.filter(item => item.professions.Nutritionist)
      }
      if (ischef) {
        newList = newList.filter(item => item.professions.Chef)
      }
      if (isfitness) {
        newList = newList.filter(item => item.professions.FitnessTrainer)
      }
      if (ismassage) {
        newList = newList.filter(item => item.professions.MassageThreapist)

      }
      console.log(newList)
    }

    console.log(newList)

    set(newList)
  }

  React.useEffect(() => {
    letfilter(value)

  }, [isnutri]);
  React.useEffect(() => {
    letfilter(value)

  }, [ischef]);
  React.useEffect(() => {
    letfilter(value)
  }, [isfitness]);
  React.useEffect(() => {
    letfilter(value)
  }, [ismassage]);
  React.useEffect(() => {
    letfilter(value)
    console.log(property)
  }, [property]);
  React.useEffect(() => {
    letfilter(value)
  }, [value]);

  const handleChangenutri = (event) => {
    setisnutri(!isnutri)
  };

  const handleChangefitness = (event) => {
    setisfitness(!isfitness)
  };

  const handleChangemasage = (event) => {
    setismassage(!ismassage)
  };

  const handleChange2 = (event) => {
    setproperty({ ...property, [event.target.name]: event.target.checked });
  };

  const handleChange = (e) => {
    setvalue(e.target.value)
  }

  console.log(property)

  const isFitnessActive = isfitness ? '' : 'search__filter--hidden';
  const isChefActive = ischef ? '' : 'search__filter--hidden';
  const isNutriActive = isnutri ? '' : 'search__filter--hidden';
  const isMassageActive = ismassage ? '' : 'search__filter--hidden';

  return (
    <div className="search">
      <div className="search__container">
        <div className="search__input">
          <input type='text' placeholder={'Search by Name'} className="searchs" onChange={handleChange} />
          <FontAwesomeIcon icon={faSearch} />
        </div>
        <div className={'search__specialties'}>
          <div className={'search__specialty'}>
            <label>Fitness Trainer</label>
            <input type={'checkbox'} name="isfitness" tabIndex={'1'} onChange={handleChangefitness} checked={isfitness} />
          </div>
          <div className={'search__specialty'}>
            <label>Nutritionist</label>
            <input type={'checkbox'} name="isnutri" tabIndex={'1'} onChange={handleChangenutri} checked={isnutri} />
          </div>
          <div className={'search__specialty'}>
            <label>Massage Therapist</label>
            <input type={'checkbox'} name="ismassage" tabIndex={'1'} onChange={handleChangemasage} checked={ismassage} />
          </div>
          <div className={'search__specialty'}>
            <label>Chef</label>
            <input type={'checkbox'} name="ischef" tabIndex={'1'} onChange={handleChangechef} checked={ischef} />
          </div>
        </div>
        <div className="seach__filters">
        
          <div className={ 'search__filter search__filter--fitness ' + isFitnessActive }>
            <h3 className={ 'heading heading--md' }>Fitness Specialties</h3>
            { isfitness && Fitness.map(item => {
              return (
                <div className="search__filter-options">
                  <FormControlLabel
                    key={item}
                    control={ <Checkbox checked={property[item]} color={'primary'} onChange={handleChange2} name={item} /> }
                    label={ removeUnderscore(item) }
                  />
                </div>
              )
            })}
          </div>

          <div className={ 'search__filter search__filter--chef ' + isChefActive }>
            <h3 className={ 'heading heading--md' }>Chef Specialties</h3>
            { ischef && chef.map(item => {
              return (
                <div className="search__filter-options">
                  <FormControlLabel
                    key={item}
                    control={<Checkbox checked={property[item]} onChange={handleChange2} name={item} />}
                    label={removeUnderscore(item)}
                  />
                </div>
              )
            })}
          </div>

          <div className={ 'search__filter search__filter--nutri ' + isNutriActive }>
            <h3 className={ 'heading heading--md' }>Nutritionist Specialties</h3>
            { isnutri && nutri.map(item => {
              return (
                <div className="search__filter-options">
                  <FormControlLabel
                    key={item}
                    control={<Checkbox checked={property[item]} onChange={handleChange2} name={item} />}
                    label={removeUnderscore(item)}
                  />
                </div>
              )
            })}
          </div>

          <div className={ 'search__filter search__filter--massage ' + isMassageActive }>
            <h3 className={ 'heading heading--md' }>Massage Specialties</h3>
            { ismassage && massage.map(item => {
              return (
                <div className="search__filter-options">
                  <FormControlLabel
                    key={item}
                    control={<Checkbox color={'primary'} checked={property[item]} onChange={handleChange2} name={item} />}
                    label={removeUnderscore(item)}
                  />
                </div>
              )
            })}
          </div>
          
        </div>
      </div>
    </div>
  );
}

const statetoprops = (state) => ({
  prouser: state.prouser.prouser,
  checkedlist: state.prouser.checked

})

export default connect(statetoprops)(BasicTextFields)