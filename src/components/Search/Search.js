import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { addchecked, addfilteredusers, addprousers } from "../../redux/reducers/proUser.reducer";
import { db } from "../Firebase/Firebase";
import { removeUnderscore } from '../Helpers';

class Search extends Component {

     constructor(props) {
          super(props);
          this.fitness = [
               "competition_prep",
               "power_lifting",
               "weight_loss",
               "body_fat_loss",
               "size_gaining",
               "endurance_training",
               "forming_and_toning",
               "flexibility",
               "aerobic_fitness",
               "pregnancy",
               "rehabilitation",
               "pilates",
               "yoga",
               "athletic"
          ]
          
          this.nutri = [
               "normal_nutrition",
               "vegan_or_vegetarian",
               "paleo",
               "public_health",
               "sports",
               "pediatric",
               "diabetes",
               "heart_health",
               "autoimmune_disease",
               "food_allergies",
          ]

          this.massage = [
               "deep_tissue",
               "swedish",
               "stone",
               "thai",
               "pregnancy",
               "sports",
               "reflexology",
          ]

          this.chef = [
               "seafood",
               "american",
               "breakfast_or_brunch",
               "international",
               "southern",
               "healthy",
               "desserts",
               "juices_and_smoothies"
          ]

          this.state = {
               property: {},
               users: [],
               prousers: [],
               filteredusers: [],
               value: "",
               isFitnessTrainercheck: false,
               isNutritionistcheck: false,
               isMassageThreapistcheck: false,
               isChefcheck: false
          }
          this.props = props
          this.userArray = []
          this.prosArray = []
          this.handleSubmit = this.handleSubmit.bind(this);
     }

     componentDidMount() {
          const datas = db.collection('users')
          const doc = datas.get().then(snapshot => {
               snapshot.forEach(doc => {
                    let a = doc.data()
                    this.userArray.push(a)
               });
          }).then(() => {
               this.prosArray = this.userArray.filter(item => item.isPro == true)
               this.props.addprousers(this.prosArray)
               this.setState({ users: this.userArray, prousers: this.prosArray })
          })
          .catch(e => console.log(e))
          console.log(this.userArray)
     }

     handleChange2 = (event) => {
          this.setState({ property: { ...this.state.property, [event.target.name]: event.target.checked } });
     };

     handleChange = (e) => {
          this.setState({
               value: e.target.value
          })
     }

     handleFitnessChange = e => {
          this.setState({
               isFitnessTrainercheck: e.target.checked
          });
     };

     handleNutritionistChange = e => {
          this.setState({
               isNutritionistcheck: e.target.checked
          });
     };

     handleMassageTherapistChange = e => {
          this.setState({
               isMassageThreapistcheck: e.target.checked
          });
     };

     handleChefChange = e => {
          this.setState({
               isChefcheck: e.target.checked
          });
     };

     handleit = (e) => {
          const isfitness = this.state.isFitnessTrainercheck
          const ischef = this.state.isChefcheck
          const ismassage = this.state.isMassageThreapistcheck
          const isnutri = this.state.isNutritionistcheck

          const checkedArray = { isfitness, ischef, ismassage, isnutri }

          e.preventDefault()
          const Prousers = this.state.prousers
          const value = this.state.value.toLowerCase()
          //const name = value.split(" ")[0]
          // const lastname = value.split(" ")[1]

          // console.log(Prousers)

          let filtered = Prousers.filter(item => {
               const prousername = item.firstName && item.firstName.toLowerCase()
               const prouserlastname = item.lastName && item.lastName.toLowerCase()
               // change search term to lowercase
               const fullname = `${prousername} ${prouserlastname}`
               console.log(fullname)
               if (fullname.includes(value)) {
                    return true
               }
               return false
          })

          if (this.state.isNutritionistcheck) {
               filtered = filtered.filter(item => item.professions.Nutritionist)
          }
          if (this.state.isChefcheck) {
               filtered = filtered.filter(item => item.professions.Chef)
          }
          if (this.state.isFitnessTrainercheck) {
               filtered = filtered.filter(item => item.professions.FitnessTrainer)
          }
          if (this.state.isMassageThreapistcheck) {
               filtered = filtered.filter(item => item.professions.MassageThreapist)
          }

          if (filtered.length > 0) {
               if (Object.keys(this.state.property).length > 0) {
                    const propArray = Object.keys(this.state.property)
                    for (let index = 0; index < propArray.length; index++) {
                         filtered = filtered.filter(item2 => {
                              return item2.Ability[propArray[index]]
                         })
                    }
               }
               this.props.filteredprousers(filtered)
               this.props.addprousers(this.state.prousers)
               this.props.addcheckedlist({ ...checkedArray, property: this.state.property })

          } else {
               this.props.filteredprousers("null")
               this.props.addprousers(this.state.prousers)
               this.props.addcheckedlist({ ...checkedArray, property: this.state.property })
          }
          this.props.history.push("/find-a-pro")
     }


     handleSubmit = (e) => {
          e.preventDefault();
          var query = this.state.search !== '' ? this.state.search : '';
          var nutritionist = this.state.nutritionist ? `nutritionist=${this.state.nutritionist}` : `nutritionist=false`;
          var fitnessTrainer = this.state.fitnessTrainer ? `fitnessTrainer=${this.state.fitnessTrainer}` : `fitnessTrainer=false`;
          var massageTherapist = this.state.massageTherapist ? `massageTherapist=${this.state.massageTherapist}` : `massageTherapist=false`;
          var chef = this.state.chef ? `chef=${this.state.chef}` : `chef=false`;
          // console.log(chef);
          window.location.href = `/find-a-pro?s=${query}&${chef}&${fitnessTrainer}&${nutritionist}&${massageTherapist}`;
     }

     render() {

          console.log(this.state)
          return (
               <section>
                    <form className={'search__compact'}>
                         {/* <div className={'search__input'}>
                              <input type='text' onChange={this.handleChange} placeholder={'Search by Name'} />
                              <button onClick={this.handleSubmit}>
                                   <FontAwesomeIcon icon={faSearch} />
                              </button>
                         </div> */}
                         {/* <input type='text' placeholder='Zip Code' /> */}
                         <div className={'search__specialties'}>
                              <div className={'search__specialty'}>
                                   <label>Fitness Trainer</label>
                                   <input type={'checkbox'} name="isFitnessTrainercheck" tabIndex={'1'} onChange={this.handleFitnessChange} checked={this.state.isFitnessTrainercheck} />
                              </div>
                              <div className={'search__specialty'}>
                                   <label>Nutritionist</label>
                                   <input type={'checkbox'} name="isNutritionistcheck" tabIndex={'1'} onChange={this.handleNutritionistChange} defaultChecked={this.state.isNutritionistcheck} />
                              </div>
                              <div className={'search__specialty'}>
                                   <label>Massage Therapist</label>
                                   <input type={'checkbox'} name="isMassageThreapistcheck" tabIndex={'1'} onChange={this.handleMassageTherapistChange} defaultChecked={this.state.isMassageThreapistcheck} />
                              </div>
                              <div className={'search__specialty'}>
                                   <label>Chef</label>
                                   <input type={'checkbox'} name="isChefcheck" tabIndex={'1'} onChange={this.handleChefChange} defaultChecked={this.state.isChefcheck} />
                              </div>
                         </div>
                         {
                              this.state.isFitnessTrainercheck &&
                              this.fitness.map(item => {
                                   return (
                                        <FormControlLabel
                                             key={item}
                                             control={<Checkbox color={'primary'} checked={this.state.property[item]} onChange={this.handleChange2} name={item} />}
                                             label={ removeUnderscore(item) }
                                        />

                                   )
                              })
                         }
                         {
                              this.state.isChefcheck &&
                              this.chef.map(item => {
                                   return (
                                        <FormControlLabel
                                             key={item}
                                             control={<Checkbox color={'primary'} checked={this.state.property[item]} onChange={this.handleChange2} name={item} />}
                                             label={ removeUnderscore(item)}
                                        />
                                   )
                              })
                         }
                         {
                              this.state.isNutritionistcheck &&
                              this.nutri.map(item => {
                                   return (
                                        <FormControlLabel
                                             key={item}
                                             control={<Checkbox color={'primary'} checked={this.state.property[item]} onChange={this.handleChange2} name={item} />}
                                             label={ removeUnderscore(item) }
                                        />

                                   )
                              })
                         }
                         {
                              this.state.isMassageThreapistcheck &&
                              this.massage.map(item => {
                                   return (
                                        <FormControlLabel
                                             key={item}
                                             control={<Checkbox color={'primary'} checked={this.state.property[item]} onChange={this.handleChange2} name={item} />}
                                             label={ removeUnderscore(item) }
                                        />
                                   )
                              })
                         }
                         <div className={'form__submit'}>
                              <button className={'button button--contrast'} onClick={this.handleit}>Find Pro</button>
                         </div>
                    </form>
               </section>
          )
     };
}


const dispatchtostate = dispatch => (
     {
          addprousers: (Array) => dispatch(addprousers(Array)),
          filteredprousers: (Array) => dispatch(addfilteredusers(Array)),
          addcheckedlist: (p) => dispatch(addchecked(p))
     }
)

export default withRouter(connect(null, dispatchtostate)(Search))