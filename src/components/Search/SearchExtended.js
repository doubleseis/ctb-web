import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { db } from '../Firebase/Firebase';
import Image from '../Profile/Image';

class SearchExtended extends Component {
     constructor(props) {
          super(props);
          this.state = {
               search: '',
               data: [],
               fitnessTrainer: Boolean,
               nutritionist: Boolean,
               massageTherapist: Boolean,
               chef: Boolean
          };
          this.renderSpecialties = this.renderSpecialties.bind(this);
          this.updateSearchFromURL = this.updateSearchFromURL.bind(this);
     }

     updateSearchFromURL = () => {
          // const { search, fitnessTrainer, nutritionist, massageTherapist, chef } = this.state;
          var vars = {};
          var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
               vars[key] = value;
          });

          this.setState({
               search: vars.s !== undefined ? vars.s : '',
               fitnessTrainer: vars.fitnessTrainer !== undefined ? (vars.fitnessTrainer === 'true' ? true : false)  : true,
               nutritionist: vars.nutritionist !== undefined ? (vars.nutritionist === 'true' ? true : false) : true,
               massageTherapist: vars.massageTherapist !== undefined ? (vars.massageTherapist === 'true' ? true : false) : true,
               chef: vars.chef !== undefined ? (vars.chef === 'true' ? true : false) : true
          })
          // console.log('function: update from url');
     }

     renderSpecialties = (profile) => {
          const { professions } = profile;
          var professionsArray = professions.toString().split(',');
          var professionDisplay = [];
          professionsArray.map((profession) => {
               var professionToString = profession.replace( /([A-Z])/g, " $1" );
               var professionToSentenceCase = ' ' + professionToString.charAt(0).toUpperCase() + professionToString.slice(1);
               professionDisplay.push(professionToSentenceCase);
          });
          // console.log('specialty rendered');
          return ReactHtmlParser(`<p><strong>Main:</strong> ${professionDisplay}</p>`);
     }

     componentDidMount() {
          db.collection('users').get()
               .then(function(querySnapshot) {
                    if (querySnapshot.docs.length !== 0) {
                         const users = querySnapshot.docs;
                         users.map(doc => {
                              // console.log(doc.id);
                              this.setState({
                                   data: [...this.state.data, {
                                        uid: doc.id,
                                        about: doc.data().about,
                                        background: doc.data().background,
                                        certifications: doc.data().certifications,
                                        favQuote: doc.data().favQuote,
                                        firstName: doc.data().firstName,
                                        funFact: doc.data().funFact,
                                        isAdmin: doc.data().isAdmin,
                                        isApproved: doc.data().isApproved,
                                        isPro: doc.data().isPro,
                                        lastName: doc.data().lastName,
                                        photoURL: doc.data().photoURL,
                                        professions: doc.data().professions,
                                        ratesInPerson: doc.data().ratesInPerson,
                                        ratesOnline: doc.data().ratesOnline
                                   }]
                              });
                              return;
                         })
                         
                    } else {
                         this.setState({ data: {} });
                         // console.log("No such document!");
                    }
               }.bind(this))
               .catch(function (error) {
                    this.setState({ data: {} });
                    // console.log("Error getting document:", error);
               }.bind(this));
          
          // window.addEventListener('load', this.updateSearchFromURL() );
          this.updateSearchFromURL()
     }

     renderProfile = (profile, index) => {          
          const { search, fitnessTrainer, nutritionist, massageTherapist, chef } = this.state;
          // var isFitnessTrainer = profile.professions.includes('fitnessTrainer') ? (profile.professions.includes('fitnessTrainer') !== fitnessTrainer ? 'hide' : 'show') : '' ;
          // var isNutritionist = profile.professions.includes('nutritionist') ? (profile.professions.includes('nutritionist') !== nutritionist ? 'hide' : 'show') : '' ;
          // var isMassageTherapist = profile.professions.includes('massageTherapist') ? (profile.professions.includes('massageTherapist') !== massageTherapist ? 'hide' : 'show') : '' ;
          // var isChef = profile.professions.includes('chef') ? (profile.professions.includes('chef') !== chef ? 'hide' : 'show') : '' ;

          if(profile.isPro === true && profile.isApproved === true){
               return (
                    // <div className={ `p-card ${isFitnessTrainer} ${isNutritionist} ${isMassageTherapist} ${isChef}` } key={index}>
                    <div className={ `p-card` } key={index}>
                         <div className={ 'p-card__outer' }>
                              <div className={ 'p-card__inner p-card__inner--image' }>
                                   <Image image={ profile.photoURL } user={ profile.uid } />
                              </div>
                              <div className={ 'p-card__inner' }>
                                   <div className={ 'p-card__title' }>
                                        <h2>{ profile.firstName } { profile.lastName }</h2>
                                   </div>
                                   <div className={ 'p-card__body' }>
                                        { this.renderSpecialties(profile) }
                                        <a href={`/pro/${profile.firstName}-${profile.lastName}`}>View Profile</a>
                                   </div>
                              </div>
                         </div>
                    </div>
               );
          }
     }

     handleChange = e => {
          this.setState({
               search: e.target.value,
          });
     };

     handleFitnessChange = e => {
          this.setState({
               fitnessTrainer: e.target.checked
          });
     };

     handleNutritionistChange = e => {
          this.setState({
               nutritionist: e.target.checked
          });
     };

     handleMassageTherapistChange = e => {
          this.setState({
               massageTherapist: e.target.checked
          });
     };

     handleChefChange = e => {
          this.setState({
               chef: e.target.checked
          });
     };

     render() {
          const { search, data } = this.state;
          const filteredProfiles = data.filter(profile => {               
               var first_name = profile.firstName ? profile.firstName.toLowerCase().indexOf(search.toLowerCase()) !== -1 : '';
               var last_name = profile.lastName ? profile.lastName.toLowerCase().indexOf(search.toLowerCase()) !== -1 : '';
               return ( first_name || last_name )
          });
          // this.updateSearchFromURL();
          
          return (
               <section className={ 'search' }>
                    <div className={ 'container search__container' }>
                         <div className={ 'search__input' }>
                              <input type={'text'} label={'Search Profiles'} onChange={this.handleChange} defaultValue={this.state.search} placeholder={'Search by Name'} />
                              <div className={'search__specialties'}>
                                   <div className={'search__specialty'}>
                                        <label>Fitness Trainer</label>
                                        <input type={'checkbox'} tabIndex={'1'} onChange={this.handleFitnessChange} defaultChecked={ this.state.fitnessTrainer } />
                                   </div>
                                   <div className={'search__specialty'}>
                                        <label>Nutritionist</label>
                                        <input type={'checkbox'} tabIndex={'1'} onChange={this.handleNutritionistChange} defaultChecked={ this.state.nutritionist } />
                                   </div>
                                   <div className={'search__specialty'}>
                                        <label>Massage Therapist</label>
                                        <input type={'checkbox'} tabIndex={'1'} onChange={this.handleMassageTherapistChange} defaultChecked={this.state.massageTherapist} />
                                   </div>
                                   <div className={'search__specialty'}>
                                        <label>Chef</label>
                                        <input type={'checkbox'} tabIndex={'1'} onChange={this.handleChefChange} defaultChecked={this.state.chef} />
                                   </div>
                              </div>
                         </div>
                         <div className={'search__results'}>
                              { filteredProfiles.map((profile, index) => {
                                   return (
                                        this.renderProfile(profile, index)
                                   );
                              })}
                         </div>
                    </div>
               </section>
          )
     };
}

export default SearchExtended;