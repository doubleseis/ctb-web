import React, { Component } from 'react';
import { PayPalButton } from "react-paypal-button-v2";

class Paypal extends Component {
     render(){
          return(
               <PayPalButton
                    options = {{ 
                         // clientId: 'AdnGkXFLEzUBky5CsXg-LToFxF9xTiJFH6jEz5vBXffma53lY5JVu4wzKPM1B1AlEZWYAlCpZDc25Dnu',
                         clientId: 'AXZo-2NNpO_ZB4UcXu5Acw4B6cyHDuOe6xkEalFeEviIUzfiu3B7dN37P9EK09SVzh9i31DeyTF8x4Ok',
                         // vault: true
                    }}
                    createSubscription={(data, actions) => {
                         console.log('action first: ', data, actions);
                         return actions.subscription.create({
                              plan_id: 'P-8AN10913427866211L2VW5WI'
                         });
                    }}
                    onApprove={(data, actions) => {
                         // Capture the funds from the transaction
                         return actions.subscription.get().then(function(details) {
                              // Show a success message to your buyer
                              alert("Subscription completed");
                    
                              // OPTIONAL: Call your server to save the subscription
                              return fetch("/paypal-subscription-complete", {
                                   method: "post",
                                   body: JSON.stringify({
                                        orderID: data.orderID,
                                        subscriptionID: data.subscriptionID
                                   })
                              });
                         });
                    }}
               />
          )
     }
}

export default Paypal;