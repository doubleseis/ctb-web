import React, { Component } from 'react';
import headImage from '../../assets/images/walk-up-steps.jpg';
import PageHeader from '../../components/PageHeader/PageHeader';
import ProRegisterForm from './ProRegisterForm';

class JoinAsPro extends Component {
     
     render() {
          const pageHeader = {
               pageTitle: "Join As Pro",
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          return (
               <section className={'join'}>
                    <PageHeader {...pageHeader} />
                    <ProRegisterForm />
               </section>
          );
     }
}
   
export default JoinAsPro;