import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { auth, db } from '../Firebase/Firebase';
import Image from '../Profile/Image';
import ImageUpload from '../Profile/ImageUpload';
class Onboarding extends Component {
     constructor(props) {
          super(props);
          this.state = {
               progress: 0,
               uid: '',
               name__first: '',
               name__last: '',
               email: '',
               isAdmin: false,
               isPro: true,
               isApproved: false,
               photoURL: '',
               about: '',
               background: '',
               certifications: '',
               funFact: '',
               favQuote: '',
               professions: '',
               businessName: "",
               adress1: "",
               adress2: "",
               city: "",
               state: "",
               zipCode: '00000',
               phoneNumber: "",
               isPremiumPro: false,
               ratesOnline: '',
               ratesInPerson: '',
               travel: false
          }
          this.modal = true;
          this.handleChange = this.handleChange.bind(this);
          this.handlePrev = this.handlePrev.bind(this);
          this.handleNext = this.handleNext.bind(this);
          this.renderOnboarding = this.renderOnboarding.bind(this);
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handlePrev = (e) => {
          e.preventDefault();
          console.log(e);
     }

     handleNext = (e) => {
          e.preventDefault();
     }

     onPricingChange = (e) => {
          this.setState({
               isPremiumPro: e.target.value === 'true' ? true : false
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log( e );
     }

     renderOnboarding = () => {
          var sections = document.querySelectorAll('.onboarding__section');
          var sectionsArr = Array.prototype.slice.call(sections);
          sectionsArr.map((section, index) => {
               var count = index + 1;
               section.setAttribute('data-pagination', count);
               if (count === 1) {
                    section.classList.add('active');
               }

               var buttonsNext = section.querySelectorAll('.button--next');
               var buttonsNext = Array.prototype.slice.call(buttonsNext);
               
               buttonsNext.map((buttonNext) => {
                    buttonNext.addEventListener('click', function (e) {
                         e.preventDefault();
                         section.classList.remove('active');
                         section.nextSibling.classList.add('active');
                    });
               });

               var buttonsPrev = section.querySelectorAll('.button--prev');
               var buttonsPrev = Array.prototype.slice.call(buttonsPrev);
               
               buttonsPrev.map((buttonPrev) => {
                    if (count === 1) {
                         buttonPrev.style.display = 'none';
                    }
                    buttonPrev.addEventListener('click', function (e) {
                         e.preventDefault();
                         section.classList.remove('active');
                         section.previousSibling.classList.add('active');
                    });
               });

          });
     }

     updateProfile = (e) => {
          e.preventDefault();
          const { uid, about, background, rates, travel, isPremiumPro, certifications, ratesOnline, ratesInPerson, funFact, favQuote, photoURL, professions
               , businessName, adress1, adress2, city, state, zipCode, phoneNumber

          } = this.state;
          db.collection('users').doc(uid).set({
               about: about,
               background: background,
               certifications: certifications,
               funFact: funFact,
               favQuote: favQuote,
               isAdmin: false,
               isPro: true,
               isPremiumPro: isPremiumPro,
               isApproved: false,
               photoURL: photoURL,
               professions: professions,
               ratesOnline: ratesOnline,
               ratesInPerson: ratesInPerson,
               travel: travel
          });
          db.collection('users').doc(uid).collection("business").doc("business").set({

               businessName: businessName,
               adress1: adress1,
               adress2: adress2,
               city: city,
               state: state,
               zipCode: zipCode,
               phoneNumber: phoneNumber,

          })
          alert('Profile successfully updated. You will be redirected to your dashboard in 5 seconds.');
          setTimeout(function () {
               window.location.href = "/dashboard";
          }, 5000);
          // console.log( about, background, rates, certifications, funFact, favQuote );
     }

     componentDidMount() {
          this.renderOnboarding();

          auth.onAuthStateChanged(function (user) {
               if (user) {
                    this.setState({
                         uid: user.uid,
                         email: user.email,
                    })
                    // console.log('This is the user: ', this.state.uid );
                    db.collection('users').doc(this.state.uid).get()
                         .then((doc) => {
                              const data = doc.data();
                              if (data.firstName) { this.setState({ name__first: data.firstName }) }
                              if (data.lastName) { this.setState({ name__last: data.lastName }) }
                              if (data.photoURL) { this.setState({ photoURL: data.photoURL }) }
                              if (data.about) { this.setState({ about: data.about }) }
                              if (data.background) { this.setState({ background: data.background }) }
                              if (data.certifications) { this.setState({ certifications: data.certifications }) }
                              if (data.funFact) { this.setState({ certifications: data.funFact }) }
                              if (data.favQuote) { this.setState({ certifications: data.favQuote }) }
                              if (data.ratesOnline) { this.setState({ ratesOnline: data.ratesOnline }) }
                              if (data.ratesInPerson) { this.setState({ ratesInPerson: data.ratesInPerson }) }
                              if (data.professions) { this.setState({ professions: data.professions }) }
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })
               }
          }.bind(this));
     }

     render() {
          console.log(this.state)
          return (
               <section className={'onboarding'}>
                    <div className={'container'}>

                         <form className={'onboarding__form'} onSubmit={this.updateProfile}>

                              <div className={'onboarding__progress'}>
                                   <label for="progress" className={'screen-reader-text'}>Onboarding Progress:</label>
                                   <progress id="progress" value={ this.state.progress } max={'100'}>{ this.state.progress }%</progress> 
                              </div>
                              
                              <fieldset className={'onboarding__section'}>
                                   <legend>Add your profile photo</legend>
                                   <div className={'onboarding__content'}>
                                        <p>Upload a clear image of yourself. The preferred pixel dimension is 580 x 580.</p>
                                        <div className={'onboarding__content-image'}>
                                             <Image image={this.state.photoURL} user={this.state.uid} />
                                             <ImageUpload user={this.state.uid} modal={this.modal} />
                                        </div>
                                   </div>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 10 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>About You</legend>
                                   <p>In a few short words, write about who you are, what you do, and how you stand out from the rest.</p>
                                   <label htmlFor={'about'}>About (HTML Allowed)</label>
                                   <textarea id={'about'} name={'about'} defaultValue={this.state.about} onChange={this.handleChange}></textarea>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 20 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Professional Background</legend>
                                   <label htmlFor={'background'}>Background (HTML Allowed)</label>
                                   <textarea id={'background'} name={'background'} defaultValue={this.state.background} onChange={this.handleChange}></textarea>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 30 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Certifications</legend>
                                   <label htmlFor={'certifications'}>Certifications (HTML Allowed)</label>
                                   <textarea id={'certifications'} name={'certifications'} defaultValue={this.state.certifications} onChange={this.handleChange}></textarea>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 40 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Fun Fact</legend>
                                   <label htmlFor={'funfact'}>Fun Fact (HTML Allowed)</label>
                                   <textarea id={'funFact'} name={'funfact'} defaultValue={this.state.funFact} onChange={this.handleChange}></textarea>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 50 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Favorite Quote</legend>
                                   <label htmlFor={'favquote'}>Favorite Quote (HTML Allowed)</label>
                                   <textarea id={'favQuote'} name={'favquote'} defaultValue={this.state.favQuote} onChange={this.handleChange}></textarea>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 60 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Your Business Info</legend>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>Business Name <sup>*required</sup></label>
                                             <input id={'businessName'} name={'businessName'} defaultValue={this.state.businessName} onChange={this.handleChange} type="text" required />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>Address <sup>*required</sup></label>
                                             <input id={'address1'} name={"address1"} defaultValue={this.state.address1} onChange={this.handleChange} type="text" required />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>Address 2</label>
                                             <input id={'address2'} name={"address2"} defaultValue={this.state.address2} onChange={this.handleChange} type="text" />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>City <sup>*required</sup></label>
                                             <input id={'city'} name={"city"} defaultValue={this.state.city} onChange={this.handleChange} type="text" required/>
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>State</label>
                                             <select name="state" id="state" onChange={this.handleChange} defaultValue="Istanbul">
                                                  <option value="">Select State</option>
                                                  <option value="AL">Alabama</option>
                                                  <option value="AK">Alaska</option>
                                                  <option value="AZ">Arizona</option>
                                                  <option value="AR">Arkansas</option>
                                                  <option value="CA">California</option>
                                                  <option value="CO">Colorado</option>
                                                  <option value="CT">Connecticut</option>
                                                  <option value="DE">Delaware</option>
                                                  <option value="DC">District Of Columbia</option>
                                                  <option value="FL">Florida</option>
                                                  <option value="GA">Georgia</option>
                                                  <option value="HI">Hawaii</option>
                                                  <option value="ID">Idaho</option>
                                                  <option value="IL">Illinois</option>
                                                  <option value="IN">Indiana</option>
                                                  <option value="IA">Iowa</option>
                                                  <option value="KS">Kansas</option>
                                                  <option value="KY">Kentucky</option>
                                                  <option value="LA">Louisiana</option>
                                                  <option value="ME">Maine</option>
                                                  <option value="MD">Maryland</option>
                                                  <option value="MA">Massachusetts</option>
                                                  <option value="MI">Michigan</option>
                                                  <option value="MN">Minnesota</option>
                                                  <option value="MS">Mississippi</option>
                                                  <option value="MO">Missouri</option>
                                                  <option value="MT">Montana</option>
                                                  <option value="NE">Nebraska</option>
                                                  <option value="NV">Nevada</option>
                                                  <option value="NH">New Hampshire</option>
                                                  <option value="NJ">New Jersey</option>
                                                  <option value="NM">New Mexico</option>
                                                  <option value="NY">New York</option>
                                                  <option value="NC">North Carolina</option>
                                                  <option value="ND">North Dakota</option>
                                                  <option value="OH">Ohio</option>
                                                  <option value="OK">Oklahoma</option>
                                                  <option value="OR">Oregon</option>
                                                  <option value="PA">Pennsylvania</option>
                                                  <option value="RI">Rhode Island</option>
                                                  <option value="SC">South Carolina</option>
                                                  <option value="SD">South Dakota</option>
                                                  <option value="TN">Tennessee</option>
                                                  <option value="TX">Texas</option>
                                                  <option value="UT">Utah</option>
                                                  <option value="VT">Vermont</option>
                                                  <option value="VA">Virginia</option>
                                                  <option value="WA">Washington</option>
                                                  <option value="WV">West Virginia</option>
                                                  <option value="WI">Wisconsin</option>
                                                  <option value="WY">Wyoming</option>
                                             </select>
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>Zip Code <sup>*required</sup></label>
                                             <input id={'zipCode'} pattern="[0-9]*" type={'text'} name={'zipCode'} defaultValue={this.state.zipCode} onChange={this.handleChange} required />
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'business'}>Phone Number</label>
                                             <input id={'phoneNumber'} name={"phoneNumber"} type="tel" defaultValue={this.state.phoneNumber} onChange={this.handleChange} />
                                        </div>
                                   </div>

                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 70 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Your Rates</legend>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'rates-online'}>Online <sup>*Required</sup></label>
                                             <input id={'ratesOnline'} min={20} type={'number'} name={'rates-online'} defaultValue={this.state.ratesOnline} onChange={this.handleChange} required />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'inperson'}>In-Person <sup>*Required</sup></label>
                                             <input id={'ratesInPerson'} min={50} type={'number'} name={'inperson'} defaultValue={this.state.ratesInPerson} onChange={this.handleChange} required />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label htmlFor={'travel'}>Available to travel to customer homes?</label>
                                             <input id={'travel'} type={'checkbox'} name={'travel'} defaultValue={this.state.travel} onChange={this.handleChange} />
                                        </div>
                                   </div>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} className={'button button--next button--contrast'} onClick={() => this.setState({ progress: 80 })}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section'}>
                                   <legend>Pricing</legend>
                                   <div className={`onboarding__pricing pricing-options`}>
                                        <div className={'pricing-options__radio'}>
                                             <label>
                                                  <input id={'premium'} type={'radio'} onChange={this.onPricingChange} value={Boolean(true)} name={'pro-pricing'} />
                                                  <div className={`pricing-options__radio-content ${this.state.isPremiumPro ? 'pricing-options__radio-content--selected' : ''}`}>
                                                       <div className={'pricing-options__radio-content-header'}>
                                                            <div className={'pricing-options__radio-content-header-name'}>
                                                                 Premium
                                                            </div>
                                                            <div className={'pricing-options__radio-content-header-cost'}>
                                                                 <span>$30</span>
                                                                 Per Listing
                                                            </div>
                                                       </div>
                                                       <div className={'pricing-options__radio-content-body'}>
                                                            <ul>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Availability Calendar</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Location</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Social Links</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Website Link</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Rewards</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Recognition</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Photos/Videos</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Reviews</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Additional Leads</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Access to Social Community</li>
                                                            </ul>
                                                            <span className={'button button--primary'}>Select<span>ed</span></span>
                                                       </div>
                                                  </div>
                                             </label>
                                        </div>
                                        <div className={'pricing-options__radio'}>
                                             <label>
                                                  <input id={'basic'} type={'radio'} onChange={this.onPricingChange} value={Boolean(false)} name={'pro-pricing'} />
                                                  <div className={`pricing-options__radio-content ${this.state.isPremiumPro ? '' : 'pricing-options__radio-content--selected'}`}>
                                                       <div className={'pricing-options__radio-content-header'}>
                                                            <div className={'pricing-options__radio-content-header-name'}>
                                                                 Basic
                                                            </div>
                                                            <div className={'pricing-options__radio-content-header-cost'}>
                                                                 <span>Free</span>
                                                                 Per Listing
                                                            </div>
                                                       </div>
                                                       <div className={'pricing-options__radio-content-body'}>
                                                            <ul>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Availability Calendar</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Location</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Social Links</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Website Link</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Rewards</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Recognition</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Photos/Videos</li>
                                                                 <li><FontAwesomeIcon icon={faCheckCircle} /> Reviews</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Additional Leads</li>
                                                                 <li><FontAwesomeIcon icon={faTimesCircle} /> Access to Social Community</li>
                                                            </ul>
                                                            <span className={'button button--primary'}>Select<span>ed</span></span>
                                                       </div>
                                                  </div>
                                             </label>
                                        </div>
                                   </div>
                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'} onClick={() => this.setState({ progress: this.state.progress - 10 })}>Previous</button>
                                        <button value={'next'} onClick={() => this.setState({ progress: 90 })} className={'button button--next button--contrast'}>Next</button>
                                   </div>
                              </fieldset>

                              <fieldset className={'onboarding__section onboarding__section--review'}>
                                   <legend>Review</legend>

                                   <p>All fields with (<sup>*Required</sup>) are required before submission.</p>
                                   
                                   <h2><strong>Profile Image</strong></h2>
                                   <div className={'form__row'}>
                                        
                                        <div className={'onboarding__content-image'}>
                                             <Image image={this.state.photoURL} user={this.state.uid} />
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h2><strong>About</strong></h2>
                                             {ReactHtmlParser(this.state.about)}
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h2><strong>Professional Background</strong></h2>
                                             {ReactHtmlParser(this.state.background)}
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h2><strong>Certifications</strong></h2>
                                             {ReactHtmlParser(this.state.certifications)}
                                        </div>
                                   </div>

                                   <h2><strong>Rates</strong></h2>
                                   <div className={'form__row form__row--double'}>
                                        <div className={'form__col'}>
                                             <h3>Online <sup>*required</sup></h3>
                                             <p>${this.state.ratesOnline} per hour</p>
                                        </div>
                                        <div className={'form__col'}>
                                             <h3>In-Person <sup>*required</sup></h3>
                                             <p>${this.state.ratesInPerson} per hour</p>
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h2><strong>Fun Fact</strong></h2>
                                             {ReactHtmlParser(this.state.funFact)}
                                        </div>
                                   </div>

                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h2><strong>Favorite Quote</strong></h2>
                                             { ReactHtmlParser(this.state.favQuote) }
                                        </div>
                                   </div>
                                   
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h3>Business Information <sup>*required</sup></h3>
                                             <p>{this.state.businessName}</p>
                                             <p> {this.state.adress1} {this.state.adress2} </p>
                                             <p>{this.state.city}, {this.state.state} {this.state.zipCode} </p>
                                        </div>
                                   </div>
                                   
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <h3>Phone Number</h3>
                                             <p>{this.state.phoneNumber} </p>
                                        </div>
                                   </div>

                                   <div className={'onboarding__nav'}>
                                        <button value={'prev'} className={'button button--prev button--primary'}>Previous</button>
                                        <button value={'Complete Profile'} type="submit" className={'button button--contrast'}>Complete Profile</button>
                                   </div>
                              </fieldset>
                         </form>
                    </div>
               </section>
          )
     }
}

export default Onboarding;