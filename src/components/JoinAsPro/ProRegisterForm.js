import { faFacebookSquare, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import defaultProfileImage from '../../assets/images/default-user.jpg';
import { auth, db, fb, google } from '../Firebase/Firebase';


class ProRegisterForm extends Component {
     constructor(props) {
          super(props);
          this.state = {
               name__first: '',
               name__last: '',
               email: '',
               password: ''
          }
          this.handleNextForm = this.handleNextForm.bind(this);
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     handleSubmit = (e) => {
          e.preventDefault();
          console.log(this.state.email, this.state.password);
          const { name__first, name__last, email, password, professions } = this.state;
          auth.createUserWithEmailAndPassword(email, password)
               .then((user) => {
                    user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         photoURL: defaultProfileImage,
                         username:"pro-"+Math.random().toString(36).substr(2, 9),
                         isPro: true,
                         isAdmin: false,
                         finishedOnboarding: false,
                         professions: professions
                    })
                         .then(function() {
                              console.log("Document successfully written!");
                              window.location.href = "/onboarding";
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                         
                    // See the UserRecord reference doc for the contents of userRecord.
                    // console.log('Successfully created new user:', user.uid);
               })
               .catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log('Error code for creating new user:', errorCode);
                    console.log('Error msg for creating new user:', errorMessage);
                    alert(errorMessage);
               });
     }

     handleFacebookLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, professions } = this.state;
          auth.signInWithPopup(fb)
               .then(({ user }) => {
                    this.setState({ user })
                    // user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         isAdmin: false,
                         username:"pro-"+Math.random().toString(36).substr(2, 9),
                         isPro: true,
                         photoURL: defaultProfileImage,
                         professions: professions,
                         finishedOnboarding: false,
                    })
                    .then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/onboarding";
                    })
               })
               // .catch(function(error) {
               //      var errorCode = error.code;
               //      var errorMessage = error.message;
               //      console.log('Error code for creating new user:', errorCode);
               //      console.log('Error msg for creating new user:', errorMessage);
               //      alert(errorMessage);
               // });
     }

     handleGoogleLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(google)
               .then(function(result) {
                    var token = result.credential.accessToken;
                    var user_id = result.user.uid;
                    var user_first_name = result.additionalUserInfo.profile.given_name;
                    var user_last_name = result.additionalUserInfo.profile.family_name;
                    var user_image_url = result.additionalUserInfo.profile.picture;
                    var user_creation_time = result.user.creationTime;

                    // console.log(result);
                    // console.log(user_id)
                    
                    db.collection('users').doc(user_id).set({
                         uid: user_id,
                         firstName: user_first_name,
                         lastName: user_last_name,
                         username:"pro-"+Math.random().toString(36).substr(2, 9),
                         isAdmin: false,
                         isPro: true,
                         finishedOnboarding: false,
                         photoURL: user_image_url,
                    }).then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/onboarding";
                    }).catch(function(error) {
                         console.error("Error writing document: ", error);
                    });
               })
               .catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...
                    console.error("Error: ", error);
               });
     }

     handleNextForm = (e) => {
          e.preventDefault();
          var step_one = document.getElementById('step-1');
          var step_two = document.getElementById('step-2');
          var professionsOptions = step_one.getElementsByClassName('checkbox');
          professionsOptions = Array.prototype.slice.call(professionsOptions);
          var checked = 0;
          var professions = [];
          professionsOptions.map(profession => {
               if( profession.checked ) {
                    checked += 1;
                    return professions.push(profession.name);
               }
               return false;
          });
          if ( checked === 0 ) {
               return alert('Please choose at least one main profession.');
          }
          this.setState({
               professions
          })
          step_one.classList.add('hide');
          step_two.classList.remove('hide');
     }

     componentDidMount() {
          // this.handleNextForm();
     }

     render(){
          return (
               <section className={'form__outer-container'}>
                    <form className={'form form--steps'} onSubmit={ this.handleSubmit }>
                         <fieldset id={'step-1'} className={'form__container active'}>
                              <legend>What is your main profession?</legend>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <div className={'form__checkbox'}>
                                             <input className={'checkbox'} type="checkbox" id="fitnessTrainer" name="fitnessTrainer" value="fitnessTrainer" />
                                             <label htmlFor="fitnessTrainer"> Fitness Trainer</label>
                                        </div>

                                        <div className={'form__checkbox'}>
                                             <input className={'checkbox'} type="checkbox" id="nutritionist" name="nutritionist" value="nutritionist" />
                                             <label htmlFor="nutritionist"> Nutritionist</label>
                                        </div>

                                        <div className={'form__checkbox'}>
                                             <input className={'checkbox'} type="checkbox" id="massageTherapist" name="massageTherapist" value="massageTherapist" />
                                             <label htmlFor="massageTherapist"> Massage Therapist</label>
                                        </div>

                                        <div className={'form__checkbox'}>
                                             <input className={'checkbox'} type="checkbox" id="chef" name="chef" value="chef" />
                                             <label htmlFor="chef"> Chef</label>
                                        </div>
                                   </div>
                              </div>
                              <div className={'form__submit'}>
                                   <button className={'button button--secondary'} onClick={this.handleNextForm}>Next</button>
                              </div>
                         </fieldset>

                         <fieldset id={'step-2'} className={'form__container hide'}>
                              <legend>Create Your Account</legend>
                              <div className={'form__row form__row--double'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__first'>First Name*</label>
                                        <input id='name__first' type='text' name='name__first' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='name__last'>Last Name*</label>
                                        <input id='name__last' type='text' name='name__last' onChange={ this.handleChange } />
                                   </div>
                              </div>
                              <div className={'form__row'}>
                                   <div className={'form__col'}>
                                        <label htmlFor='email'>Email</label>
                                        <input id='email' type='email' name='email' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'form__col'}>
                                        <label htmlFor='password'>Password</label>
                                        <input id='password' type='password' name='password' onChange={ this.handleChange } />
                                   </div>
                                   <div className={'disclaimer'}>
                                        <p>By clicking Create Account, you agree to the <a href={'/terms-of-use'} target={'blank'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                                   </div>
                              </div>

                              <div className={'form__submit'}>
                                   <input className={'button button--secondary'} type='submit' value='Create Account' />
                              </div>

                              <div className={'divider'}></div>
                    
                              <div className={'social-logins'}>
                                   <div className={'disclaimer'}>
                                        <p>By clicking Sign Up with Facebook or Sign Up with Google, you agree to the <a href={'/terms-of-use'} target={'blank'}>Terms of Use</a> and <a href={'/privacy-policy'} target={'blank'}>Privacy Policy</a>.</p>
                                   </div>
                                   <div className={'social-logins__buttons'}>
                                        <button className={'button button--secondary button--facebook'} onClick={this.handleFacebookLogin}><FontAwesomeIcon icon={faFacebookSquare} /> Sign Up with Facebook</button>
                                        <button className={'button button--secondary button--google'} onClick={this.handleGoogleLogin}><FontAwesomeIcon icon={faGoogle} /> Sign Up with Google</button>
                                   </div>
                              </div>
                         </fieldset>
                         
                    </form>

               </section>
          )
     }
}
   
export default ProRegisterForm;