import PropTypes from 'prop-types';
import React from 'react';

class Modal extends React.Component {
	static propTypes = {
		isModalOpen: PropTypes.bool.isRequired,
		closeModal: PropTypes.func.isRequired
	};

	// render modal
	render() {
		return (
			<dialog className={ this.props.isModalOpen ? "modal modal--active" : "modal"}>
				<div className={'modal__overlay'} onClick={this.props.closeModal} />
				<div className={'modal__container'}>
                         <div className={'modal__box'}>
                              {this.props.children}
                         </div>     
                    </div>
			</dialog>
		);
	}
}

export default Modal;