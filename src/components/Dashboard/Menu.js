import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class DashboardMenu extends Component {
     render() {
          return ( 
               <aside className={'dashboard__menu'}>
                    <ul>
                         <li><Link to='/dashboard'>Dashboard</Link></li>
                         <li><Link to='/dashboard/messages'>Messages</Link></li>
                         {
                              this.props.pro === true ?
                                   <li><Link to='/dashboard/booking-requests'>Booking Requests</Link></li>
                              : ''
                         }
                         {/* {
                              this.props.pro === true ?
                                   <li><Link to='/dashboard/community'>Community</Link></li>
                              : ''
                         } */}
                         <li><Link to='/dashboard/profile'>Profile</Link></li>
                         {/* <li><Link to='/dashboard/payment-history'>Payment History</Link></li> */}
                         <li><Link to='/dashboard/settings'>Settings</Link></li>
                    </ul>
               </aside>
          );
     }
}

export default DashboardMenu;