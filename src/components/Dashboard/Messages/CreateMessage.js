import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MuiAlert from '@material-ui/lab/Alert';
import * as firebase from 'firebase';
import React, { Component } from "react";
import { FormGroup, Input } from 'reactstrap';
import { auth, db, dbRef } from "../../Firebase/Firebase";
import Modal from '../../Modal/ModalView';

class CreateMessage extends Component {

     constructor(props) {
          super(props);
          this.state = {
               following: [],
               followingNames: [],
               message: '',
               sendingTo: '',
               messageStatus: false,
               select:"",
          }
          this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
          this.handleSend = this.handleSend.bind(this);
          this.handleChange = this.handleChange.bind(this);
          this.handle = this.handle.bind(this);
          this.handleOptionChange = this.handleOptionChange.bind(this);
          this.handleKeyPress = this.handleKeyPress.bind(this);
     }
     handle(e){
          const Name=e.target.value.split(" ")[0]
          const LastName=e.target.value.split(" ")[1]
          // console.log(Name)
          // console.log(LastName)
          const towho=this.state.followingNames.find(item=>item.firstName===Name&&item.lastName===LastName)
          // console.log(towho)
          this.setState({select: e.target.value,sendingTo:towho.uid});
        }
     handleChange(event) {
          this.setState({message: event.target.value});
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }

     handleOptionChange(event) {
          this.setState({sendingTo: event.target.value});
     }

     handleSend() {
          let current = this;
          if (current.state.message && current.state.sendingTo) {
               const uniqueID = Math.random().toString(36).substr(2, 9);
               var newMessage = {
                    msg: this.state.message,
                    sentBy: this.props.userLoggedIn
               }
               var messageMeta = {
                    originalSender: this.props.userLoggedIn,
                    originalReceiver: this.state.sendingTo
               }
               db.collection('users').doc(this.props.userLoggedIn).collection('messages').doc('groups').update({
                    groupIDs: firebase.firestore.FieldValue.arrayUnion(uniqueID)
               })
               db.collection('users').doc(this.state.sendingTo).collection('messages').doc('groups').get()
                    .then((doc)=> {
                         if( doc.exists ) {
                              db.collection(`users`).doc(this.state.sendingTo).collection('messages').doc('groups').update({
                                   groupIDs: firebase.firestore.FieldValue.arrayUnion(uniqueID)
                              })
                         } else {
                              db.collection(`users`).doc(this.state.sendingTo).collection('messages').doc('groups').set({
                                   groupIDs: firebase.firestore.FieldValue.arrayUnion(uniqueID)
                              })
                         }
                    })
               dbRef.ref(`messages/${uniqueID}`).child('msgs').set(messageMeta);
               dbRef.ref(`messages/${uniqueID}`).child('msgs').push(newMessage);
               // console.log('success!!');
          }
          this.closeModal();
          this.setState({
               messageStatus: true
          })
          this.props.stated()
     }

     handleKeyPress(event) {
          if (event.key !== 'Enter') return;
          this.handleSend();
     }

     componentDidMount = () => {
          auth.onAuthStateChanged(function(user) {
               if(user) {
                    db.collection(`users`).doc(user.uid).collection('social').doc('following').get()
                         .then((doc) => {
                              // console.log('cdm', doc);
                              const data = doc.data();
                              const Array = [];
                              db.collection('users').get().then(snapshot=>{
                                   snapshot.forEach(doc => {
                                        Array.push( doc.data() );
                                   });
                                   let Arrayfollowed = [];
                                   for( let i = 0; i < data.accounts.length; i++ ){
                                        Arrayfollowed.push( Array.find((item) => item.uid === data.accounts[i]) ); 
                                   }
                                   this.setState({
                                        followingNames: Arrayfollowed
                                   })
                              }).catch(e => console.log(e) )       
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })
                    db.collection(`users`).doc(user.uid).collection('messages').doc('groups').get()
                         .then(function(doc) {
                              if( doc.exists ){
                                   //exists
                              } else {
                                   db.collection(`users`).doc(user.uid).collection('messages').doc('groups').set({
                                        groupIDs: []
                                   }).then(function() {
                                        console.log("Document successfully written!");
                                   })
                                   .catch(function(error) {
                                        console.error("Error writing document: ", error);
                                   });
                              }
                         })
               }
          }.bind(this));
     }
    
     render() {
          // console.log(this.state)
          return (
               <div className={'create-message'}>
                    <button className="button button--contrast" onClick={this.openModal}>Create New Message</button>
                    <Modal 
                         isModalOpen={this.state.isModalOpen}
					closeModal={this.closeModal}
                    >
                         <h2>Send Message</h2>
                         <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                         <div className={'create-message__form'}>
                              <div className={'create-message__search'}>
                                   <FormGroup>
                                        { this.state.followingNames && this.state.followingNames.length ? '' : (
                                             <MuiAlert color="warning" variant="filled" style={{marginBottom:10}}>
                                                  <p>Start following Pros to send them a message.</p>
                                             </MuiAlert>
                                        ) }
                                        <Input type="select" name="select" id="exampleSelect" style={{width:"100%",marginBottom:30,height:40}} label="select" value={ this.state.select } onChange={this.handle} >
                                             <option value="">Select Name</option>
                                             { this.state.followingNames.length != 0 && this.state.followingNames.map((item) =>
                                                  <option key={item.uid} >{`${item.firstName}  ${item.lastName} ( ${item.username} ) `}</option>
                                             )}
                                        </Input>
                                   </FormGroup>    
                              </div>
                              <div className={'send-message__container'}>
                                   <input className="send-message__input" type="text" placeholder="Type message" value={this.state.message} onChange={this.handleChange} onKeyPress={this.handleKeyPress} />
                                   <button className="send-message__button" onClick={this.handleSend}>send</button>
                              </div>
                         </div>

                    </Modal>
               </div>
          );
     }

}

export default CreateMessage;