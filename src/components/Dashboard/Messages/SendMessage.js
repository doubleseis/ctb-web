import React, { Component } from "react";
import { dbRef } from "../../Firebase/Firebase";

class SendMessage extends Component {

     constructor(props) {
          super(props);
          this.state = {
               message: ''
          }
          this.handleSend = this.handleSend.bind(this);
          this.handleChange = this.handleChange.bind(this);
          this.handleKeyPress = this.handleKeyPress.bind(this);
     }

     handleChange(event) {
          this.setState({message: event.target.value});
     }

     componentWillReceiveProps(nextProps) {
          if(nextProps.user) {
               this.setState({'userName': nextProps.user.displayName});
          }
     }

     handleSend() {
          if (this.state.message) {
               var newItem = {
                    msg: this.state.message,
                    sentBy: this.props.sender
               }
               dbRef.ref(`messages/${this.props.msgGroupID}`).child('msgs').push(newItem);
          }
     }

     handleKeyPress(event) {
          if (event.key !== 'Enter') return;
          this.handleSend();
     }

     render() {
          return (
               <div className={'send-message'}>
                    <div className={'send-message__container'}>
                         <input className="send-message__input" type="text" placeholder="Type message" value={this.state.message} onChange={this.handleChange} onKeyPress={this.handleKeyPress} />
                         <button className="send-message__button" onClick={this.handleSend}>send</button>
                    </div>
               </div>
          );
     }

}

export default SendMessage;