import React, { Component } from 'react';
import { auth, db } from '../../Firebase/Firebase';
import CreateMessage from './CreateMessage';
import GetMessages from './GetMessages';

class Messages extends Component {
     constructor(props) {
          super(props);
          this.state = {
               groupIDs: []
          }
          this.getMessages = this.getMessages.bind(this);
          this.renderParent = this.renderParent.bind(this);
     }

     

     getMessages = ( groupIDs ) => {
          if( Array.isArray(groupIDs) && groupIDs.length ) {
               
               return(
                    groupIDs.map((groupID, index) => (
                         <GetMessages key={index} userLoggedIn={this.props.data.uid} msgGroupID={groupID}  />
                    ))
               );
          }
     }

     renderParent = () => { 
          var userFirebase = auth.currentUser;
          let thisProp = this;
          if(userFirebase){
               // console.log(userfirebase)
               db.collection('users').doc(userFirebase.uid).collection('messages').doc('groups').get()
                    .then(function(doc) {
                         if(doc.exists ){
                              thisProp.setState({
                                   groupIDs: doc.data().groupIDs,
                              })
                         }
                    })
               }
     }

     componentDidMount(){
          console.log("did mount")
          let current = this;
          auth.onAuthStateChanged(function(user) {
               if(user) {
                    db.collection('users').doc(user.uid).collection('messages').doc('groups').get()
                         .then(function(doc) {
                              if(doc.exists ){
                                   current.setState({
                                        groupIDs: doc.data().groupIDs,
                                   })
                              }
                         })
               }
          })
     }

     render() {
          // console.log("render message")
          const { uid } = this.props.data;
          
          return (
               <section className={'messages'}>
                    <div className={'messages__container'}>
                         <div className={'messages__title'}>
                              <h2>Messages</h2>
                         </div>
                         <div className={'messages__list'}>
                              <CreateMessage userLoggedIn={ this.props.data.uid } stated={ this.renderParent } />
                              {/* { this.state.groupIDs.map((groupID, index) => (
                                   <GetMessages key={index} userLoggedIn={this.props.data.uid} msgGroupID={groupID}   />
                              )) } */}
                              { this.getMessages(this.state.groupIDs) }
                         </div>
                         
                    </div> 
               </section>
          );
     }
}

export default Messages;