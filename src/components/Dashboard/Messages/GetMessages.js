import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from "react";
import { Accordion } from 'semantic-ui-react';
import { db, dbRef } from "../../Firebase/Firebase";
import OptionsMessages from './OptionsMessages';
import SendMessage from "./SendMessage";

class GetMessages extends Component {

     constructor(props) {
          super(props);
          this.state = {
               originalReceiver: '',
               originalSender: '',
               receiverIDName: '',
               list: [],
               activeIndex: 0,
               receivername: "",
          }
          this.renderMessages = this.renderMessages.bind(this);
          this.listenMessages();
          this.renderReceiverName = this.renderReceiverName.bind(this);
     }

     componentDidMount() {
          let currentComponent = this;
          // const groupID = this.props.msgGroupID ? this.props.msgGroupID : '';
          // if( this.props.msgGroupID !== '' && this.props.msgGroupID !== undefined && this.props.msgGroupID !== null ) {
          dbRef.ref(`messages/${this.props.msgGroupID}/msgs`).once('value')
               .then((snapshot) => {
                    // console.log(snapshot);

                    if (snapshot.exists) {
                         let userreciever = ""
                         let firstName;
                         let lastName;
                         db.collection(`users`).doc(snapshot.val().originalReceiver).get().then((doc) => {

                              userreciever = doc.data()

                              firstName = userreciever.firstName
                              lastName = userreciever.lastName
                              if (firstName) {

                                   this.setState({
                                        receivername: `${firstName} ${lastName}`
                                   })
                              }
                         }).catch(e => console.log(e))

                         currentComponent.setState({
                              originalReceiver: snapshot.val().originalReceiver,
                              originalSender: snapshot.val().originalSender,

                         },
                              function () {
                                   currentComponent.renderReceiverName();
                              })
                    } else {
                         console.log("No Messages");
                    }
               })
          // }

     }

     handleClick = (e, titleProps) => {
          const { index } = titleProps
          const { activeIndex } = this.state
          const newIndex = activeIndex === index ? -1 : index

          this.setState({ activeIndex: newIndex })
     }

     renderReceiverName = () => {
          let currentComponent = this;
          const { userLoggedIn } = this.props;
          const { originalReceiver, originalSender } = this.state
          const uid = originalReceiver === userLoggedIn ? originalReceiver : originalSender;

          db.collection('users').doc(uid).get()
               .then(function (doc) {
                    const name = doc.data().firstName !== undefined ? 'with ' + doc.data().firstName + ' ' + doc.data().lastName : '';
                    // console.log(name);
                    currentComponent.setState({
                         receiverIDName: name
                    })
               })
     }

     listenMessages() {
          if (this.props.msgGroupID !== null || this.props.msgGroupID !== undefined || this.propsmsgGroupID !== '') {
               dbRef.ref(`messages/${this.props.msgGroupID}`).child('msgs')
                    .limitToLast(10)
                    .on('value', message => {
                         this.setState({
                              list: Object.values(message.val()),
                         });
                    });
          }
     }

     renderMessages = (msg, sender, sendDate, index) => {
          return (
               <div key={index} className={this.props.userLoggedIn !== sender ? 'message' : 'message message--blue'}>
                    {/* { sender } */}
                    {/* { sendDate ? new Date((sendDate.seconds*1000)).toLocaleString('en-us', { year: 'numeric', day: 'numeric', month: 'long'  }) : '' } */}
                    {msg}
               </div>
          )
     }


     render() {
          //     console.log(this.state.receivername)
          const { activeIndex } = this.state
          return (
               <Accordion tabIndex='1' className={'msgs'}>
                    <Accordion.Title className={'msgs__title'} active={activeIndex === -1} index={0} onClick={this.handleClick}>
                         Conversation with {this.state.receivername}
                         <span>
                              <FontAwesomeIcon icon={faCaretDown} />
                         </span>
                    </Accordion.Title>
                    <Accordion.Content className={'msgs__content'} active={activeIndex === -1}>
                         {this.state.list.map((msg, index) => (
                              this.renderMessages(msg.msg, msg.sentBy, msg.sentOn, index)
                         ))}
                         <OptionsMessages sender={this.props.userLoggedIn} msgGroupID={this.props.msgGroupID} />
                         <SendMessage sender={this.props.userLoggedIn} msgGroupID={this.props.msgGroupID} />
                    </Accordion.Content>
               </Accordion>
          );
     }

}

export default GetMessages;