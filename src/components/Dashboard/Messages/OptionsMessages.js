import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as firebase from 'firebase';
import React, { Component } from "react";
import { db, dbRef } from '../../Firebase/Firebase';
import Modal from '../../Modal/ModalView';

class OptionsMessages extends Component {

     constructor(props) {
          super(props);
          this.state = {
               isModalOpen: false
          }
          this.handleDelete = this.handleDelete.bind(this);
          this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
     }

     handleDelete(event) {
          console.log('delete btn pressed');
          dbRef.ref(`messages`).child(this.props.msgGroupID).remove();
          db.collection('users').doc(this.props.sender).collection('messages').doc('groups').update({
               groupIDs: firebase.firestore.FieldValue.arrayRemove(this.props.msgGroupID)
          }) 
          this.closeModal();
          // window.location.reload(false);
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }

     render() {
          return (
               <div className={'message-options'}>
                    <div className={'message-options__container'}>
                         <ul>
                              <li>
                                   <button className={'link link--danger'} onClick={this.openModal}>Delete Chat</button>
                              </li>
                         </ul>
                    </div>
                    <Modal 
                         isModalOpen={this.state.isModalOpen}
					closeModal={this.closeModal}
                    >
                         <h2>Confirm Delete</h2>
                         <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                         <button className={'button button--danger'} onClick={this.handleDelete}>Delete Permanently</button>
                    </Modal>
               </div>
          );
     }

}

export default OptionsMessages;