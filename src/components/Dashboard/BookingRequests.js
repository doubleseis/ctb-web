
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { auth, db } from '../Firebase/Firebase';
import Modal from '../Modal/ModalView';

class BookingRequests extends Component {
     constructor(props) {
          super(props);
          this.state = {
               requests: [],
               isModalOpen: false
          }
          this.renderRequests = this.renderRequests.bind(this);
          this.toSentenceCase = this.toSentenceCase.bind(this);
          this.closeModal = this.closeModal.bind(this);
          this.openModal = this.openModal.bind(this);
          this.doApprove = this.doApprove.bind(this);
          this.doDecline = this.doDecline.bind(this);
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }

     toSentenceCase = (camelCase) => {
          var addSpaces = camelCase.replace( /([A-Z])/g, " $1" );
          var makeSentenceCase = addSpaces.charAt(0).toUpperCase() + addSpaces.slice(1);
          return makeSentenceCase;
     }

     doApprove(bookingID) {
          // console.log('works');
          db.collection('users').doc(this.props.data.uid).collection('bookingRequests').doc(bookingID).update({
               status: true
          })
     }

     doDecline(bookingID, req) {
          db.collection('users').doc(this.props.data.uid).collection('bookingRequests').doc(bookingID).update({
               status: false
          })
     }

     renderRequests = () => {
          const requestsArray = [];
          this.state.requests.map((request,index) => {
               console.log(request);
               requestsArray.push(
                    <tr key={ index }>
                         { request.name !== undefined ?
                              <td>{ request.name }</td>
                         : 
                              <td>N/A</td>
                         }
                         { request.date !== undefined ?
                              <td>{ new Date((request.date.seconds*1000)).toLocaleString('en-us', { year: 'numeric', day: 'numeric', month: 'long'  }) }</td>
                         : 
                              <td>N/A</td>
                         }
                         { request.profession !== undefined ?
                              <td>{ this.toSentenceCase(request.profession) }</td>
                         : 
                              <td>N/A</td>
                         }
                         <td>
                              <button className={'btn btn--primary btn--action'} value={index} onClick={this.openModal}>Details</button>
                              <Modal 
                                   isModalOpen={this.state.isModalOpen}
                                   closeModal={this.closeModal}
                              >
                                   <h2>Booking Details</h2>
                                   <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>

                                   <div className={'dashboard__booking-details'}>
                                        <p><strong>Name:</strong> { request.name ? request.name : 'No Name Available' }</p>
                                        <p><strong>Service Requested:</strong> { request.profession ? this.toSentenceCase(request.profession) : 'No Service Selected' }</p>
                                        <p><strong>Rate:</strong> { request.rate }</p>
                                        <p><strong>Date:</strong> { new Date((request.date.seconds*1000)).toLocaleString('en-us', { year: 'numeric', day: 'numeric', month: 'long'  }) }</p>
                                        <p><strong>Time:</strong> { request.time ? request.time : 'No Time Available' }</p>
                                        <p><strong>Status:</strong> { request.status === 'pending' ? 'Pending' : request.status ? 'Approved' : 'Declined' }</p>
                                        <div className={'dashboard__booking-details-btns'}>
                                             { request.status !== true ? 
                                                  <button className={'btn btn--primary'} onClick={this.doApprove(request.id)}>Approve</button>
                                             : '' }
                                             { request.status !== false ? 
                                                  <button className={'btn btn--primary'} onClick={this.doDecline(request.id, request)}>Decline</button>
                                             : '' }
                                        </div>
                                   </div>
                              </Modal>
                         </td>
                         <td>
                              { request.status === 'pending' ? 'Pending' : request.status ? 'Approved' : 'Declined' }
                         </td>
                    </tr>
               )
          })          
          return requestsArray;
     }

     componentDidMount() { 
          let currentComponent = this;
          auth.onAuthStateChanged(function(user) {
               if(user) {
                    db.collection('users').doc(user.uid).collection('bookingRequests').get()
                         .then(function(querySnapshot) {                                
                              querySnapshot.forEach(function(doc) {       
                                   currentComponent.setState((prevState) => ({
                                        requests: [...prevState.requests, {
                                             id: doc.id,
                                             name: doc.data().name,
                                             time: doc.data().time,
                                             status: doc.data().status,
                                             date: doc.data().date,
                                             profession: doc.data().profession,
                                             userRequestor: doc.data().userRequestor
                                        }]
                                   }))
                              })
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })  
               }
          }.bind(this));
     }

     render() {
          return (
               <section className={'dashboard__booking'}>
                    <div className={'dashboard__booking-container'}>
                         <div className={'dashboard__booking-title'}>
                              <h2>Booking Requests</h2>
                         </div>
                         <div className={'dashboard__booking-table'}>
                              <table>
                                   <tbody>
                                        <tr>
                                             <th>Name</th>
                                             <th>Request Date</th>
                                             <th>Service Requested</th>
                                             <th>Actons</th>
                                             <th>Status</th>
                                        </tr>
                                        { this.renderRequests() }
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </section>
          );
     }
}

export default BookingRequests;