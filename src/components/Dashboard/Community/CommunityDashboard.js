import React, { Component } from 'react';
import { dbRef } from '../../Firebase/Firebase';

class Community extends Component {

     constructor(props) {
          super(props);
          this.state = {
               list: []
          }
          this.renderMessages = this.renderMessages.bind(this);
          this.listenMessages();
     }

     listenMessages() {
          dbRef.ref(`global`).child('timeline')
               .limitToLast(10)
               .on('value', message => {
                    this.setState({
                         list: Object.values( message.val() ),
                    });
               });
     }

     renderMessages = (msg, index) => {
          return (
               <div key={index} className={ 'message' }>
                    { msg }
               </div>
          )
     }


     componentDidMount(){
          let currentComponent = this;
          dbRef.ref(`global/timeline/`).once('value')
               .then((snapshot) => {
                    if(snapshot.exists) {
                         // currentComponent.setState({
                         //      originalReceiver: snapshot.val().originalReceiver,
                         //      originalSender: snapshot.val().originalSender
                         // },
                         // function() { 
                         //      currentComponent.renderReceiverName();
                         // } )
                    } else {
                         console.log("No Messages");
                    }
               })          
     }

     render() {
          return (
               <section className={'community'}>
                    <div className={'community__container'}>
                         <h2>Community</h2>
                         <div class={'community__board'}>
                              { this.state.list.map((msg, index) => (
                                   this.renderMessages(msg.msg, msg.sentBy, msg.sentOn, index)
                              )) }
                         </div>
                    </div> 
               </section>
          );
     }
}

export default Community;