import MuiAlert from '@material-ui/lab/Alert';
import React, { Component } from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import Checkbox from "../Checkbox/index";
import { auth, db } from '../Firebase/Firebase';
import Image from '../Profile/Image';
import ImageUpload from '../Profile/ImageUpload';

class Profile extends Component {
     constructor() {
          super();
          this.state = {
               currentuser: null,
               currentuserpro: false,
               uid: '',
               name__first: '',
               name__last: '',
               email: '',
               photoURL: '',
               about: '',
               background: '',
               certifications: '',
               funFact: '',
               favQuote: '',
               professions: '',
               ratesOnline: '',
               ratesInPerson: '',
               facebook: '',
               twitter: '',
               linkedIn: '',
               pinterest: '',
               travel: false,
               socialFormUpdated: false,
               profileFormUpdated: false,
               faqUpdated: false,
               faq_1_question: '',
               faq_1_answer: '',
               faq_2_question: '',
               faq_2_answer: '',
               faq_3_question: '',
               faq_3_answer: '',
               faq_4_question: '',
               faq_4_answer: '',
               faq_5_question: '',
               faq_5_answer: ''
          }
          this.modal = true;
          this.updateProfile = this.updateProfile.bind(this);
     }

     handleChange = (e) => {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     updateProfile = (e) => {
          e.preventDefault();
          const { uid, firstName, lastName, about, background, certifications, ratesOnline, ratesInPerson, funFact, favQuote, photoURL, professions, travel } = this.state;
          // console.log(uid, firstName, lastName, about, background, certifications, ratesOnline, ratesInPerson, funFact, favQuote, photoURL, professions);
          db.collection('users').doc(uid).update({
               firstName: firstName,
               lastName: lastName,
               about: about,
               background: background,
               certifications: certifications,
               funFact: funFact,
               favQuote: favQuote,
               photoURL: photoURL,
               professions: professions,
               ratesOnline: ratesOnline,
               ratesInPerson: ratesInPerson,
               travel: travel
          });
          this.setState({
               profileFormUpdated: true
          })
     }

     updateSocial = (e) => {
          e.preventDefault();
          const { uid, facebook, twitter, linkedIn, pinterest } = this.state;
          db.collection('users').doc(uid).collection('social').doc('external').get()
               .then((doc) => {
                    // console.log(doc, doc.data())
                    if(doc.exists) {
                         db.collection('users').doc(uid).collection('social').doc('external').update({
                              facebook: facebook,
                              twitter: twitter,
                              linkedIn: linkedIn,
                              pinterest: pinterest 
                         }) 
                    } else {
                         db.collection('users').doc(uid).collection('social').doc('external').set({
                              facebook: facebook,
                              twitter: twitter,
                              linkedIn: linkedIn,
                              pinterest: pinterest 
                         }) 
                    }
                    this.setState({
                         socialFormUpdated: true
                    })
               })
     }

     updateFAQ = (e) => {
          e.preventDefault();
          const {
               uid,
               faq_1_question,
               faq_1_answer,
               faq_2_question,
               faq_2_answer,
               faq_3_question,
               faq_3_answer,
               faq_4_question,
               faq_4_answer,
               faq_5_question,
               faq_5_answer } = this.state;
               
          db.collection('users').doc(uid).collection('social').doc('faq').get()
               .then((doc) => {
                    if(doc.exists) {
                         db.collection('users').doc(uid).collection('social').doc('faq').update({
                              faq_1_question: faq_1_question,
                              faq_1_answer: faq_1_answer,
                              faq_2_question: faq_2_question,
                              faq_2_answer: faq_2_answer,
                              faq_3_question: faq_3_question,
                              faq_3_answer: faq_3_answer,
                              faq_4_question: faq_4_question,
                              faq_4_answer: faq_4_answer,
                              faq_5_question: faq_5_question,
                              faq_5_answer: faq_5_answer
                         })
                         .then(function() {
                              console.log("Document successfully written!");
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                    } else {
                         db.collection('users').doc(uid).collection('social').doc('faq').set({
                              faq_1_question: faq_1_question,
                              faq_1_answer: faq_1_answer,
                              faq_2_question: faq_2_question,
                              faq_2_answer: faq_2_answer,
                              faq_3_question: faq_3_question,
                              faq_3_answer: faq_3_answer,
                              faq_4_question: faq_4_question,
                              faq_4_answer: faq_4_answer,
                              faq_5_question: faq_5_question,
                              faq_5_answer: faq_5_answer
                         })
                         .then(function() {
                              console.log("Document successfully written!");
                         })
                         .catch(function(error) {
                              console.error("Error writing document: ", error);
                         });
                    }
                    this.setState({
                         faqUpdated: true
                    })
               })
     }

     componentDidMount() {
          let user = auth.currentUser.uid
          let getuser = db.collection('users').doc(user);
          let getDoc = getuser.get().then((doc) => {
               console.log(doc.data())
               this.setState({ currentuserpro: doc.data().isPro, currentuser: doc.data() })

          }

          ).catch(e => console.log(e))

          auth.onAuthStateChanged(function (user) {
               if (user) {
                    this.setState({
                         uid: user.uid,
                         email: user.email,
                         phoneNumber: user.phoneNumber,
                    })
                    // console.log('This is the user: ', this.state.uid );
                    db.collection('users').doc(this.state.uid).get()
                         .then((doc) => {
                              const data = doc.data();
                              if (data.firstName) { this.setState({ firstName: data.firstName }) }
                              if (data.lastName) { this.setState({ lastName: data.lastName }) }
                              if (data.photoURL) { this.setState({ photoURL: `${data.photoURL}` }) }
                              if (data.about) { this.setState({ about: data.about }) }
                              if (data.background) { this.setState({ background: data.background }) }
                              if (data.certifications) { this.setState({ certifications: data.certifications }) }
                              if (data.funFact) { this.setState({ funFact: data.funFact }) }
                              if (data.favQuote) { this.setState({ favQuote: data.favQuote }) }
                              if (data.ratesOnline) { this.setState({ ratesOnline: data.ratesOnline }) }
                              if (data.ratesInPerson) { this.setState({ ratesInPerson: data.ratesInPerson }) }
                              if (data.professions) { this.setState({ professions: data.professions }) }
                              if (data.travel) { this.setState({ travel: data.travel }) }
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })

                    db.collection('users').doc(this.state.uid).collection('social').doc('external').get()
                         .then((doc) => {
                              const data = doc.data();
                              if (data.facebook) { this.setState({ facebook: data.facebook }) }
                              if (data.twitter) { this.setState({ facebook: data.twitter }) }
                              if (data.linkedIn) { this.setState({ facebook: data.linkedIn }) }
                              if (data.pinterest) { this.setState({ facebook: data.pinterest }) }
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })

                    db.collection('users').doc(this.state.uid).collection('social').doc('faq').get()
                         .then((doc) => {
                              const data = doc.data();
                              if (data.faq_1_question) { this.setState({ faq_1_question: data.faq_1_question }) }
                              if (data.faq_1_answer) { this.setState({ faq_1_answer: data.faq_1_answer }) }
                              if (data.faq_2_question) { this.setState({ faq_2_question: data.faq_2_question }) }
                              if (data.faq_2_answer) { this.setState({ faq_2_answer: data.faq_2_answer }) }
                              if (data.faq_3_question) { this.setState({ faq_2_question: data.faq_3_question }) }
                              if (data.faq_3_answer) { this.setState({ faq_2_answer: data.faq_3_answer }) }
                              if (data.faq_4_question) { this.setState({ faq_2_question: data.faq_4_question }) }
                              if (data.faq_4_answer) { this.setState({ faq_2_answer: data.faq_4_answer }) }
                              if (data.faq_5_question) { this.setState({ faq_2_question: data.faq_5_question }) }
                              if (data.faq_5_answer) { this.setState({ faq_2_answer: data.faq_5_answer }) }
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })
               }
          }.bind(this));
     }

     render() {
          // console.log(this.state.photoURL)
          return (
               <section className={'dashboard__profile'}>
                    
                    <div className={'dashboard__profile-container'}>
                         <div className={'dashboard__profile-title'}>
                              <h2>Profile</h2>
                         </div>

                         <Tabs>
                              <TabList>
                                   { this.props.data.isPro === true ?
                                        <Tab>Specialities</Tab>
                                   : '' }
                                   <Tab>Image</Tab>
                                   <Tab>Profile</Tab>
                                   { this.props.data.isPro === true ?
                                        <Tab>Social</Tab>
                                   : '' }
                                   { this.props.data.isPro === true ?
                                        <Tab>FAQ</Tab>
                                   : '' }
                              </TabList>

                              { this.props.data.isPro === true ?
                                   <TabPanel>
                                        <h2>Update Specialities</h2>
                                        <Checkbox />
                                   </TabPanel>
                              : '' }

                              <TabPanel>
                                   <h2>Update Image</h2>
                                   <section className={'profile-image'}>
                                        <div className={'profile-image__body'}>
                                             <Image image={this.state.photoURL} user={this.state.uid} alt={`${this.state.firstName} ${this.state.lastName}'s profile picture`} />
                                             <ImageUpload user={this.state.uid} modal={this.modal} />
                                        </div>
                                   </section>
                              </TabPanel>

                              <TabPanel>
                                   <h2>Update Profile</h2>
                                   <section className={'user-profile'}>
                                        <form className={'user-profile__form'} onSubmit={this.updateProfile}>
                                             <fieldset className={'form__container'}>
                                                  <div className={'form__row form__row--double'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor='name__first'>First Name*</label>
                                                            <input id='firstName' type='text' name='name__first' value={this.state.firstName} onChange={this.handleChange} placeholder='First Name' />
                                                       </div>
                                                       <div className={'form__col'}>
                                                            <label htmlFor='name__last'>Last Name*</label>
                                                            <input id='lastName' type='text' name='name__last' value={this.state.lastName} onChange={this.handleChange} placeholder='Last Name' />
                                                       </div>
                                                  </div>
                                                  <div className={'form__row--double' + this.state.isPro ? '' : 'hide'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'rates-online'}>Online</label>
                                                            <input id={'ratesOnline'} type={'number'} name={'rates-online'} defaultValue={this.state.ratesOnline} onChange={this.handleChange} required />
                                                       </div>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'inperson'}>In-Person</label>
                                                            <input id={'ratesInPerson'} type={'number'} name={'inperson'} defaultValue={this.state.ratesInPerson} onChange={this.handleChange} required />
                                                       </div>
                                                  </div>
                                                  <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'about'}>About (HTML Allowed)</label>
                                                            <textarea id={'about'} name={'about'} defaultValue={this.state.about} onChange={this.handleChange}></textarea>
                                                       </div>
                                                  </div>
                                                  <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'background'}>Background (HTML Allowed)</label>
                                                            <textarea id={'background'} name={'background'} defaultValue={this.state.background} onChange={this.handleChange}></textarea>
                                                       </div>
                                                  </div>
                                                  <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'certifications'}>Certifications (HTML Allowed)</label>
                                                            <textarea id={'certifications'} name={'certifications'} defaultValue={this.state.certifications} onChange={this.handleChange}></textarea>
                                                       </div>
                                                  </div>
                                                  <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'funfact'}>Fun Fact (HTML Allowed)</label>
                                                            <textarea id={'funFact'} name={'funfact'} defaultValue={this.state.funFact} onChange={this.handleChange}></textarea>
                                                       </div>
                                                  </div>
                                                  <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <label htmlFor={'favquote'}>Favorite Quote (HTML Allowed)</label>
                                                            <textarea id={'favQuote'} name={'favquote'} defaultValue={this.state.favQuote} onChange={this.handleChange}></textarea>
                                                       </div>
                                                  </div>
                                                  {/* <div className={'form__row'}>
                                                       <div className={'form__col'}>
                                                            <legend>Location Preference</legend>
                                                            <label htmlFor="travel">Your current preference is set to <strong>{ this.state.travel ? 'on' : 'off'}</strong>.</label>
                                                            <input id="travel" type='checkbox' onChange={ this.handleChange } />
                                                       </div>
                                                  </div> */}
                                             </fieldset>
                                             <div className={'form__submit'}>
                                                  <input type='submit' value='Update' className={'button button--secondary'} />
                                             </div>
                                        </form>
                                        <div className={ this.state.profileFormUpdated ? 'fade fade--in' : 'fade' } style={{paddingTop:10}}>
                                             <MuiAlert color="success" variant="filled">
                                                  <p>Profile Updated!</p>
                                             </MuiAlert>
                                        </div>
                                   </section>
                              </TabPanel>
                              
                              { this.props.data.isPro === true ?
                                   <TabPanel>
                                        <h2>Social Accounts</h2>
                                        <section className={'social'}>
                                             <form className={'social__form'} onSubmit={this.updateSocial}>
                                                  <legend style={{ marginBottom:20}}>Add Your Social URLs below</legend>
                                                  <fieldset className={'form__container'}>
                                                       <div className={'form__row'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='facebook'>Facebook</label>
                                                                 <input id='facebook' type='url' name='facebook' value={this.state.facebook} onChange={this.handleChange} placeholder='Facebook Url' />
                                                            </div>
                                                       </div>
                                                       <div className={'form__row'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='twitter'>Twitter</label>
                                                                 <input id='twitter' type='url' name='twitter' value={this.state.twitter} onChange={this.handleChange} placeholder='Twitter Url' />
                                                            </div>
                                                       </div>
                                                       <div className={'form__row'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='linkedIn'>Linked In</label>
                                                                 <input id='linkedIn' type='url' name='linkedIn' value={this.state.linkedIn} onChange={this.handleChange} placeholder='LinkedIn Url' />
                                                            </div>
                                                       </div>
                                                       <div className={'form__row'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='pinterest'>Pinterest</label>
                                                                 <input id='pinterest' type='url' name='pinterest' value={this.state.pinterest} onChange={this.handleChange} placeholder='Pinterest Url' />
                                                            </div>
                                                       </div>
                                                  </fieldset>
                                                  <div className={'form__submit'}>
                                                       <input type='submit' value='Update' className={'button button--secondary'} />
                                                  </div>
                                             </form>

                                             <div className={ this.state.socialFormUpdated ? 'fade fade--in' : 'fade' } style={{paddingTop:10}}>
                                                  <MuiAlert color="success" variant="filled">
                                                       <p>Profile Updated!</p>
                                                  </MuiAlert>
                                             </div>
                                        </section>
                                   </TabPanel>
                              : '' }

                              { this.props.data.isPro === true ?
                                   <TabPanel>
                                        <h2>FAQ</h2>
                                        <section className={'faq-post'}>
                                             <form className={'faq-post__form'} onSubmit={this.updateFAQ}>
                                                  <legend style={{ marginBottom:20}}>
                                                       <p>Add your top 5 frequently asked questions.</p>
                                                       <p>Examples:</p>
                                                       <ul>
                                                            <li>What should the customer know about your pricing?</li>
                                                            <li>What type of customers have you worked with?</li>
                                                            <li>What is your typical process for working with a new customer?</li>
                                                       </ul>
                                                  </legend>
                                                  <fieldset className={'form__container'}>

                                                       <div className={'form__row blocked'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_1_question'>Question</label>
                                                                 <input id='faq_1_question' type='text' name='faq_1' value={this.state.faq_1_question} onChange={this.handleChange} placeholder='Question' />
                                                            </div>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_1_answer'>Answer</label>
                                                                 <textarea id='faq_1_answer' name='faq_1' value={this.state.faq_1_answer} onChange={this.handleChange} placeholder='Answer'></textarea>
                                                            </div>
                                                       </div>

                                                       <div className={'form__row blocked'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_2_question'>Question</label>
                                                                 <input id='faq_2_question' type='text' name='faq_2' value={this.state.faq_2_question} onChange={this.handleChange} placeholder='Question' />
                                                            </div>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_2_answer'>Answer</label>
                                                                 <textarea id='faq_2_answer' name='faq_2' value={this.state.faq_2_answer} onChange={this.handleChange} placeholder='Answer'></textarea>
                                                            </div>
                                                       </div>

                                                       <div className={'form__row blocked'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_3_question'>Question</label>
                                                                 <input id='faq_3_question' type='text' name='faq_3' value={this.state.faq_3_question} onChange={this.handleChange} placeholder='Question' />
                                                            </div>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_3_answer'>Answer</label>
                                                                 <textarea id='faq_3_answer' name='faq_3' value={this.state.faq_3_answer} onChange={this.handleChange} placeholder='Answer'></textarea>
                                                            </div>
                                                       </div>

                                                       <div className={'form__row blocked'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_4_question'>Question</label>
                                                                 <input id='faq_4_question' type='text' name='faq_4' value={this.state.faq_4_question} onChange={this.handleChange} placeholder='Question' />
                                                            </div>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_4_answer'>Answer</label>
                                                                 <textarea id='faq_4_answer' name='faq_4' value={this.state.faq_4_answer} onChange={this.handleChange} placeholder='Answer'></textarea>
                                                            </div>
                                                       </div>
                                                       
                                                       <div className={'form__row blocked'}>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_5_question'>Question</label>
                                                                 <input id='faq_5_question' type='text' name='faq_5' value={this.state.faq_5_question} onChange={this.handleChange} placeholder='Question' />
                                                            </div>
                                                            <div className={'form__col'}>
                                                                 <label htmlFor='faq_5_answer'>Answer</label>
                                                                 <textarea id='faq_5_answer' name='faq_5' value={this.state.faq_5_answer} onChange={this.handleChange} placeholder='Answer'></textarea>
                                                            </div>
                                                       </div>

                                                  </fieldset>
                                                  <div className={'form__submit'}>
                                                       <input type='submit' value='Update' className={'button button--secondary'} />
                                                  </div>
                                             </form>

                                             <div className={ this.state.faqUpdated ? 'fade fade--in' : 'fade' } style={{paddingTop:10}}>
                                                  <MuiAlert color="success" variant="filled">
                                                       <p>FAQ Updated!</p>
                                                  </MuiAlert>
                                             </div>
                                        </section>
                                   </TabPanel>
                              : '' }

                         </Tabs>

                    </div>
               </section>
          );
     }
}

export default Profile;