import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { auth, db } from '../Firebase/Firebase';
import List from "./listfollowed/list";

class Highlights extends Component {

     constructor(props) {
          super(props);
          this.state = {
               requests: [],
               messages: [],
               bookmarkedProfiles: [],
               isPro: false
          }
          this.renderBookingRequests = this.renderBookingRequests.bind(this);
          this.renderBookmarkedProfiles = this.renderBookmarkedProfiles.bind(this);
          // this.renderUserNamebyUID = this.renderUserNamebyUID.bind(this);
     }

     renderBookmarkedProfiles = () => {
          const { bookmarkedProfiles } = this.state;
          return (
               bookmarkedProfiles.map((uid, index) => (
                    <li key={index}>{this.renderUserNamebyUID(uid, index)}</li>
               ))
          );
     }

     renderMessageRequests = () => {
          const { messages } = this.state;
          if (messages.length !== 0) {
               var msgCount = messages.length + 1;
               return msgCount;
          }
          return '0';
     }

     renderBookingRequests = () => {
          const { requests } = this.state;
          if (requests.length !== 0) {
               return requests.length;
          }
          return '0';
     }

     componentDidMount() {
          let currentComponent = this;
          this.setState({ isPro: auth.currentUser.isPro })
          auth.onAuthStateChanged(function (user) {
               if (user) {
                    db.collection('users').doc(user.uid).collection('bookingRequests').get()
                         .then(function (querySnapshot) {
                              querySnapshot.forEach(function (doc) {
                                   currentComponent.setState((prevState) => ({
                                        requests: [...prevState.requests, {}]
                                   }))
                              })
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })

                    db.collection('users').doc(user.uid).collection('messages').get()
                         .then(function (querySnapshot) {
                              querySnapshot.forEach(function (doc) {
                                   currentComponent.setState((prevState) => ({
                                        messages: [...prevState.messages, {}]
                                   }))
                              })
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })

                    db.collection('users').doc(user.uid).collection('social').doc('following').get()
                         .then((doc) => {
                              const data = doc.data();
                              const Array = []
                              db.collection('users').get().then(snapshot => {
                                   snapshot.forEach(doc => {
                                        Array.push(doc.data())
                                   });
                                   let Arrayfollowed = []
                                   for (let i = 0; i < data.accounts.length; i++) {
                                        Arrayfollowed.push(Array.find((item) => item.uid === data.accounts[i]))
                                   }
                                   this.setState({
                                        bookmarkedProfiles: Arrayfollowed
                                   })
                              }).catch(e => console.log(e))
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })
               }
          }.bind(this));
     }

     render() {

          return (
               <section className={'dashboard__highlights'}>

                    <div className={'dashboard__highlights-container'}>
                         <div className={'dashboard__highlights-welcome'}>
                              <h2 className={'text--capitalize'}>Hey, {this.props.data.firstName}!</h2>
                         </div>
                         <div className={'dashboard__highlights-boxes'}>

                              <div className={'dashboard__highlights-box'}>
                                   <div className={'dashboard__highlights-box-num'}>
                                        <Link to='/dashboard/messages'>
                                             {this.renderMessageRequests()}
                                        </Link>
                                   </div>
                                   <div className={'dashboard__highlights-box-content'}>
                                        <Link to='/dashboard/messages'>
                                             New Messages
                                        </Link>
                                   </div>
                              </div>

                              {this.props.data.isPro === true ?
                                   <div className={'dashboard__highlights-box'}>
                                        <div className={'dashboard__highlights-box-num'}>
                                             <Link to='/dashboard/booking-requests'>
                                                  {this.renderBookingRequests()}
                                             </Link>
                                        </div>
                                        <div className={'dashboard__highlights-box-content'}>
                                             <Link to='/dashboard/booking-requests'>
                                                  Booking Requests
                                             </Link>
                                        </div>
                                   </div>
                                   : ''}

                              {/* <div className={'dashboard__highlights-box'}>
                                   <div className={'dashboard__highlights-box-num'}>
                                        1
                                   </div>
                                   <div className={'dashboard__highlights-box-content'}>
                                        New Payment Pending
                                   </div>
                              </div> */}

                         </div>
                    </div>

                    <div className={'dashboard__highlights-container'}>
                         { this.state.bookmarkedProfiles === '' ? (
                              <List followed={this.state.bookmarkedProfiles}></List>
                         ) : ''}
                    </div>

               </section>
          );
     }
}

export default Highlights;