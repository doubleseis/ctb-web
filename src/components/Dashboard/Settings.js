import { Button } from "@material-ui/core";
import React from 'react';
import { auth, db } from "../../components/Firebase/Firebase";
import MediaCard from "./Settings/DeleteAccount";
import UpgradeMembership from "./Settings/UpgradeMembership";

const Settings = () => {

  const [users,setusers]=React.useState([])
  const [currentuser,setcurrentuser]=React.useState()
  const [value,setvalue]=React.useState()
  const [userinfo,setuserinfo]=React.useState({})

  React.useEffect(() => {

    let user = auth.currentUser;

    let current = db.collection('users').doc(user.uid).get().then(
      (doc)=>setuserinfo(doc.data())
    )

    setcurrentuser(user);

    let usersArray = [];
    let userRef = db.collection('users');
    let users = userRef.get()
      .then( snapshot => {
        snapshot.forEach(doc => {
          usersArray.push(doc.data())
        });
        setusers(usersArray)
      })
      .catch(err => {
        console.log('Error getting documents', err);
      });

  }, [] );


  const handleChange = (e) => {
    setvalue(e.target.value)
  }

  const submit = () => {
    let Filter = users.filter( item => item.username === value );
    // console.log( Filter );
    if( Filter.length > 0 ){
     console.log(`you cant get ${value} `)
    } else {
      db.collection('users').doc(currentuser.uid).update({
        'username': value
      })
    }
  }
  
  // console.log(userinfo)

  return (
    <section className="dashboard__settings">
      <div className="dashboard__settings-container container">
        <div className={'dashboard__settings-title'}>
          <h2>Settings</h2>
        </div>
        
        { userinfo.isProPremium !== true ? <UpgradeMembership /> : ''}

        { userinfo && (
          <div className={'change-username blocked'}>
            <h3>Change Username</h3>
            { userinfo.username ? (
              <p>Your current username is <strong>{ userinfo.username}</strong>.</p>
            ) : ''}

              { userinfo.isPremium ?
                <div className="search__input">
                  <input type='text' placeholder={'Enter New Username'} className="searchs" onChange={handleChange} />
                  <Button variant="contained" color="primary" onClick={submit}>
                    Change Username
                  </Button>
                </div>
              :
                <div className="search__input">
                  <p><strong>Upgrade membership</strong> to update username.</p>
                  <input type='text' placeholder={'Enter New Username'} className="searchs" value={ userinfo.username } style={{ marginBottom: 10}}/>
                  <Button disabled="true" variant="contained" color="primary">
                    Change Username
                  </Button>
                </div>
              }
               
          </div>
        )}
        
        <MediaCard />
      </div>
    </section>
  );
}

export default Settings;