import Button from "@material-ui/core/Button";
import React from "react";
import SimpleModal from "../../ModalSecondary/modal";

const DeleteAccount = () => {

     const [open, setopen] = React.useState(false);
     //   console.log(open);

     const close = () => {
          setopen(false);
     };

     return (
          <div className={'delete-account blocked'}>
               <h3>Delete Account</h3>
               <p>Permanently delete your account and all of your content.</p>
               <Button variant="contained" color="secondary" onClick={() => { setopen(!open); }}>Delete</Button>
               <SimpleModal open={open} close={close}></SimpleModal>
          </div>
     );
}

export default DeleteAccount;