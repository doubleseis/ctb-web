import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '@material-ui/core';
import React, { Component } from 'react';
import Modal from '../../Modal/ModalView';
import Paypal from '../../Paypal';

class UpgradeMembership extends Component {

     constructor(props) {
          super(props)
          this.state = {}
          this.handleClick = this.handleClick.bind(this)
          this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
     }

     handleClick(){
          this.setState({
               condition: !this.state.condition
          })
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }
     
     render(){
          return(
               <div className={'upgrade-membership blocked'}>
                    <h3>Upgrade Membership</h3>
                    <p>You are currently enrolled in the free Pro membership. Upgrade to Pro Premium to unlock new features.</p>
                    <Button variant="contained" color="primary" onClick={this.openModal}>Upgrade</Button>
                    <Modal 
                         isModalOpen={this.state.isModalOpen}
					closeModal={this.closeModal}
                    >
                         <h2>Upgrade</h2>
                         <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                         <Paypal />
                    </Modal>
               </div>
          )
     }
}

export default UpgradeMembership;