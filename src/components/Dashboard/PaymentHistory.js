
import React, { Component } from 'react';

class PaymentHistory extends Component {
     render() {
          return (
               <section className={'dashboard__history'}>
                    <div className={'dashboard__history-container'}>
                         <div className={'dashboard__history-title'}>
                              <h2>Payment History</h2>
                         </div>
                         <div className={'dashboard__history-table'}>
                              <table>
                                   <tr>
                                        <th>Name</th>
                                        <th>Invoice Date</th>
                                        <th>Invoice Status</th>
                                        <th>Actons</th>
                                   </tr>
                                   <tr>
                                        <td>Pro</td>
                                        <td>Feb 28, 2020</td>
                                        <td>Paid</td>
                                        <td>
                                             <button>View Invoice</button>
                                        </td>
                                   </tr>
                              </table>
                         </div>
                    </div>
               </section>
          );
     }
}

export default PaymentHistory;