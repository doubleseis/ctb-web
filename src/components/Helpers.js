import { faFacebookF, faLinkedin, faPinterestP, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

// Share Page to Social Channels
export function socialShare(pageName) {
     var pageName;
     return (
          <ul className={'social-share'}>
               <li>Share { pageName }:</li>
               <li><a target={'blank'} href={'http://www.facebook.com/sharer/sharer.php?u=' + window.location.href}><FontAwesomeIcon icon={faFacebookF} /></a></li>
               <li><a target={'blank'} href={'https://twitter.com/intent/tweet?text=Choose%20To%20Be%20You&url=' + window.location.href}><FontAwesomeIcon icon={faTwitter} /></a></li>
               <li><a target={'blank'} href={'http://pinterest.com/pin/create/button/?url=' + window.location.href}><FontAwesomeIcon icon={faPinterestP} /></a></li>
          </ul>
     );
}


export function proShare(fb, tw, pi, li) {
     var fb, tw, pi, li;

     return (
          <ul className={'social-share-pro'}>
               { fb.length ? (<li><a target={'blank'} href={ fb }><FontAwesomeIcon icon={faFacebookF} /></a></li>) : '' }
               { tw.length ? (<li><a target={'blank'} href={ tw }><FontAwesomeIcon icon={faTwitter} /></a></li>) : '' }
               { li.length ? (<li><a target={'blank'} href={ li }><FontAwesomeIcon icon={faLinkedin} /></a></li>) : '' }
               { pi.length ? (<li><a target={'blank'} href={ pi }><FontAwesomeIcon icon={faPinterestP} /></a></li>) : '' }
          </ul>
     );
}

export function removeUnderscore(string) {
     var string = string;
     var fixedString = string.split('_').join(' ');
     return fixedString;
}