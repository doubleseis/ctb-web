import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { fire } from './Firebase/Firebase';

class LogIn extends Component {
     constructor(props) {
          super(props);
          this.authWithEmailPassword = this.authWithEmailPassword.bind(this);
          this.forgotPassword = this.forgotPassword.bind(this);
          this.state = {
               redirect: false
          }
     }

     authWithEmailPassword(event) {
          event.preventDefault();
          
          const email = this.emailInput.value;
          const password = this.passwordInput.value;

          fire.auth().fetchSignInMethodsForEmail(email)
               .then((providers) => {
                    if (providers.length === 0) {
                         // create user
                         // return fire.auth().createUserWithEmailAndPassword(email,password);
                         this.loginForm.reset();
                         console.log('please create new account');
                    } else if (providers.indexOf('password') === -1 ) {
                         // they used a social
                         this.loginForm.reset();
                    } else {
                         // sign user in
                         return fire.auth().signInWithEmailAndPassword(email,password);
                    }
               })
               .then((user) => {
                    if (user && user.email) {
                         this.loginForm.reset();
                         this.props.setCurrentUser(user);
                         this.setState({redirect: true});
                    }
               })
               .catch((error) => {
                    console.log(error);
               })
     }

     forgotPassword = (e) => {
          e.preventDefault();
          const { action } = this.props;
          action();
          return window.location.href = "/forgot-password";
     }

     render(){
          if ( this.state.redirect === true ) {
               return <Redirect to='/dashboard' />
          }
          return (
               <div>
                    {/* <button className={'btn btn--primary'}>Login</button> */}

                    <form className={'form'} onSubmit={(event) => { this.authWithEmailPassword(event)} } ref={(form) => { this.loginForm = form }}>
                         <div className={'form__row'}>
                              <div className={'form__col'}>
                                   <label>Email*</label>
                                   <input type='email' name='email' ref={(input) => { this.emailInput = input }} />
                              </div>
                         </div>
                         <div className={'form__row'}>
                              <div className={'form__col'}>
                                   <label>Password*</label>
                                   <input type='password' name='password' ref={(input) => { this.passwordInput = input }} />
                              </div>
                         </div>
                         <div className={'form__row form__row--double'}>
                              <div className={'form__col'}>
                                   <button onClick={this.forgotPassword}>Forgot password?</button>
                              </div>
                              <div className={'form__col'}>
                                   {/* <input type='checkbox' /> Remember Me */}
                              </div>
                         </div>
                         <div className={'form__submit'}>
                              <input className={'button button--secondary'} type='submit' value='Log In' />
                         </div>
                    </form>
               </div>
          )
     }
}

export default LogIn;