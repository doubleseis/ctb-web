import * as firebase from 'firebase';
import React, { Component } from 'react';
import Calendar from 'react-calendar';
import ReactHtmlParser from 'react-html-parser';
import MultiStep from 'react-multistep';
import Alert from "../Alert/index";
import { db } from '../Firebase/Firebase';

class ProPricing extends Component {
     constructor(props){
          super(props);
          this.state = {
               date: new Date(),
               availability: this.props.availability,
               rateSelected: '',
               timeSelected: '---',
               locationPreferenceSelected: ''
          }
          this.onDateChange = this.onDateChange.bind(this);
          this.onLocChange = this.onLocChange.bind(this);
          this.renderDateSelected = this.renderDateSelected.bind(this);
          this.renderAvailability = this.renderAvailability.bind(this);
          // this.addTimeSlot = this.addTimeSlot.bind(this);
     }

     onDateChange = (date) => {
          this.setState({ date });
     }

     onRateChange = (e) => {
          this.setState({
               rateSelected: e.target.id
          })
     }

     onLocChange = (e) => {
          this.setState({
               locationPreferenceSelected: e.target.id
          })
     }

     renderDateSelected = (dateSelected) => {
          const selected = dateSelected.toLocaleString('en-us', { month: 'long', day: 'numeric', year: 'numeric' });
          return selected;
     }

     renderAvailability = (availability, selectedDate) => {
          const availTimes = availability;
          
          if(availTimes){
               availTimes.map((avail, index)=> {
                    const convertedDate = new Date((avail.seconds*1000)).toLocaleString('en-us', { year: 'numeric', day: 'numeric', month: '2-digit'  })
                    const convertedSelection = selectedDate.toLocaleString('en-us', { year: 'numeric', day: 'numeric', month: '2-digit'  })
                    // console.log(convertedDate, convertedSelection);
                    if ( convertedDate !== convertedSelection) {
                         // this.setState({
                         //      openDates: ['none']
                         // })
                    }
                    // returnDates = [convertedSelection]
                    // this.setState({
                    //      openDates: ['time']
                    // })
                    // return 'dates';
               })
               return this.openDates;
          }
          // console.log( this.state.openDates );
          
          return false;
     }
     
     addTimeSlot = (e) => {
          e.preventDefault();
          var el = document.querySelectorAll('.time-slot__time');
          for (let i = 0; i < el.length; i++) {
               el[i].onclick = function() {
                    var c = 0;
                    while (c < el.length) {
                         el[c++].className = 'time-slot__time';
                    }
                    el[i].className = 'time-slot__time time-slot__time--selected';
               };
          }
          // e.preventDefault();
          this.setState({
               timeSelected: e.target.value
          })
          // e.target.classList.toggle('selected');

          console.log('clicked', e.target );
          // e.
     }

     submitBookingRequest = (e) => {
          e.preventDefault();
          var selectBox = document.getElementById('selectProfession');
          if(selectBox.value === 'none') {
               alert(`Please choose the Pro's profession`);
               return false;
          }
          var { rateSelected, date, availability, timeSelected, locationPreferenceSelected } = this.state;

          // Pro
          var proUid = this.props.proUid;
          var requestedDate = date.toLocaleString('en-us', { month: 'long', day: 'numeric', year: 'numeric' });
          var firstName = this.props.firstName;
          var lastName = this.props.lastName;

          //Requestor
          var requestorUid = this.props.user;
          var requestorFirstName = '';
          var requestorLastName = '';
          var reqDocName = '';
          var uniqueID = Math.random().toString(36).substr(2, 9);

          db.collection('users').doc(requestorUid).get()
               .then(function(doc) {
                    requestorFirstName = doc.data().firstName;
                    requestorLastName = doc.data().lastName;
                    console.log('inside promise', requestorFirstName, requestorLastName);
                    reqDocName = `req-${uniqueID}`;
                    db.collection(`users`).doc(requestorUid).collection('social').doc('following').update({
                         accounts: firebase.firestore.FieldValue.arrayUnion( proUid )
                    });
               })
               .then(function(){
                    db.collection(`users`).doc(proUid).collection('social').doc('following').update({
                         accounts: firebase.firestore.FieldValue.arrayUnion( requestorUid )
                    });
                    db.collection('users').doc(proUid).collection('bookingRequests').doc(reqDocName).set({
                         userRequestor: requestorUid,
                         date: date,
                         doc: reqDocName,
                         profession: selectBox.value,
                         status: 'pending',
                         rate: rateSelected,
                         time: timeSelected,
                         name: `${requestorFirstName} ${requestorLastName}`,
                         locationPreference: locationPreferenceSelected
                    })
                    return <Alert />;
               })
               .catch(function(error) {
                    console.log("Error getting document:", error);
                    return alert('Error. Please refresh and try again.');
               });
     }

     render() {
          // console.log(this.props.user);
          return (
               <section className={'pro-booking text-center'}>
                    <div className={'pro-booking__title'}>
                         <h2><strong>Book a Session in 3 Easy Steps</strong></h2>
                    </div>
                    <MultiStep showNavigation={true} steps={[
                         {
                              name:'Choose Date',
                              component:(
                                   <div className={'pro-booking__start'}>
                                        <Calendar onChange={this.onDateChange} value={this.state.date} />
                                        <p>Start <strong>booking your session</strong> { this.props.firstName ? ReactHtmlParser('with <strong>'+ this.props.firstName +'</strong>') : '' }<br/>by clicking choosing a date above and clicking 'Next' below.</p>
                                   </div>
                              )
                         },
                         {
                              name:'Choose Time',
                              component:(
                                   <div>
                                        { ReactHtmlParser( this.renderAvailability(this.props.availability, this.state.date) )}
                                        <div className="time-slot">
                                             <button className="time-slot__time" value={'09:00 AM'} onClick={ this.addTimeSlot }>9:00 AM</button>
                                             <button className="time-slot__time" value={'09:30 AM'} onClick={ this.addTimeSlot }>9:30 AM</button>
                                             <button className="time-slot__time" value={'10:00 AM'} onClick={ this.addTimeSlot }>10:00 AM</button>
                                             <button className="time-slot__time" value={'10:30 AM'} onClick={ this.addTimeSlot }>10:30 AM</button>
                                             <button className="time-slot__time" value={'11:00 AM'} onClick={ this.addTimeSlot }>11:00 AM</button>
                                             <button className="time-slot__time" value={'11:30 AM'} onClick={ this.addTimeSlot }>11:30 AM</button>
                                             <button className="time-slot__time" value={'12:00 PM'} onClick={ this.addTimeSlot }>12:00 PM</button>
                                             <button className="time-slot__time" value={'12:30 PM'} onClick={ this.addTimeSlot }>12:30 PM</button>
                                             <button className="time-slot__time" value={'1:00 PM'} onClick={ this.addTimeSlot }>1:00 PM</button>
                                             <button className="time-slot__time" value={'1:30 PM'} onClick={ this.addTimeSlot }>1:30 PM</button>
                                             <button className="time-slot__time" value={'2:00 PM'} onClick={ this.addTimeSlot }>2:00 PM</button>
                                             <button className="time-slot__time" value={'2:30 PM'} onClick={ this.addTimeSlot }>2:30 PM</button>
                                             <button className="time-slot__time" value={'3:00 PM'} onClick={ this.addTimeSlot }>3:00 PM</button>
                                             <button className="time-slot__time" value={'3:30 PM'} onClick={ this.addTimeSlot }>3:30 PM</button>
                                             <button className="time-slot__time" value={'4:00 PM'} onClick={ this.addTimeSlot }>4:00 PM</button>
                                             <button className="time-slot__time" value={'4:30 PM'} onClick={ this.addTimeSlot }>4:30 PM</button>
                                             <button className="time-slot__time" value={'5:00 PM'} onClick={ this.addTimeSlot }>5:00 PM</button>
                                             <button className="time-slot__time" value={'5:00 PM'} onClick={ this.addTimeSlot }>5:30 PM</button>
                                        </div>
                                   </div>
                              )
                         },
                         {
                              name:'Review',
                              component:(
                                   <form className={'pro-booking__form'}>
                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label htmlFor={'date_requested'}>Date Requested</label>
                                                  <input id={'date_requested'} type={'text'} value={this.renderDateSelected(this.state.date)}/>
                                             </div>
                                        </div>

                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label>Time Requested</label>
                                                  <input type={'text'} value={ this.state.timeSelected }/>
                                             </div>
                                        </div>

                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label className={'screen-reader-text'}>Choose Profession</label>
                                                  <select id={'selectProfession'}>
                                                       <option value="none" selected>Select Profession</option>
                                                       { this.props.professions.Chef ?
                                                            <option value="chef">Chef</option>
                                                       : '' }
                                                       { this.props.professions.FitnessTrainer ?
                                                            <option value="fitness_trainer">Fitness Trainer</option>
                                                       : '' }
                                                       { this.props.professions.MassageThreapist ?
                                                            <option value="massage_therapist">Massage Therapist</option>
                                                       : '' }
                                                       { this.props.professions.Nutritionist ?
                                                            <option value="nutritionist">Nutritionist</option>
                                                       : '' }
                                                  </select>
                                             </div>
                                        </div>

                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label>Service Type:</label>
                                                  <ul className={'list--unstyled'}>
                                                       <li>
                                                            <input id={'inPerson'} type={'radio'} name={'session'} onChange={ this.onRateChange } />
                                                            <label for={'inPerson'}> {'$' + this.props.ratesInPerson} per in person session</label>
                                                       </li>
                                                       <li>
                                                            <input id={'online'} type={'radio'} name={'session'} onChange={ this.onRateChange } />
                                                            <label for={'online'}> {'$' + this.props.ratesOnline} per online session</label>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>

                                        
                                        <div className={'form__row'}>
                                             <div className={'form__col'}>
                                                  <label>Location Preference:</label>
                                                  { this.props.travel ? (
                                                  <ul className={'list--unstyled'}>
                                                       <li>
                                                            <input id={'incall'} type={'radio'} name={'location_preference'} onChange={ this.onLocChange } />
                                                            <label for={'incall'}> Go to Pro</label>
                                                       </li>
                                                       <li>
                                                            <input id={'outcall'} type={'radio'} name={'location_preference'} onChange={ this.onLocChange } />
                                                            <label for={'outcall'}> Have the Pro travel to you</label>
                                                       </li>
                                                  </ul>
                                                  ) : 'This Pro is only available for in-calls.' } 
                                             </div>
                                        </div>

                                        <input className={'button button--secondary'} type={'submit'} onClick={this.submitBookingRequest} value={'Request Booking'} />
                                   </form>
                              )
                         }

                    ]} />
               </section>
          );
     }
}
   
export default ProPricing;