import { faFacebookSquare, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { auth, db, fb, google } from '../Firebase/Firebase';
import LogIn from '../LogIn';
import Modal from '../Modal/ModalView';

class ProPricingHidden extends Component {
     constructor(props) {
          super(props)
          this.state = {
               condition: false,
               isModalOpen: false,
			isInnerModalOpen: false
          }
          this.handleClick = this.handleClick.bind(this)
          this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
     }

     handleClick(){
          this.setState({
               condition: !this.state.condition
          })
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }
     
     handleFacebookLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(fb)
               .then(({ user }) => {
                    this.setState({ user })
                    // user = auth.currentUser
                    db.collection('users').doc(user.uid).set({
                         firstName: name__first,
                         lastName: name__last,
                         isAdmin: isAdmin
                    })
                    .then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/dashboard";
                    })
               })
               // .catch(function(error) {
               //      var errorCode = error.code;
               //      var errorMessage = error.message;
               //      console.log('Error code for creating new user:', errorCode);
               //      console.log('Error msg for creating new user:', errorMessage);
               //      alert(errorMessage);
               // });
     }

     handleGoogleLogin = (e) => {
          e.preventDefault();
          const { name__first, name__last, isAdmin } = this.state;
          auth.signInWithPopup(google)
               .then(function(result) {
                    var token = result.credential.accessToken;
                    var user_id = result.user.uid;
                    var user_first_name = result.additionalUserInfo.profile.given_name;
                    var user_last_name = result.additionalUserInfo.profile.family_name;
                    var user_image_url = result.additionalUserInfo.profile.picture;
                    var user_creation_time = result.user.creationTime;

                    console.log(result);
               
                    db.collection('users').doc(user_id).set({
                         uid: user_id,
                         firstName: user_first_name,
                         lastName: user_last_name,
                         isAdmin: isAdmin,
                         finishedOnboarding: false,
                         photoURL: user_image_url,
                    }).then(function() {
                         console.log("Document successfully written!");
                         window.location.href = "/dashboard";
                    }).catch(function(error) {
                         console.error("Error writing document: ", error);
                    });
               })
               .catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...
                    console.error("Error: ", error);
               });
     }
     
     render() {
          // Create a modal reusable component to connect the login button
          return (
               <section className={'pro-pricing__hidden'}>
                    <div className={'pro-pricing__hidden-content'}>
                         <h2>Login To See Availabilities</h2>
                         <p>You must be logged in to see the Pro's availability and prices.</p>
                         <div className={'inline-buttons'}>
                              <button onClick={this.openModal} className={'button button--primary'}>Login</button>
                              <a href={'/register'} className={'button button--primary-contrast'}>Join for Free</a>
                         </div>
                    </div>

                    <Modal 
                         isModalOpen={this.state.isModalOpen}
					closeModal={this.closeModal}
                    >
                         <h2>Log In</h2>
                         <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                         <LogIn />
                         <div className={'social-logins__buttons'}>
                              <button className={'button button--secondary button--facebook'} onClick={this.handleFacebookLogin}><FontAwesomeIcon icon={faFacebookSquare} /> Login with Facebook</button>
                              <button className={'button button--secondary button--google'} onClick={this.handleGoogleLogin}><FontAwesomeIcon icon={faGoogle} /> Login with Google</button>
                         </div>
                    </Modal>
                    
               </section>
          );
     }
}
   
export default ProPricingHidden;