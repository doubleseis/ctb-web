import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';

class ContentBlock extends Component {
     constructor(props) {
          super(props);
          this.props = {
               modifier: props.modifier ? props.modifier : '',
               background: {
                    image: props.background.image,
                    position: props.background.position,
                    size: props.background.size,
                    repeat: props.background.repeat
               },
               image: props.image,
               spacing: props.spacing,
               columns: props.columns,
               title: props.title,
               titleSize: props.titleSize,
               content: props.content,
               centerBlockAlignment: Boolean,
               textColor: props.textColor,
               textStyle: props.textStyle,
               textAlignment: props.textAlignment,
               iframe : {
                    display: Boolean,
                    src: props.iframe.src,
                    autoPlay: Boolean
               },
               button: {
                    link: props.button.link,
                    title: props.button.title,
                    style: props.button.style
               }
          }
     }

     renderVideo = () => {
          return (
               <div className={'content-block__video'}>
                    <div className={'embed embed--responsive'}>
                         <iframe className={'embed__item'} src={this.props.iframe.src} frameBorder={'0'} allowFullScreen></iframe>
                    </div>
               </div>
          );
     }
     
     render(){
          const contentBlockStyle = {
               backgroundImage: 'url(' + this.props.background.image + ')',
               backgroundSize: this.props.background.size,
               backgroundPosition: this.props.background.position,
               backgroundRepeat: this.props.background.repeat,
               color: this.props.textColor,
               textAlign: this.props.textAlignment,
          }
          const textBlockStyle = {
               fontStyle: this.props.textStyle
          }

          const renderButton = <a href={this.props.button.link} className={'button button--' + this.props.button.style}>{this.props.button.title}</a>;
          const renderTitleSize = this.props.titleSize ? ' content-block__headline--' + this.props.titleSize : '';

          return (
               <section className={'content-block content-block--' + this.props.modifier + ' content-block--spacing-' + this.props.spacing} style={contentBlockStyle}>
                    <div className={'content-block__container container'}>
                         <div className={'content-block__inner content-block__inner--' + this.props.columns + ''}>
                              {this.props.image !== undefined ? ReactHtmlParser('<div class="content-block__image"><img src="' + this.props.image + '" /></div>') : '' }
                              <div className={ this.props.centerBlockAlignment === true ? 'content-block__content content-block__content--center' : 'content-block__content'}>
                                   <h2 className={'content-block__headline' + renderTitleSize }>{this.props.title}</h2>
                                   {this.props.content ? <div style={textBlockStyle}>{ ReactHtmlParser (this.props.content) }</div> : '' }
                                   {this.props.iframe.display ? this.renderVideo() : ''}
                                   {this.props.button.link ? renderButton : '' }
                              </div>
                         </div>
                    </div>
               </section>
          )
     }
}

export default ContentBlock;