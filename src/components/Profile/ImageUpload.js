import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { db, storage } from '../Firebase/Firebase';
import Modal from '../Modal/ModalView';


class ImageUpload extends Component {
     constructor(props) {
          super(props);
          this.state = {
               image: null,
               url: '',
               progress: 0,
               photoURL: '',
               condition: false,
               isModalOpen: false,
			isInnerModalOpen: false
          }
          this.handleChange = this.handleChange.bind(this);
          this.handleUpload = this.handleUpload.bind(this);
          this.openModal = this.openModal.bind(this);
          this.closeModal = this.closeModal.bind(this);
     }

     handleChange = (e) => {
          if( e.target.files[0] ){
               const image = e.target.files[0];
               this.setState(() => ({
                    image: image
               }));
          }
     }

     handleUpload = (e) => {
          const { image } = this.state;
          const { user } = this.props;
          const uploadTask = storage.ref(`users/${user}/${image.name}`).put(image);
          uploadTask.on('state_changed',
               (snapshot) => {
                    // Progress
                    const progress = Math.ceil((snapshot.bytesTransferred / snapshot.totalBytes)*100);
                    this.setState({ progress });
               },
               (error) => {
                    // Errors
                    console.log('Error:', error);    
               },
               () => {
                    // Complete
                    storage.ref(`users/${user}`).child(image.name).getDownloadURL().then( url => {
                         // console.log(url);
                         this.setState({url});
                    })
                    db.collection(`users`).doc(user).update({
                         photoURL: image.name
                    });
                    this.setState({
                         isModalOpen: false
                    });
               });
     }

     openModal(e) {
          e.preventDefault();
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }
     
     renderModal = ( bool ) => {
          if(bool !== true) {
               return;
          }
          return (
               <Modal isModalOpen={this.state.isModalOpen} closeModal={this.closeModal} >
                    <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                    <progress value={ this.state.progress } max="100" />
                    <input type="file" onChange={ this.handleChange } />
                    <button className={'button button--primary'}  onClick={ this.handleUpload }>Upload Image</button>
               </Modal>
          );
     }

     render() {
          return (
               <div className={'profile-image__upload'}>
                    <button className={'button'} onClick={this.openModal}>Update Image</button>
                    { this.renderModal(this.props.modal) }
               </div>
          );
     }
}

export default ImageUpload;