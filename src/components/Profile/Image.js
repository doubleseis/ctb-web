import React, { Component } from 'react';
import defaultUserImage from '../../assets/images/default-user.jpg';
import { storage } from '../Firebase/Firebase';

class Image extends Component {
     


     constructor(props) {
          super(props);
this.currentimage=defaultUserImage
          this.state = {
               url: this.props.image ? this.props.image : defaultUserImage,
               isModalOpen: false,
          }
         
          this.renderImage = this.renderImage.bind(this);
     }
componentDidMount(){

     const { image, user } = this.props;

     console.log(user)
     const ref = storage.ref(`users/${user}/${image}`)
              ref.getDownloadURL()
                   .then((url) => {
                         console.log(url);
                       

     this.setState({url})


              
                   })
                   .catch((error) => {
                      
                        
                        
                        console.log(error);
                   }) 

          
         


}
    
     
   renderImage = () => {
         
         

  
                    
          return (
               <img src={this.state.url} alt={ this.props.alt } />
          );            
     }

     render() {        
          return (
               <div className={'profile-image__current'}>
                    { this.renderImage() }
               </div>
          );
     }
}

export default Image;