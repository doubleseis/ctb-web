import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import MuiAlert from '@material-ui/lab/Alert';
import React from 'react';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} style={{width:500}}/>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

function CustomizedSnackbars(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
 
  return (
    <div className={classes.root}>
      
      <Snackbar open={open} autoHideDuration={4000} onClose={handleClose} >
        <Alert onClose={handleClose} severity="success" >
         Update Successful!
        </Alert>
      </Snackbar>
      
    </div>
  );
}


export default CustomizedSnackbars