import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top';
import Spinner from './assets/images/spinner.gif';
import "./assets/style/style.scss";
import { auth, db } from './components/Firebase/Firebase';
import Footer from './components/Footer';
import ResetPassword from "./components/Forms/ResetPassword";
import Header from './components/Header';
import JoinAsPro from './components/JoinAsPro/JoinAsPro';
import Onboarding from "./components/JoinAsPro/Onboarding";
import LogOut from './components/LogOut';
import About from "./pages/About";
import BlogTemplate from "./pages/BlogTemplate";
import Branding from "./pages/Branding";
import Contact from "./pages/Contact";
import Dashboard from './pages/Dashboard';
import FAQClients from "./pages/FAQClients";
import FAQPros from "./pages/FAQPros";
import Home from './pages/Home';
import HowItWorks from './pages/HowItWorks';
import MeetOurPros from './pages/MeetOurPros';
import PrivacyPolicy from "./pages/PrivacyPolicy";
import ProPremium from "./pages/ProPremium";
import ProTemplate from "./pages/ProTemplate";
import Register from './pages/Register';
import Splash from "./pages/Splash";
import TermsOfUse from "./pages/TermsOfUse";

class App extends Component {
  constructor() {
    super();
    this.state = {
      splash: true,
      authenticated : false,
      loading: true,
      uid: '',
      posts: [],
      pros: []
    }
  }

  renderSplash = () => {
    this.setState({
      splash: false
    });
  }

  UNSAFE_componentWillMount() {
    this.removeAuthListener = auth.onAuthStateChanged((user) => {
      if(user) {
        this.setState({
          authenticated: true,
          loading: false,
          uid: user.uid
        })
      } else {
        this.setState({
          authenticated: false,
          loading: false
        })
      }
    })
  }

  componentWillUnmount() {
    this.removeAuthListener();
  }

  componentDidMount() {
    let currentComponent = this;
    db.collection('blog').get().then(function(querySnapshot) {
         querySnapshot.forEach(function(doc) {
              currentComponent.setState((prevState) => ({
                   posts: [...prevState.posts, {
                        slug: doc.data().slug ? doc.data().slug : doc.id,
                   }]
              }))
         });
    });
    db.collection('users').get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
           currentComponent.setState((prevState) => ({
                pros: [...prevState.pros, {
                    proUid: doc.id,
                    slug: doc.data().username,
                    firstName: doc.data().firstName,
                    lastName: doc.data().lastName,
                    about: doc.data().about,
                    background: doc.data().background,
                    certifications: doc.data().certifications,
                    funFact: doc.data().funFact,
                    favQuote: doc.data().favQuote,
                    isApproved: doc.data().isApproved,
                    isAdmin: doc.data().isAdmin,
                    isPro: doc.data().isPro,
                    photoURL: doc.data().photoURL,
                    professions: doc.data().professions,
                    availability: doc.data().availability,
                    ratesInPerson: doc.data().ratesInPerson,
                    ratesOnline: doc.data().ratesOnline,
                    abilities: doc.data().Ability,
                    travel: doc.data().travel
                }]
           }))
      });
    });
    db.collection('settings').get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
           currentComponent.setState(() => ({
                splash: doc.data().splash,
           }))
      });
 });
  }

  render() {

    if( this.state.splash === true ) {
      return (
        <Splash action={ this.renderSplash } />
      )
    } 

    if(this.state.loading === true) {
      return (
        <div className={'loading'}>
          <div className={'loading__container container'}>
            <div className={'loading__content'}>
              <img src={ Spinner } />
              <p><strong>Loading</strong></p>
            </div>
          </div>
        </div>
      )
    }

    return (
      <Router>
        <Header authenticated={this.state.authenticated} />
          <ScrollToTop>
            <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/find-a-pro" component={MeetOurPros} />
              <Route path="/how-choose-to-be-fit-works" component={HowItWorks} />
              <Route exact path="/logout" component={LogOut} />
              <Route path="/about" component={About} />
              <Route path="/faq-pros" component={FAQPros} />
              <Route path="/faq-clients" component={FAQClients} />
              <Route path="/register" component={Register} />
              <Route path="/blog" component={BlogTemplate} />
              <Route path="/contact" component={Contact} />
              <Route path="/terms-of-use" component={TermsOfUse} />
              <Route path="/privacy-policy" component={PrivacyPolicy} />
              <Route path="/join-as-pro" component={JoinAsPro} /> 
              <Route path="/forgot-password" component={ResetPassword} /> 
              <Route path="/pro-premium" component={ProPremium} />
              <Route path="/branding" component={Branding} /> 

              { this.state.posts.map((post,index) => (
                <Route key={index} path={'/blog/' + post.slug} state={this.state.post} component={BlogTemplate}/>
              )) }
              {
                this.state.pros.map((pro,index) => {
                  const isPro = pro.isPro ?
                    <Route key={index} path={`/pro/${pro.slug}`} render={(props) => <ProTemplate {...props} pro={pro} user={ this.state.uid } authenticated={this.state.authenticated}  />} />
                    : '' ;
                  return isPro;
                })
              }
              { this.state.authenticated === true ? 
                  <Route path="/dashboard" render={(props) => <Dashboard {...props} uid={this.state.uid} />} />
                : ''
              }
              { this.state.authenticated === true ? 
                  <Route path='/onboarding' render={(props) => <Onboarding {...props} uid={this.state.uid} />} />
                : ''
              }
            </Switch>
          </ScrollToTop>
        <Footer />
      </Router>
    );
  }
}

export default App;
