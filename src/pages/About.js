import React, { Component } from 'react';
import audricImage from '../assets/images/audric.png';
import justenImage from '../assets/images/justen.png';
import imageBlockImage from '../assets/images/pushing-weights.jpg';
import headImage from '../assets/images/walk-up-steps.jpg';
import ContentBlock from '../components/ContentBlock/ContentBlock';
import PageHeader from '../components/PageHeader/PageHeader';

class About extends Component {
     render() {
          const pageHeader = {
               pageTitle: 'About',
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };

          const textBlock = {
               background: {},
               content: '<p>Choose To Be Fit (C2B) provides a platform and community to connect professionals (Trainers, Nutritionists, Chefs, and Massage Therapists) with clients in a convenient way through one of the most powerful tools out there today, the internet. If you need a trainer to help you meet your fitness goals, a massage therapist to provide a therapeutic massage, a nutritionist to design a tailored meal plan, or a chef to prepare the perfect meal, choose Choose To Be Fit. With C2B, our focus is to promote a lifestyle of health, wellness, motivation, and positivity for both clients and professionals. The purpose of our existence is to create a culture where everyone chooses to be fit for all aspects of life.</p>',
               spacing: 'xl',
               textAlignment: 'center',
               iframe: {},
               button: {}
          }
          
          const imageBlock = {
               background: {
                    image: imageBlockImage,
                    position: 'center',
                    size: 'cover',
                    repeat: 'no-repeat'
               },
               title: 'We’re on a mission to foster a GLOBAL community of health, positivity, motivation, and happiness in the next 5 years',
               spacing: 'xl',
               centerBlockAlignment: true,
               textColor: 'white',
               textAlignment: 'center',
               iframe: {},
               button: {}
          }
          
          const audric = {
               background: {},
               modifier: 'inverse',
               image: audricImage,
               title: 'Audric Smith',
               spacing: 'xl',
               columns: 'two',
               content: '<p class="content-block__sub-head">Co-Founder, CEO</p><p>Audric Smith is a Founder and Owner of Choose To Be You. He has always had a passion for health &amp; fitness, and discovered C2Byou to transfer this lifestyle to others and positively impact as many lives as possible. Audric received his Masters of Accounting degree from The Ohio State, and Bachelor of Science in Accounting from UNC Fayetteville State University. He also has a CPA license and is a Senior Manager at an accounting and financial consulting firm. Audric is also active in the community, contributing to organizations such as Big Brothers Big Sisters, Salvation Army, and Boys &amp; Girls Club, and Camillus House. Audric was born in Nassau, Bahamas and currently resides in Florida. When not occupied with the aforementioned activities, he is engaged in motivational speaking, self-education, and writing.</p>',
               textColor: 'black',
               textStyle: 'normal',
               iframe: {
                    display: false,
               },
               button: {
                    link: ''
               }
          };
          const justen = {
               background: {},
               image: justenImage,
               title: 'Justen Augustine',
               spacing: 'xl',
               columns: 'two',
               content: '<p class="content-block__sub-head">Co-Founder, COO</p><p>Justen Augustine is a Founder at Choose To Be You and directs strategic initiatives that concerns the company’s growth in the Health &amp; Fitness Industry. He has always been extremely passionate about health &amp; fitness since he was 15. Later, he discovered this opportunity, C2Byou, with his cousin, Audric Smith, to transfer his strong emotion into value, by helping others live a happier and healthier life. Justen has acquired his Personal Trainer Certification from ISSA. As an online fitness instructor, Justen has helped thousands of students and clients achieve their physical fitness goals. Justen was born in Florida and currently resides in New York. When he’s not working, the Social Media Influencer is engaged in exercise, self-education, and travel.</p>',
               textColor: 'black',
               textStyle: 'normal',
               iframe: {
                    display: false,
               },
               button: {
                    link: ''
               }
          };

          return (
               <section className={'about'}>
                    <PageHeader {...pageHeader} />
                    <div className={'about__container'}>
                         <ContentBlock {...textBlock}></ContentBlock>
                         <ContentBlock {...imageBlock}></ContentBlock>
                         <ContentBlock {...audric}></ContentBlock>
                         <ContentBlock {...justen}></ContentBlock>
                    </div>
               </section>

          );
     }
}
   
export default About;