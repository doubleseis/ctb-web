import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import BookingRequests from '../components/Dashboard/BookingRequests';
import Community from '../components/Dashboard/Community/CommunityDashboard';
import Highlights from '../components/Dashboard/Dashboard';
import Menu from '../components/Dashboard/Menu';
import Messages from '../components/Dashboard/Messages/Messages';
import PaymentHistory from '../components/Dashboard/PaymentHistory';
import Profile from '../components/Dashboard/Profile';
import Settings from '../components/Dashboard/Settings';
import { auth, db } from '../components/Firebase/Firebase';


class Dashboard extends Component {
     constructor() {
          super();
          this.state = {
               authenticated : false,
               loading: true,
               uid: ''
          }
     }

     componentDidMount() {
          
          auth.onAuthStateChanged(function(user) {
               if (user) {
                    this.setState({
                         uid: user.uid,
                         email: user.email,
                         phoneNumber: user.phoneNumber,
                    })
                    // console.log('This is the user: ', this.state.uid );
                    db.collection('users').doc(this.state.uid).get()
                         .then((doc) => {
                              const data = doc.data();
                              if(data.firstName){ this.setState({firstName: data.firstName}) }
                              if(data.lastName){ this.setState({lastName: data.lastName}) }
                              if(data.photoURL){ this.setState({photoURL: data.photoURL}) }
                              if(data.about){ this.setState({about: data.about}) }
                              if(data.background){ this.setState({background: data.background}) }
                              if(data.certifications){ this.setState({certifications: data.certifications})}
                              if(data.funFact){ this.setState({certifications: data.funFact})}
                              if(data.favQuote){ this.setState({certifications: data.favQuote})}
                              if(data.ratesOnline){ this.setState({ratesOnline: data.ratesOnline})}
                              if(data.ratesInPerson){ this.setState({ratesInPerson: data.ratesInPerson})}
                              if(data.professions){ this.setState({professions: data.professions})}
                              this.setState({
                                   isPro: data.isPro,
                                   isApproved: data.isApproved,
                                   isAdmin: data.isAdmin,
                                   username: data.username,
                                   travel: data.travel
                              })
                         })
                         .catch((error) => {
                              console.log('error:', error);
                         })              
               }
          }.bind(this));
     }
     
     render() {
          return (
               <section className={'dashboard'}>
                    <Menu pro={this.state.isPro} />
                    <Switch>
                         <Route path="/dashboard" exact render={(props) => <Highlights {...props} data={this.state}  />} />
                         <Route path="/dashboard/messages" render={(props) => <Messages {...props} data={this.state}  />} />
                         {
                              this.state.isPro === true ?
                                   <Route path="/dashboard/booking-requests" render={(props) => <BookingRequests {...props} data={this.state}  />} />
                              : ''
                         }
                         {
                              this.state.isPro === true ?
                                   <Route path="/dashboard/community" render={(props) => <Community {...props} data={this.state}  />} />
                              : ''
                         }
                         <Route path="/dashboard/profile" render={(props) => <Profile {...props} data={this.state}  />} />
                         <Route path="/dashboard/payment-history" render={(props) => <PaymentHistory {...props} data={this.state}  />} />
                         <Route path="/dashboard/settings" render={(props) => <Settings {...props} data={this.state}  />}/>
                    </Switch>
               </section>
          );
     }
}
   
export default Dashboard;