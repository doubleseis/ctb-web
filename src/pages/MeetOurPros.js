import Pagination from '@material-ui/lab/Pagination';
import React, { useState } from 'react';
import { connect } from "react-redux";
import headImage from '../assets/images/walk-up-steps.jpg';
import { db } from "../components/Firebase/Firebase";
import PageHeader from '../components/PageHeader/PageHeader';
import Search from "../components/Search/findPageSearch";
import Cards from "../components/Search/proUserCards";
import { addfilteredusers, addprousers } from "../redux/reducers/proUser.reducer";


const MeetOurPros = (props) => {
     const [states, setstates] = useState([])
     const [prousers, setprousers] = useState([])
     const [currentPage, setCurrentPage] = useState(1);
     const [postsPerPage] = useState(10);
     const indexOfLastPost = currentPage * postsPerPage;
     const indexOfFirstPost = indexOfLastPost - postsPerPage;
     const currentlist = states
     const paginationList = currentlist.slice(indexOfFirstPost, indexOfLastPost);
     const pageNumber = currentlist.length / 10 + 1

     React.useEffect(() => {
          let userArray = []
          const datas = db.collection('users')
          const doc = datas.get()
               .then(snapshot => {
                    snapshot.forEach(doc => {
                         let a = doc.data()
                         userArray.push(a)
                    });
               })
               .then(() => {
                    const prosArray = userArray.filter(item => item.isPro == true)
                    setprousers(prosArray)
                    setstates(prosArray)
               })
               .catch(e => console.log(e))
     }, [])

     const pageHeader = {
          pageTitle: 'Your health & fitness pro is just a click away',
          includeSearch: false,
          background: {
               image: headImage,
               size: 'cover',
               repeat: 'no-repeat',
               position: 'center',
               overlay: true
          }
     }

     return (

          <section className={'meet-pros'}>
               <PageHeader {...pageHeader} />
          
               <div className="meet-pros__main container">
                    <Search set={setstates} list={prousers} />
                    
                    <div className="meet-pros__cards">
                         { paginationList.map(user => {
                              return (
                                   <Cards profile={user}></Cards>
                              )
                         })}
                    </div>
                    
                    <div className="pagination">
                         <Pagination count={ pageNumber } onChange={(e, page) => setCurrentPage(page)} color="primary" />
                    </div>
               </div>
          </section>
     );

}

const mapstatetoprops = (state) => (
     {
          filtered: state.prouser.filteredUsers,
          prouser: state.prouser.proUsers

     }
)
const dispatchtostate = dispatch => (
     {
          addprousers: (Array) => dispatch(addprousers(Array)),
          filteredprousers: (Array) => dispatch(addfilteredusers(Array))
     }
)

export default connect(mapstatetoprops, dispatchtostate)(MeetOurPros);