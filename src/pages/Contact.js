import React, { Component } from "react";
import headImage from '../assets/images/walk-up-steps.jpg';
import ContactUsForm from "../components/Forms/ContactUsForm";
import PageHeader from "../components/PageHeader/PageHeader";

class Contact extends Component {
     render() {
          const pageHeader = {
               pageTitle: "Contact Us",
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          return (
               <div className={"register"}>
                    <PageHeader {...pageHeader} />

                    <div className={'form-block'}>
                         <div className={'container'}>
                              <ContactUsForm />
                         </div>
                    </div>
               </div>
          );
     }
}

export default Contact;
