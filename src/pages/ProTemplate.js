import { faList, faQuoteLeft, faSmileBeam } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as firebase from 'firebase';
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import headImage from '../assets/images/walk-up-steps.jpg';
import FAQItem from '../components/FAQ/FAQItem';
import FAQList from '../components/FAQ/FAQList';
import { auth, db } from '../components/Firebase/Firebase';
import { proShare, removeUnderscore, socialShare } from '../components/Helpers';
import PageHeader from '../components/PageHeader/PageHeader';
import Image from '../components/Profile/Image';
import ProPricing from '../components/ProTemplate/Pricing';
import ProPricingHidden from '../components/ProTemplate/PricingHidden';

class ProTemplate extends Component {

     constructor(props){
          super(props);
          this.state = {
               following: false,
               facebook: '',
               twitter: '',
               linkedIn: '',
               pinterest: '',
               faq_1_question: '',
               faq_1_answer: '',
               faq_2_question: '',
               faq_2_answer: '',
               faq_3_question: '',
               faq_3_answer: '',
               faq_4_question: '',
               faq_4_answer: '',
               faq_5_question: '',
               faq_5_answer: ''
          }
          this.renderSpecialties = this.renderSpecialties.bind(this);
          this.toggleFollow = this.toggleFollow.bind(this);
     }

     componentDidMount() {
          let current = this;
          auth.onAuthStateChanged(function(user) {
               if(user) {
                    db.collection('users').doc(user.uid).collection('social').doc('following').get()
                         .then(function(doc) {
                              if(doc.exists) {
                                   current.setState({
                                        following: doc.data().accounts.includes(current.props.pro.proUid) ? true : false ,
                                   })
                              }
                         })
                    db.collection('users').doc(user.uid).collection('social').doc('external').get()
                         .then( function(doc) {
                              if(doc.exists) {
                                   current.setState({
                                        facebook: doc.data().facebook,
                                        twitter: doc.data().twitter,
                                        linkedIn: doc.data().linkedIn,
                                        pinterest: doc.data().pinterest
                                   })
                              }
                         })

                    db.collection('users').doc(user.uid).collection('social').doc('faq').get()
                         .then( function(doc) {
                              if(doc.exists) {
                                   current.setState({
                                        faq_1_question: doc.data().faq_1_question,
                                        faq_1_answer: doc.data().faq_1_answer,
                                        faq_2_question: doc.data().faq_2_question,
                                        faq_2_answer: doc.data().faq_2_answer,
                                        faq_3_question: doc.data().faq_3_question,
                                        faq_3_answer: doc.data().faq_3_answer,
                                        faq_4_question: doc.data().faq_4_question,
                                        faq_4_answer: doc.data().faq_4_answer,
                                        faq_5_question: doc.data().faq_5_question,
                                        faq_5_answer: doc.data().faq_5_answer
                                   })
                              }
                         })
               }
          })
     }

     toggleFollow = (e) => {
          e.preventDefault();
          let current = this;
          if (this.state.following !== true) {
               db.collection(`users`).doc(`${current.props.user}`).collection('social').doc('following').get()
                    .then((doc) => {
                         if(doc.exists) {
                              db.collection(`users`).doc(`${current.props.user}`).collection('social').doc('following').update({
                                   accounts: firebase.firestore.FieldValue.arrayUnion( current.props.pro.proUid )
                              });
                         } else {
                              db.collection(`users`).doc(`${current.props.user}`).collection('social').doc('following').set({
                                   accounts: firebase.firestore.FieldValue.arrayUnion( current.props.pro.proUid )
                              });
                         }
                    })
                    this.setState({
                         following: true
                    })
               return;
          }
          
          db.collection(`users`).doc(`${current.props.user}`).collection('social').doc('following').update({ 
               accounts: firebase.firestore.FieldValue.arrayRemove( current.props.pro.proUid )
          });

          this.setState({
               following: false
          })
          console.log('true');
          return;
     }

     renderSpecialties = () => {          
          const { professions, abilities } = this.props.pro;
          const isChef = professions.Chef ? '<li>Chef</li>' : '';
          const isTrainer = professions.FitnessTrainer ? '<li>Fitness Trainer</li>' : '';
          const isMassage = professions.MassageThreapist ? '<li>Massage Therapist</li>' : '';
          const isNutritionist = professions.Nutritionist ? '<li>Nutritionist</li>' : '';
          
          const abilitiesArr = [];
          typeof(abilities) !== 'undefined' && abilities.aerobic_fitness && ( abilitiesArr.push( removeUnderscore('aerobic_fitness') ) );
          typeof(abilities) !== 'undefined' && abilities.athletic && ( abilitiesArr.push( removeUnderscore('athletic') ) );
          typeof(abilities) !== 'undefined' && abilities.body_fat_loss && ( abilitiesArr.push( removeUnderscore('body_fat_loss') ) );
          typeof(abilities) !== 'undefined' && abilities.competition_prep && ( abilitiesArr.push( removeUnderscore('competition_prep') ) );
          typeof(abilities) !== 'undefined' && abilities.endurance_training && ( abilitiesArr.push('endurance_training') );
          typeof(abilities) !== 'undefined' && abilities.pilflexibilityates && ( abilitiesArr.push('flexibility') );
          typeof(abilities) !== 'undefined' && abilities.forming_and_toning && ( abilitiesArr.push('forming_and_toning') );
          typeof(abilities) !== 'undefined' && abilities.normal_nutrition && ( abilitiesArr.push('normal_nutrition') );
          typeof(abilities) !== 'undefined' && abilities.paleo && ( abilitiesArr.push('paleo') );
          typeof(abilities) !== 'undefined' && abilities.pilates && ( abilitiesArr.push('pilates') );
          typeof(abilities) !== 'undefined' && abilities.power_lifting && ( abilitiesArr.push('power_lifting') );
          typeof(abilities) !== 'undefined' && abilities.pregnancy && ( abilitiesArr.push('pregnancy') );
          typeof(abilities) !== 'undefined' && abilities.rehabilitation && ( abilitiesArr.push('rehabilitation') );
          typeof(abilities) !== 'undefined' && abilities.size_gaining && ( abilitiesArr.push('size_gaining') );
          typeof(abilities) !== 'undefined' && abilities.vegan_or_vegetarian && ( abilitiesArr.push('vegan_or_vegetarian') );
          typeof(abilities) !== 'undefined' && abilities.weight_loss && ( abilitiesArr.push('weight_loss') );
          typeof(abilities) !== 'undefined' && abilities.yoga && ( abilitiesArr.push('yoga') );

          typeof(abilities) !== 'undefined' && abilities.public_health && ( abilitiesArr.push('public_health') );
          typeof(abilities) !== 'undefined' && abilities.sports && ( abilitiesArr.push('sports') );
          typeof(abilities) !== 'undefined' && abilities.pediatric && ( abilitiesArr.push('pediatric') );
          typeof(abilities) !== 'undefined' && abilities.diabetes && ( abilitiesArr.push('diabetes') );
          typeof(abilities) !== 'undefined' && abilities.heart_health && ( abilitiesArr.push('heart_health') );
          typeof(abilities) !== 'undefined' && abilities.autoimmune_disease && ( abilitiesArr.push('autoimmune_disease') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('food_allergies') );

          typeof(abilities) !== 'undefined' && abilities.deep_tissue && ( abilitiesArr.push('deep_tissue') );
          typeof(abilities) !== 'undefined' && abilities.swedish && ( abilitiesArr.push('swedish') );
          typeof(abilities) !== 'undefined' && abilities.stone && ( abilitiesArr.push('stone') );
          typeof(abilities) !== 'undefined' && abilities.thai && ( abilitiesArr.push('thai') );
          typeof(abilities) !== 'undefined' && abilities.sports && ( abilitiesArr.push('sports') );
          typeof(abilities) !== 'undefined' && abilities.reflexology && ( abilitiesArr.push('reflexology') );

          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('seafood') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('american') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('breakfast_or_brunch') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('international') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('southern') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('healthy') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('desserts') );
          typeof(abilities) !== 'undefined' && abilities.food_allergies && ( abilitiesArr.push('juices_and_smoothies') );

          var renderAbilities = abilitiesArr.join(", ");
          var renderAbilitiesMarkup = typeof(abilities) !== 'undefined' ? `<p style="text-transform:capitalize">${renderAbilities}</p>` : '';

          return ReactHtmlParser(`
               <h3><strong>Main:</strong></h3>
               <ul>
                    ${isChef}
                    ${isTrainer}
                    ${isMassage}
                    ${isNutritionist}
               </ul>
               <h3><strong>Specializing in:</strong></h3>
               ${renderAbilitiesMarkup}
          `);
     }

     renderPrices = () => {
          const { authenticated } = this.props;
          // console.log(authentication);
          
          if( authenticated ) {
               // console.log(authentication);
               return <ProPricing
                         proUid={ this.props.pro.proUid }
                         user={this.props.user}
                         firstName={this.props.pro.firstName}
                         lastName={this.props.pro.lastName}
                         availability={ this.props.pro.availability }
                         professions={ this.props.pro.professions }
                         ratesInPerson={ this.props.pro.ratesInPerson }
                         ratesOnline={ this.props.pro.ratesOnline }
                         travel={ this.props.pro.travel } />;
          }
          return <ProPricingHidden />;
     }
     
     render() {
          // console.log(this.props.pro.uid);

          const pageTitle = {
               pageTitle: this.props.pro.firstName + ' ' + this.props.pro.lastName,
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };

          return (
               <section className={'pro'}>
                    <PageHeader {...pageTitle} />
                    <div className={'pro__container container'}>
                         <div className={'pro__head'}>
                              <div className={'pro__head-title'}>
                                   <h2 className={'heading--xl'}>About {this.props.pro.firstName} {this.props.pro.lastName}</h2>
                              </div>
                              <div className={'pro__head-social'}>
                                   { socialShare('Profile') } 
                                   <button className={ this.state.following ? 'button button--primary-contrast button--following' : ' button button--primary button--not-following' } onClick={this.toggleFollow}>{ this.state.following ? 'following' : 'follow' }</button>
                              </div>
                         </div>
                         
                         <div className={'pro__body'}>
                              <div className={'pro__body-image'}>
                                   {/* { ReactHtmlParser(this.props.pro.uid) } */}
                                   <Image image={ this.props.pro.photoURL } user={ this.props.pro.proUid } />
                              </div>
                              <div className={'pro__body-about'}>
                                   { ReactHtmlParser(this.props.pro.about) }

                                   <h2><FontAwesomeIcon icon={faList} /> Specialties</h2>
                                   
                                   { this.renderSpecialties() }
                              </div>
                         </div>
                    </div>

                    <div className={'pro__container pro__container--bg'}>
                         <div className={'container'}>
                              <div className={'pro__pricing'}>
                                   { this.renderPrices() }
                              </div>
                         </div>
                    </div>

                    <div className={'pro__container container'}>
                         <div className={'pro__fun'}>
                              { this.props.pro.funFact && (
                                   <div className={'pro__fun-fact'}>
                                        <div className={'pro__fun-content'}>
                                             <h2><FontAwesomeIcon icon={faSmileBeam} /> Fun Fact</h2>
                                             { ReactHtmlParser(this.props.pro.funFact) }
                                        </div>
                                   </div>
                              )}
                              { this.props.pro.favQuote && (
                                   <div className={'pro__fun-quote'}>
                                        <div className={'pro__fun-content'}>
                                             <h2><FontAwesomeIcon icon={faQuoteLeft} /> Favorite Quote</h2>
                                             { ReactHtmlParser(this.props.pro.favQuote) }
                                        </div>
                                   </div>
                              )}
                         </div>

                         <div className={'pro__accordion accordion__container'}>
                              <FAQList>
                                   { this.props.pro.background && (
                                        <FAQItem question={'Professional Background'} answer={this.props.pro.background} />
                                   )}
                                   { this.props.pro.certifications && (
                                        <FAQItem question={'Certifications'} answer={this.props.pro.certifications} />
                                   )}
                              </FAQList>
                         </div>
                    </div>

                    <div className={'pro__container pro__container--bg'}>
                         <div className={'container'}>
                              <div className={'pro__social'}>
                                   <h2>Follow <span>{ this.props.pro.firstName ? `${this.props.pro.firstName} on` : '' }</span> these social accounts</h2>
                                   { proShare( this.state.facebook, this.state.twitter, this.state.linkedIn, this.state.pinterest) }
                              </div>
                         </div>
                    </div>

                    <div className={'pro__container'}>
                         <div className={'container'}>
                              <div className={'pro__faq-title'}>
                                   <h2 className={'text-center'}>Frequently Asked Questions for <span style={{textTransform:'capitalize'}}>{ this.props.pro.firstName }</span></h2>
                              </div>
                              <div className={'pro__faq-content'}>
                                   <FAQList>
                                        { this.state.faq_1_question && this.state.faq_1_answer && (
                                             <FAQItem question={ this.state.faq_1_question } answer={ this.state.faq_1_answer } />
                                        )}
                                        { this.state.faq_2_question && this.state.faq_2_answer && (
                                             <FAQItem question={ this.state.faq_2_question } answer={ this.state.faq_2_answer } />
                                        )}
                                        { this.state.faq_3_question && this.state.faq_3_answer && (
                                             <FAQItem question={ this.state.faq_3_question } answer={ this.state.faq_3_answer } />
                                        )}
                                        { this.state.faq_4_question && this.state.faq_4_answer && (
                                             <FAQItem question={ this.state.faq_4_question } answer={ this.state.faq_4_answer } />
                                        )}
                                        { this.state.faq_5_question && this.state.faq_5_answer && (
                                             <FAQItem question={ this.state.faq_5_question } answer={ this.state.faq_5_answer } />
                                        )}
                                   </FAQList>
                              </div>
                         </div>
                    </div>

               </section>
          );
     }
}
   
export default ProTemplate;