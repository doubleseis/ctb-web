import React, { Component } from 'react';
import headImage from '../assets/images/walk-up-steps.jpg';
import FAQItem from '../components/FAQ/FAQItem';
import FAQList from '../components/FAQ/FAQList';
import { db } from '../components/Firebase/Firebase';
import PageHeader from '../components/PageHeader/PageHeader';

class FAQClients extends Component {
     constructor(props) {
          super(props);
          this.state = {
               faqs: []
          }
     }
     
     componentDidMount() {
          db.collection('faq-clients').get()
               .then(function(querySnapshot) {
                    if (querySnapshot.docs.length !== 0) {
                         const faqs = querySnapshot.docs;
                         // console.log(faqs);
                         faqs.map(faq => {
                              this.setState({
                                   faqs: [...this.state.faqs, faq.data()]
                              });
                              return;
                              // console.log(this.state.faqs);
                         })
                         
                    } else {
                         // this.setState({ data: {} });
                         console.log("No such document!");
                    }
               }.bind(this))
               .catch(function (error) {
                    // this.setState({ data: {} });
                    console.log("Error getting document:", error);
               }.bind(this));
     }
     
     render() {
          const pageTitle = {
               pageTitle: 'Client FAQs',
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          const { faqs } = this.state;

          return (
               <section className={'accordion'}>
                    <PageHeader {...pageTitle} />
                    <div className={'accordion__container container'}>
                         <FAQList>
                              { faqs.map((faq,index) => (
                                   <FAQItem key={index} isMessage={ false } {...faq} />
                              )) }
                         </FAQList>
                    </div>
               </section>

          );
     }
}

export default FAQClients;
