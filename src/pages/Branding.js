import React, { Component } from "react";
import headImage from '../assets/images/walk-up-steps.jpg';
import PageHeader from "../components/PageHeader/PageHeader";

class Branding extends Component {
     render() {
          const pageHeader = {
               pageTitle: "Branding",
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          return (
               <div>
                    <PageHeader {...pageHeader} />

                    <main className={'branding'} style={{paddingTop:20,paddingBottom:20}}>
                         <div className={'branding__container container'}>
                              <div className={'branding__title'}>
                                   <p className={'heading--md text-center'}>Choose To Be You - Branding Standards</p>
                              </div>
                              <div className={'branding__content'}>
                                   <div className={'branding__font blocked'}>
                                        <h3>Fonts</h3>
                                        <p>Primary Font: Raleway</p>
                                        <p>Secondary Font: Quicksand</p>
                                   </div>
                                   <div className={'branding__colors blocked'}>
                                        <h3>Colors</h3>
                                        <p>Primary: #4f63a2 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#4f63a2'}}></span></p>
                                        <p>Primary Hover: #23282d <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#23282d'}}></span></p>
                                        <p>Primary Darker: #23282d <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#23282d'}}></span></p>
                                        <p>Secondary: #1b4588 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#1b4588'}}></span></p>
                                        <p>Contrast: #1bad51 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#1bad51'}}></span></p>
                                        <p>White: #fff <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#fff'}}></span></p>
                                        <p>Black: #000 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#000'}}></span></p>
                                        <p>Gray Base: #000 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#000'}}></span></p>
                                        <p>Gray Darker: #2e343a <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#2e343a'}}></span></p>
                                        <p>Gray: #808080 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#808080'}}></span></p>
                                        <p>Gray Light: #96a0a9 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#96a0a9'}}></span></p>
                                        <p>Gray Lighter: #b6b6b6 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#b6b6b6'}}></span></p>
                                        <p>Gray Lightest: #eff3f6 <span style={{border:'black solid 1px',width:10,height:10,display:'inline-block',background:'#eff3f6'}}></span></p>
                                   </div>
                              </div>
                         </div>
                    </main>
               </div>
          );
     }
}

export default Branding;
