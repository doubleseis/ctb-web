import React, { Component } from 'react';
import bgPattern from '../assets/images/bg-pattern.jpg';
import imageCertified from '../assets/images/c2bfit-certified.png';
import cbImage1 from '../assets/images/choosetobefitbck.png';
import cbImage2 from '../assets/images/match_with_pro2-1.jpg';
import headImage from '../assets/images/walk-up-steps.jpg';
import ContentBlock from '../components/ContentBlock/ContentBlock';
import PageHeader from '../components/PageHeader/PageHeader';

class HowItWorks extends Component {
     render() {
          const pageHeader = {
               pageTitle: 'How Choose To Be Fit Works',
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          }
          const howItWorksBlock = {
               title: 'How It Works',
               spacing: 'xl',
               background: {
                    image: cbImage1,
                    size: 'contain',
                    repeat: 'no-repeat'
               },
               content: '<p>Experience health & wellness in all aspects of your life; make it a lifestyle.<br />Choose your category.<br />Browse profiles.<br />Find your match.<br />Then get ready to achieve your goal.<br />It’s that convenient and easy. <strong>Choose To Be Fit</strong>.</p>',
               centerBlockAlignment: true,
               textColor: 'black',
               textStyle: 'normal',
               textAlignment: 'center',
               iframe: {
                    display: true,
                    src: 'https://www.youtube.com/embed/uz1wLTFXidA',
                    autoPlay: false,

               },
               button: {
                    link: '/register',
                    title: 'Get Started Today',
                    style: 'contrast'
               }
          }
          const matchWithProBlock = {
               title: 'Match With A Pro',
               spacing: 'xl',
               background: {
                    image: cbImage2 
               },
               content: '<p>Complete your profile for personalization. Browse a listing of professionals based on your needs.</p><p>With each you match the following features are included:</p><ul><li>Professional profile</li><li>Pricing</li><li>Online chat</li></ul>',
               textColor: 'white',
               textStyle: 'normal',
               iframe: {
                    display: false,
               },
               button: {
                    link: '/register',
                    title: 'Apply Now',
                    style: 'contrast'
               }
          }
          const connectBlock = {
               title: 'Connect with the Best',
               image: imageCertified,
               modifier: 'inverse',
               spacing: 'xl',
               columns: 'two',
               centerBlockAlignment: true,
               background: {
                    image: bgPattern 
               },
               content: '<p>Communicate with professionals if you need more information. When you’re ready and have made the choice to be fit, select the pro at the price that is best for you.</p>',
               textColor: 'black',
               textStyle: 'normal',
               textAlignment: 'center',
               iframe: {
                    display: false,
               },
               button: {}
          }
          return (
               <section>
                    <PageHeader {...pageHeader} />
                    <ContentBlock {...howItWorksBlock}></ContentBlock>
                    <ContentBlock {...matchWithProBlock}></ContentBlock>
                    <ContentBlock {...connectBlock}></ContentBlock>
               </section>
          );
     }
}

export default HowItWorks;