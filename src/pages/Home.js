import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import cbImage2 from '../assets/images/five-star-trainer.png';
import phoneMock from '../assets/images/phone-mock.jpg';
import headImage from '../assets/images/walk-up-steps.jpg';
import cbImage1 from '../assets/images/who-we-are-header.jpg';
import ContentBlock from '../components/ContentBlock/ContentBlock';
import PageHeader from '../components/PageHeader/PageHeader';


class Home extends Component {

     constructor(props){
          super(props);
          this.renderIcon = this.renderIcon.bind(this);
     }

     renderIcon = (iconName) => {
          return <FontAwesomeIcon icon={iconName} />
     }

     render() {
          const pageHeader = {
               pageTitle: 'We send health & fitness pros right to your door',
               includeSearch: true,
               background : {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
               
          }

          const howItWorksBlock = {
               title: 'How It Works',
               // titleSize: 'xl',
               spacing: 'xl',
               modifier: 'center',
               background: {
                    image: 'none'
               },
               content: '<p>Choose your category. Browse profiles. Find your match. Then get ready to achieve your health and fitness goals.</p><ul class="inline-list"><li><i class="fas fa-user"></i><p>Find nearby fitness professionals in your area</p></li><li><i class="fas fa-dollar-sign"></i><p>The Right Pro, For the Right Price</p></li><li><i class="fas fa-users"></i><p>Community You Can Rely On</p></li><li><i class="fas fa-check-circle"></i><p>C2B Certified</p></li><li><i class="fas fa-book"></i><p>Resources to Support Your Fitness Journey</p></li><li><i class="fas fa-handshake"></i><p>Security Through Using Our Platform</p></li><li><i class="fas fa-hand-peace"></i><p>Positivity, Always</p></li></ul>',
               textColor: 'black',
               textStyle: 'normal',
               textAlignment: 'center',
               iframe: {
                    display: false,
               },
               button: {
                    link: '/how-choose-to-be-fit-works',
                    title: 'How It Works',
                    style: 'contrast'
               }
          }

          const whoWeAreBlock = {
               title: 'Who We Are',
               spacing: 'xl',
               background: {
                    image: cbImage1
               },
               content: 'We created Choose To Be You (C2Byou) to help people lead a healthier lifestyle -- and we haven’t stopped building since. C2Byou is a platform where you can book health pros instantly online, based on your convenience, and get the resources you need to live a long happy life. Our mission is to foster a community of health, positivity, motivation, and happiness. If you want to achieve your life goals, then choose Choose To Be You.',
               textColor: 'white',
               textStyle: 'normal',
               textAlignment: 'center',
               iframe: {
                    display: true,
                    src: 'https://www.youtube.com/embed/uz1wLTFXidA',
                    autoPlay: false,

               },
               button: {
                    link: '#',
                    title: 'Learn More About Our Vision',
                    style: 'contrast'
               }
          }

          const downloadAppBlock = {
               title: 'Get the C2Byou app',
               titleSize: 'xl',
               spacing: '',
               modifier: 'inverse download-app',
               image: phoneMock,
               columns: 'two',
               background: {
                    image: 'none'
               },
               content: '<p>When you need to hire a health &amp; fitness pro, C2Byou finds them for you for free.</p><ul><li>Find nearby health &amp; fitness pros</li><li>Browse reviews by real clients</li><li>Book sessions, anywhere and anytime.</li></ul>',
               textColor: 'black',
               textStyle: 'normal',
               textAlignment: 'left',
               iframe: {
                    display: false,
               },
               button: {
                    link: '#',
                    title: 'Apply Now',
                    style: 'none'
               }
          }

          const fiveStarProBlock = {
               title: 'Are You A Five-Star Pro?',
               spacing: 'xl',
               background: {
                    image: cbImage2
               },
               content: '<p>List your business to reach thousands of potential clients.</p>',
               textColor: 'white',
               textStyle: 'normal',
               iframe: {
                    display: false,
               },
               button: {
                    link: '#',
                    title: 'Apply Now',
                    style: 'contrast'
               }
          }

          return (
               <section className={'home'}>
                    <PageHeader {...pageHeader} /> 
                    <ContentBlock {...downloadAppBlock}></ContentBlock>
                    <ContentBlock {...whoWeAreBlock}></ContentBlock>
                    <ContentBlock {...howItWorksBlock}></ContentBlock>
                    <ContentBlock {...fiveStarProBlock}></ContentBlock>
               </section>
          );
     }
}
   
export default Home;