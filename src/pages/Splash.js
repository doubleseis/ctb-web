import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import logoSecondary from '../assets/images/logo-secondary.png';
import underConstructionImage from '../assets/images/uc.png';
import Modal from '../components/Modal/ModalView';

class Splash extends Component {

     constructor(props) {
          super(props);
          this.state = {
               condition: false,
               isModalOpen: false,
               isInnerModalOpen: false,
               pw: ''
          }
          this.handleClick = this.handleClick.bind(this)
          this.closeModal = this.closeModal.bind(this);
          this.openModal = this.openModal.bind(this);
          this.onPasswordFieldChange = this.onPasswordFieldChange.bind(this);
          this.betaTestersLogin = this.betaTestersLogin.bind(this);
     }

     handleClick(){
          this.setState({
               condition: !this.state.condition
          })
     }

     openModal() {
		this.setState({
			isModalOpen: true
		});
     }
     
     closeModal() {
		this.setState({
			isModalOpen: false
		});
     }

     onPasswordFieldChange(e) {
          this.setState({
               [e.target.id]: e.target.value
          })
     }

     betaTestersLogin = (e) => {
          e.preventDefault();
          const { action } = this.props;
          if( this.state.pw !== 'ChooseToBe2020') {
               // console.log('nope');
               alert('Password is incorrect. Please try again.');
               return;
          } 
          // console.log('works', action);
          return action();
          
          // return action;
          // return this.props.action;
          // console.log( 'action', action );
     }

     render() {
          
          return (
               <section className={'splash'}>
                    <div className={'splash__container container'}>
                         <div className={'splash__content'}>
                              <img alt={'logo'} src={ logoSecondary } />
                              <h2><strong>C2B You</strong> is Under Maintenance.</h2>
                              <p>We are working on our website and app. This new platform will be way more modern, user-friendly, and innovative. Don't miss a beat, be the first to know when we go live.</p>
                         </div>

                         <div className={'splash__beta'}>
                              <button className={'splash__beta-btn'} onClick={this.openModal}>Beta Testers Login</button>
                         </div>
                         
                         <form method={'post'} action={'https://choosetobefit.us20.list-manage.com/subscribe/post?u=0c2f5384fdef29a5a9b9956dd&amp;id=441bbab02d'} className={'splash__form form validate'} name="mc-embedded-subscribe-form" target="_blank" noValidate onSubmit={ this.handleSubmit }>
                              <fieldset className={'form__container'}>
                                   <legend>Join Our Community</legend>
                                   <div className={'form__row form__row--double'}>
                                        <div className={'form__col'}>
                                             <label className={'screen-reader-text'} htmlFor='mce-FNAME'>First Name*</label>
                                             <input placeholder={'First Name'} id='mce-FNAME' type='text' name='FNAME' onChange={ this.handleChange } required />
                                        </div>
                                        <div className={'form__col'}>
                                             <label className={'screen-reader-text'} htmlFor='mce-LNAME'>Last Name*</label>
                                             <input placeholder={'Last Name'} id='mce-LNAME' type='text' name='LNAME' onChange={ this.handleChange } />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label className={'screen-reader-text'} htmlFor='mce-EMAIL'>Email</label>
                                             <input placeholder={'Email'} id='mce-EMAIL' type='email' name='EMAIL' onChange={ this.handleChange } required />
                                        </div>
                                   </div>
                                   <div className={'form__row'}>
                                        <div className={'form__col'}>
                                             <label className={'screen-reader-text'} htmlFor='mce-PHONE'>Phone Number</label>
                                             <div className="phonefield phonefield-us">
                                                  <span className="phonearea"><input className="phonepart " pattern="[0-9]*" id="mce-PHONE-area" name="PHONE[area]" maxLength="3" size="3" type="text" placeholder={'###'} /> - </span>
                                                  <span className="phonedetail1"><input className="phonepart " pattern="[0-9]*" id="mce-PHONE-detail1" name="PHONE[detail1]" maxLength="3" size="3" type="text" placeholder={'###'} /> - </span> 
                                                  <span className="phonedetail2"><input className="phonepart " pattern="[0-9]*" id="mce-PHONE-detail2" name="PHONE[detail2]" maxLength="4" size="4" type="text" placeholder={'####'} /></span>
                                                  {/* <span className="small-meta nowrap">&#40;###&#41; ###-####</span> */}
                                             </div>
                                             {/* <input placeholder={'Email'} id='mce-PHONE' type='phone' name='EMAIL' onChange={ this.handleChange } required /> */}
                                        </div>
                                   </div>
                              </fieldset>

                              <div id="mce-responses" className="clear">
                                   <div className="response" id="mce-error-response hide"></div>
                                   <div className="response" id="mce-success-response hide" ></div>
                              </div>

                              <div className={'screen-reader-text'} aria-hidden="true">
                                   <input type="text" name="b_0c2f5384fdef29a5a9b9956dd_441bbab02d" tabIndex="-1" defaultValue="" />
                              </div>

                              <div className={'form__submit'}>
                                   <input className={'button button--primary-inverse'} type='submit' defaultValue='Sign Up' name='subscribe' id="mc-embedded-subscribe" />
                              </div>
                         </form>

                         <Modal 
                              isModalOpen={this.state.isModalOpen}
                              closeModal={this.closeModal}
                         >
                              <h2>Beta Testers</h2>
                              <div className="form__row">
                                   <div className="form__col">
                                        <input id={'pw'} type={'password'} onChange={ this.onPasswordFieldChange } placeholder='Enter Password' />
                                   </div>
                                   <div className="form__col">
                                        <button className={'button button--primary'} onClick={this.betaTestersLogin}>Enter</button>
                                   </div>
                              </div>
                              <button className={'modal__close'} onClick={this.closeModal}><FontAwesomeIcon icon={faTimes} /></button>
                         </Modal>
                    </div>
                    <img src={underConstructionImage} />
               </section>

          );
     }
}
   
export default Splash;