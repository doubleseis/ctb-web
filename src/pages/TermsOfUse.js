import React, { Component } from 'react';
import headImage from '../assets/images/walk-up-steps.jpg';
import ContentBlock from '../components/ContentBlock/ContentBlock';
import { db } from '../components/Firebase/Firebase';
import PageHeader from '../components/PageHeader/PageHeader';

class TermsOfUse extends Component {

     constructor(props) {
          super(props);
          this.state = {
               data: []
          }
     }

     componentDidMount() {
          db.collection('pages').doc('37eakss4pw.r').get()
               .then((doc) => {
                    if (doc.exists) {
                         const data = doc.data();
                         this.setState({ data: data });
                         console.log("Document data:", data);
                    } else {
                         this.setState({ data: null });
                         // console.log("No such document!");
                    }
               }).catch(function (error) {
                    this.setState({ data: null });
                    console.log("Error getting document:", error);
               });
     }

     render() {
          const pageHeader = {
               pageTitle: this.state.data.title,
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          const audric = {
               background: {},
               spacing: 'xl',
               content: this.state.data.content,
               textStyle: 'normal',
               iframe: {
                    display: false,
               },
               button: {
                    link: ''
               }
          };

          return (
               <section className={'terms'}>
                    <PageHeader {...pageHeader} />
                    <div className={'terms__container'}>
                         <ContentBlock {...audric}></ContentBlock>
                    </div>
               </section>

          );
     }
}
   
export default TermsOfUse;