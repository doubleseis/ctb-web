import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import BlogLanding from '../components/Blog/BlogLanding';
import BlogPost from '../components/Blog/BlogPost';

class BlogTemplate extends Component {
     render() {
          return (
               <section className={'blog'}>
                    <Switch>
                         <Route path="/blog" component={ BlogLanding } exact />
                         <Route exact path="/blog/:blogid" component={ BlogPost }  />
                    </Switch>
               </section>
          );
     }
}
   
export default BlogTemplate;