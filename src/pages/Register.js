import React, { Component } from "react";
import headImage from '../assets/images/walk-up-steps.jpg';
import SignUpFrom from "../components/Forms/SignUpForm";
import PageHeader from "../components/PageHeader/PageHeader";

class Register extends Component {
     render() {
          const pageHeader = {
               pageTitle: "Your health & fitness pro is just a click away",
               includeSearch: false,
               background: {
                    image: headImage,
                    size: 'cover',
                    repeat: 'no-repeat',
                    position: 'center',
                    overlay: true
               }
          };
          return (
               <div className={"register"}>
                    <PageHeader {...pageHeader} />

                    <div className={'form-block'}>
                         <div className={'container'}>
                              <SignUpFrom />
                         </div>
                    </div>
               </div>
          );
     }
}

export default Register;
